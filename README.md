# Qtbat

## Ogólnie o projekcie

Projekt aplikacji o nazwie Qtbat

**Wymagania aplikacji**

- PHP 7.0
- Nginx
- baza Postgresql
- Composer
- Redis
- Node.js

## Zasady projektu

- nazewnictwo wszystkiego w języku angielskim
- komentarze, tak aby osoba nie znająca kodu mogła od razu stwierdzić za co dany fragment kodu odpowiada (albo jeszcze lepiej gdyby kod i nazewnictwo było takie, aby wszystko było wiadomo bez komentarzy)
- pisząc coś swojego pracujemy na osobnej gałęzi i dopiero gdy funkcjonalność jest gotowa włączamy się pull requestem po akceptacji innych do głównej gałęzi projektu
- wstawianie '// TODO treść' przy elementach kodu, które wymagają dokończenia
- przy SELECT daty, dopiska ::timestamp (aby pozbyć się dopisku związanego ze strefą czasową) np.
    SELECT register_date::timestamp FROM users;
- przy nazwach plików tam gdzie chcemy dać oddzielenie słow dajemy '_' np. 'profile_groups' (nie 'profile-groups'),
- w przypadku dwóch słow w linkach rozdzielamy je myślnikiem, a nie twardą spacją np. '/members-list', a nie '/members_list',
- przy tworzeniu  nowych konkursów podpinać się do już istniejących funkcji i schematów - stosować się do zapisanych w database.sql oznaczeń typów konkursów

## Przydatne informacje

**Wymiana kodu** - https://bitbucket.org/qtbat/

**WWW** - http://qtbat.com

## Instalacja

1. pobrać wszystkie pliki
2. stworzyć pliki .php na podstawie plików .template w config/
3. skonfigurować plik node/config.json
3. wgrać bazę danych
4. composer install
5. w katalogu node w konsoli użyć komendy npm install
5. gotowe!

## Node

Aby włączyć aplikację node.js należy w konsoli będąc w katalogu node użyć komendy node notifications.js
