<?php

function hashFromTimeAdd($length = 30, $add = null)
{
    return substr(md5(time().$add), 0, $length);
}

function bbcodeToHtml($text)
{
    $bbcode = [
        '<', '>',
        '[ul]', '[/ul]',
        '[ol]', '[/ol]',
        '[li]', '[/li]',
        '[b]', '[/b]',
        '[u]', '[/u]',
        '[i]', '[/i]',
        '"]'
    ];
    $html = [
        '&lt;', '&gt;',
        '<ul>', '</ul>',
        '<ol>', '</ol>',
        '<li>', '</li>',
        '<b>', '</b>',
        '<u>', '</u>',
        '<i>', '</i>',
        '">'
    ];
    $text = str_replace($bbcode, $html, $text);
    $text = nl2br($text);
    return $text;
}

function validateInput($value, $label, $parametrs, $database = null)
{
    /* parametrs array or name from defined
    return exception code or true
    exception codes:
    0 = email or function error
    1 = min legth
    2 = max length
    3 = characters
    4 = unique
    */
    $defined = [
        // min length, max length, characters (regex), unique (if yes enter database.column_name)
        // null = not required
        // 'email' from table users is FILTER_VALIDATE_EMAIL
        'login' => [3, 30, 'a-zA-Z0-9', 'users.login'],
        'password' => [7, null, null, null],
        'name' => [0, 100, 'a-zA-Z0-9ążśźęćńółĄŻŚŹĘĆŃÓŁ\s\-', null],
        'location' => [0, 100, 'a-zA-Z0-9ążśźęćńółĄŻŚŹĘĆŃÓŁ\s\-', null],
        //groups
        'group_unique_name' => [2, 30, 'a-zA-Z0-9', 'groups.unique_name'],
        'group_name' => [2, 80, 'a-zA-Z0-9ążśźęćńółĄŻŚŹĘĆŃÓŁ\s\-', null],
        //contest_form
        'title' => [2, 100, null, null],
        'link' => [2, 100, 'a-z0-9\-', null],
        'answer' => [1, 100, null, null],
        'question' => [5, 200, null, null],
        'answer_form' => [1, 50, null, null],
        'prize' => [1, 500, null, null],
        'opinion_comment' => [0, 200, 'a-zA-Z0-9ążśźęćńółĄŻŚŹĘĆŃÓŁ\s\-\.,\/=\+\(\)\[\]\"\'\?\\\!%\^<>:;\$&\*', null],
        'select_winner_text' => [1, 1000, null, null]
    ];
    if (!is_array($parametrs)) {
        if ($parametrs=='email') {
            if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                if (empty($database)) {
                    throw new Exception('NIE PRZEKAZANO OBIEKTU BAZY DANYCH!');
                }
                $query = $database->selectWithWhere('SELECT email FROM users WHERE lower(email)=lower(?)', [$value]);
                if (!empty($query)) {
                    throw new Exception('Podany adres e-mail jest już używany!');
                } else {
                    return true;
                }
            }
            else
            throw new Exception('Podany adres e-mail jest błędny!');
        }
        foreach ($defined as $definedInput => $definedParametrs)
        {
            if ($definedInput == $parametrs) {
                $parametrs = $definedParametrs;
            }
        }
        if (!is_array($parametrs)) {
            throw new Exception('BŁĘDNA NAZWA LUB PARAMETRY POLA!');
        }
    }
    if (!empty($parametrs[0])) {
        if (mb_strlen($value) < $parametrs[0]) {
            throw new Exception('Wartość pola '.$label.' jest za krótka!');
        }
    }
    if (!empty($parametrs[1])) {
        if (mb_strlen($value) > $parametrs[1]) {
            throw new Exception('Wartość pola '.$label.' jest za długa!');
        }
    }
    if (!empty($parametrs[2])) {
        if (!preg_match('/^['.$parametrs[2].']+$/u', $value)) {
            throw new Exception('Wartość pola '.$label.' zawiera niedozwolone znaki!');
        }
    }
    if (!empty($parametrs[3])) {
        if (empty($database)) {
            throw new Exception('NIE PRZEKAZANO OBIEKTU BAZY DANYCH!');
        }
        $queryParametrs = explode('.', $parametrs[3]);
        $query = $database->selectWithWhere('SELECT '.$queryParametrs[1].' FROM '.$queryParametrs[0].' WHERE lower('.$queryParametrs[1].')=lower(?)', [$value]);
        if (!empty($query)) {
            throw new Exception('Podany '.$label.' jest już używany!');
        }
    }
    return true;
}

function clearString($string)
{
    $string = trim($string);
    $string = preg_replace('!\s+!', ' ', $string);
    $string = htmlentities($string);

    return $string;
}
