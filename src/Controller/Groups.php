<?php
namespace Qtbat\Controller;

use Qtbat\Engine\Controller;
use Qtbat\Exception\PageError;

class Groups extends Controller
{

	public function contestsList($group_name)
	{
		$group = new \Qtbat\Model\Groups($this->database, $this->user);
		$data = $group->data($group_name[1]);
		$members_stats = $group->membersStats($data['group_id']);
		if (isset($group_name[3])) {
			$contests_list_href = $group_name[3];
		} else {
			$contests_list_href = null;
		}

		$template = $this->twig->loadTemplate('groups/contests_list.html.twig');
		$template->display([
			'notifications' => $this->user->getNotifications(),
			'user' => $this->user->getData(),
			'data' => $data['data'],
			'user_info' => $data['user_info'],
			//count for displaying proper message if admin is leaving group
			'count' => ['admins' => $members_stats['count_admins'], 'group_members' => $members_stats['count_group_members']],
			'allow_join' => $data['allow_join'],
			'href' => 'contests-list',
			'contests_list_href' => $contests_list_href
		]);
	}

	public function loadContestsList($group_name)
	{
		if (!isset($_POST['current_page']) && !isset($_POST['start'])) {
			throw new PageError(404);
		} elseif (filter_input(INPUT_POST, 'current_page', FILTER_VALIDATE_INT)===false || filter_input(INPUT_POST, 'start', FILTER_VALIDATE_INT)===false) {
			throw new PageError(404);
		} else {
			$group = new \Qtbat\Model\Groups($this->database, $this->user);
			$data = $group->data($group_name[1]);
			$display_contests_list = $group->displayContestsList($group_name[1], $_POST['start'], $_POST['href'], $_POST['current_page']);

			$template = $this->twig->loadTemplate('/groups/load_contests_list.html.twig');
			$html_response = $template->render([
				'group_contests' => $display_contests_list['contests_list'],
				'user_info' => $data['user_info']
			]);

			echo json_encode(['contests_list' => $html_response, 'start' => $display_contests_list['start'], 'stop' => $display_contests_list['stop']]);
		}
	}

	public function administratorPanel($group_name)
	{
		$group = new \Qtbat\Model\Groups($this->database, $this->user);
		$data = $group->data($group_name[1]);
		if ($data['user_info']['rank']!=1)
			throw new PageError(403);

		$members_stats = $group->membersStats($data['group_id']);

		$template = $this->twig->loadTemplate('groups/administrator_panel.html.twig');
		$template->display([
			'notifications' => $this->user->getNotifications(),
			'user' => $this->user->getData(),
			'data' => $data['data'],
			'user_info' => $data['user_info'],
			'count' => ['admins' => $members_stats['count_admins'], 'group_members' => $members_stats['count_group_members']],
			'href' => 'administrator-panel'
		]);
	}

	public function membersList($group_name)
	{
		$group = new \Qtbat\Model\Groups($this->database, $this->user);
		$data = $group->data($group_name[1]);
		$members_stats = $group->membersStats($data['group_id']);

		$template = $this->twig->loadTemplate('groups/members_list.html.twig');
		$template->display([
			'notifications' => $this->user->getNotifications(),
			'user' => $this->user->getData(),
			'data' => $data['data'],
			'user_info' => $data['user_info'],
			'count' => ['admins' => $members_stats['count_admins'], 'group_members' => $members_stats['count_group_members']],
			'allow_join' => $data['allow_join'],
			'href' => 'members-list'
		]);
	}

	public function loadMembersList($group_name)
	{
		if (!isset($_POST['start']) && !isset($_POST['search_value'])) {
			throw new PageError(404);
		} elseif (filter_input(INPUT_POST, 'start', FILTER_VALIDATE_INT)===false) {
			throw new PageError(404);
		} else {
			$group = new \Qtbat\Model\Groups($this->database, $this->user);
			$data = $group->data($group_name[1]);
			$search_value = strtolower(filter_input(INPUT_POST, 'search_value', FILTER_SANITIZE_SPECIAL_CHARS));
			$display_members = $group->displayMembers($group_name[1], $_POST['start'], $search_value);

			$template = $this->twig->loadTemplate('/groups/load_members_list.html.twig');
			$html_response = $template->render([
				'user' => $this->user->getData(),
				'data' => $data['data'],
				'user_info' => $data['user_info'],
				'members_list' => $display_members['members_list'],
				'avatar_path' => $display_members['avatar_path'],
				'is_empty' => $display_members['is_empty']
			]);

			echo json_encode(['members_list' => $html_response, 'start' => $display_members['start'], 'stop' => $display_members['stop']]);
		}
	}

	public function userInteraction($group_name)
	{
		$group = new \Qtbat\Model\Groups($this->database, $this->user);
		$results = $group->userInteraction($group_name);
	}

	public function groupEdit($group_name)
	{
		$group = new \Qtbat\Model\Groups($this->database, $this->user);
		$results = $group->edit($group_name);
	}

	public function createGroup()
	{
        if (!$this->user->isLogged())
            throw new PageError(403);

		$template = $this->twig->loadTemplate('groups/create_group.html.twig');
		$template->display([
			'notifications' => $this->user->getNotifications(),
			'user' => $this->user->getData()
		]);
	}

	public function createGroupAction()
	{
		if (!$this->user->isLogged())
			throw new PageError(404);

		if (isset($_POST['unique_name'])) {
			try {
				if (!isset($_POST['unique_name']) || !isset($_POST['name']) || !isset($_POST['type'])) {
					throw new \Exception('Wystąpił błąd');
				} else {
					if ($_POST['type']!='0' && $_POST['type']!='1') {
						throw new \Exception('Wystąpił błąd');
					} else {
						$message = null;
						$status = 'success';
						$group = new \Qtbat\Model\Groups($this->database, $this->user);
						$results = $group->createGroup();
					}
				}
			} catch (\Exception $e) {
				$message = $e->getMessage();
				$status = 'error';
			}
			$json = array('message'=>$message, 'status'=>$status);
			echo json_encode($json);
		} else
			throw new PageError(404);
	}

	public function invitationConfirm()
	{
		if (!$this->user->isLogged())
			throw new PageError(404);

		$groups = new \Qtbat\Model\Groups($this->database, $this->user);
		$groups->invitationConfirm();
	}
}
