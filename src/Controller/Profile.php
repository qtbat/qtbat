<?php
namespace Qtbat\Controller;

use Qtbat\Engine\Controller;
use Qtbat\Exception\PageError;

class Profile extends Controller
{
    public function show($login)
	{
        $profile = new \Qtbat\Model\Profile($this->database, $this->user);
        $groups = new \Qtbat\Model\Groups($this->database, $this->user);
        $groups_data = $groups->panelGroups();
        $profile_info = $profile->get($login[1]);

        if (empty($profile_info))
            throw new PageError(404);

        $opinions_list = $profile->usersOpinions($profile_info[0]['id_user']);

        $template = $this->twig->loadTemplate('profile/profile.html.twig');
        $template->display([
            'notifications' => $this->user->getNotifications(),
            'profile' => $profile_info[0],
            'is_logged' => $this->user->isLogged(),
            'user' => $this->user->getData(),
            'opinions' => $opinions_list['opinions'],
            'positive' => $opinions_list['positive'],
            'negative' => $opinions_list['negative'],
            'groups_list' => $groups_data['groups_list']
        ]);
    }

	public function settings()
    {
		if (!$this->user->isLogged())
            throw new PageError(403);

        $loggeduser_Id = $this->user->getId();

        $profile = new \Qtbat\Model\Profile($this->database, $this->user);
		$results = $profile->settings($loggeduser_Id);
        $groups = new \Qtbat\Model\Groups($this->database, $this->user);
        $groups_data = $groups->panelGroups();

		$avatar_name = $this->user->getAvatarPath($loggeduser_Id);

		$template = $this->twig->loadTemplate('profile/profile_settings.html.twig');
		$template->display([
            'notifications' => $this->user->getNotifications(),
			'user' => $this->user->getData(),
			'profile' => $results,
			'avatar_name' => $avatar_name,
            'groups_list' => $groups_data['groups_list']
		]);

	}

	public function uploadAvatar() {

        if (!$this->user->isLogged())
            throw new PageError(404);

		if (isset($_FILES['avatar'])) {
			try {
				$message = true;
				$status = 'success';
				$loggeduser_Id = $this->user->getId();
				$uploadfile = new \Qtbat\Engine\UploadFile($this->database);
				$uploadfile->uploadAvatar($loggeduser_Id, $_FILES['avatar']);
			} catch (\Exception $e) {
				$message = $e->getMessage();
				$status = 'error';
			}
			$json = array('message'=>$message, 'status'=>$status);
			echo json_encode($json);
		}
		else
			throw new PageError(404);
	}

	public function edit()
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        $profile = new \Qtbat\Model\Profile($this->database, $this->user);
        $profile->edit();
	}

    public function loggedUserContests()
    {
        if (!$this->user->isLogged())
            throw new PageError(403);

        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $groups = new \Qtbat\Model\Groups($this->database, $this->user);
        $groups_data = $groups->panelGroups();

        $template = $this->twig->loadTemplate('profile/user_contests.html.twig');
        $template->display([
            'notifications' => $this->user->getNotifications(),
            'user' => $this->user->getData(),
            'groups_list' => $groups_data['groups_list']
        ]);
    }

    public function loadLoggedUserContests()
    {
        if (!isset($_POST['start']) && !isset($_POST['page']))
            throw new PageError(404);

        if (filter_input(INPUT_POST, 'start', FILTER_VALIDATE_INT)===false)
            throw new PageError(404);

        if (filter_input(INPUT_POST, 'page', FILTER_VALIDATE_INT)===false || $_POST['page']<1)
            throw new PageError(404);


        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $display_contests = $display_contest->displayLoggedUserContests($_POST['start'], $_POST['page']);

        $template = $this->twig->loadTemplate('profile/load_user_contests.html.twig');
        $html_response = $template->render([
            'user' => $this->user->getData(),
            'list' => $display_contests['contests_list']
        ]);

        echo json_encode(['contests_list' => $html_response, 'start' => $display_contests['start'], 'stop' => $display_contests['stop']]);
    }
}
