<?php
namespace Qtbat\Controller;

use Qtbat\Engine\Controller;
use Qtbat\Engine\Config;
use Qtbat\Exception\PageError;

class File extends Controller
{
    public function show($matches)
    {
        $hash = $matches[1];
        $type = strtolower($matches[2]);
        if (!empty($hash) && !empty($type)) {
            $result = $this->database->selectWithWhere('SELECT disk, folder FROM uploaded_files WHERE name=?', [$hash.'.'.$type]);
            if (isset($result[0]) && !empty($result[0])) {
                $result = $result[0];
                $allowTypes = ['jpg', 'jpeg', 'png', 'gif'];
                if (in_array($type, $allowTypes)) {
                    $disks = new Config('file_management_config.php');
                    $disk = $disks->get($result['disk'], 'path');
                    if (!empty($disk)) {
                        $file = $disk.$result['folder'].'/'.$hash.'.'.$type;;
                        if (file_exists($file)) {
                            header('Content-Type: image/'.$type);
                            list($width, $height) = getimagesize($file);
                            $resize = false;
                            if (isset($matches[4]) && !empty($matches[4])) {
                                $resize = true;
                                $newWidth = $matches[4];
                                $newHeight = $matches[5];
                            }
                            if (isset($matches[6]) && !empty($matches[6])) {
                                $resize = true;
                                $newWidth = $matches[6];
                                $newHeight = $height;
                                $ratio = $width/$height;
                                if ($newWidth/$newHeight > $ratio) {
                                    $newWidth = $newHeight*$ratio;
                                } else {
                                    $newHeight = $newWidth/$ratio;
                                }
                            }
                            if (isset($matches[7]) && !empty($matches[7])) {
                                $resize = true;
                                $newWidth = $width;
                                $newHeight = $matches[7];
                                $ratio = $width/$height;
                                if ($newWidth/$newHeight > $ratio) {
                                    $newWidth = $newHeight*$ratio;
                                } else {
                                    $newHeight = $newWidth/$ratio;
                                }
                            }
                            if ($resize === true) {
                                switch ($type) {
                                    case 'jpg':
                                    case 'jpeg':
                                        $original = imagecreatefromjpeg($file);
                                        break;
                                    case 'png':
                                        $original = imagecreatefrompng($file);
                                        break;
                                    case 'gif':
                                        $original = imagecreatefromgif($file);
                                        break;
                                }
                                if (isset($original)) {
                                    $new = imagecreatetruecolor($newWidth, $newHeight);
                                    imagecopyresampled($new, $original, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
                                    switch ($type) {
                                        case 'jpg':
                                        case 'jpeg':
                                            imagejpeg($new);
                                            break;
                                        case 'png':
                                            imagepng($new);
                                            break;
                                        case 'gif':
                                            imagegif($new);
                                            break;
                                    }
                                }
                            } else {
                                readfile($file);
                            }
                            exit();
                        } else {
                            $error = true;
                        }
                    } else {
                        $error = true;
                    }
                } else {
                    $error = true;
                }
            } else {
                $error = true;
            }
        } else {
            $error = true;
        }
        if ($error === true) {
            throw new PageError(404);
        }
    }

    public function download($matches)
    {
        $hash = $matches[1];
        $type = strtolower($matches[2]);
        if (!empty($hash) && !empty($type)) {
            $result = $this->database->selectWithWhere('SELECT original_name, disk, folder FROM uploaded_files WHERE name=?', [$hash.'.'.$type]);
            if (isset($result[0]) && !empty($result[0])) {
                $result = $result[0];
                $disks = new Config('file_management_config.php');
                $disk = $disks->get($result['disk'], 'path');
                if (!empty($disk)) {
                    $file = $disk.$result['folder'].'/'.$hash.'.'.$type;
                    if (file_exists($file)) {
                        header('Content-Description: File Transfer');
                        header('Content-Type: application/octet-stream');
                        header('Content-Disposition: attachment; filename="'.$result['original_name'].'"');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: '.filesize($file));
                        readfile($file);
                        exit();
                    } else {
                        $error = true;
                    }
                } else {
                    $error = true;
                }
            } else {
                $error = true;
            }
        } else {
            $error = true;
        }
        if ($error === true) {
            throw new PageError(404);
        }
    }
}
