<?php
namespace Qtbat\Controller;

use Qtbat\Engine\Controller;
use Qtbat\Exception\PageError;
use Exception;

class PrivateMessage extends Controller
{
    public function newMessage($matches)
    {
        if (!$this->user->isLogged()) {
            throw new PageError(403);
        }
        if (!empty($matches)) {
            $user = new \Qtbat\Model\Profile($this->database, $this->user);
            if (empty($user->get($matches[1]))) {
                throw new PageError(404);
            }
        }
        $privateMessage = new \Qtbat\Model\PrivateMessage($this->database, $this->user);
        $privateMessage->prepare($matches[1]);
        $replyTopic = '';
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && !isset($_POST['replyTopic'])) {
            try {
                $privateMessage->send();
                echo json_encode(['status' => '1', 'message' => 'Twoja wiadomość została pomyślnie wysłana!']);
            } catch (Exception $e) {
                echo json_encode(['status' => '0', 'message' => 'Nie można teraz wysłać wiadomości! '.$e->getMessage()]);
            }
            exit();
        } elseif (!empty($_POST['replyTopic'])) {
            $replyTopic = trim(filter_input(INPUT_POST, 'replyTopic'));
            if (mb_substr($replyTopic, 0, 3) !== 'Re:') {
                $replyTopic = 'Re: '.$replyTopic;
            }
        }
        $template = $this->twig->loadTemplate('message/send.html.twig');
        $template->display([
            'notifications' => $this->user->getNotifications(),
            'user' => $this->user->getData(),
            'to' => $matches[1],
            'replyTopic' => $replyTopic
        ]);
    }

    public function showMessage($data = null)
    {
        if (!$this->user->isLogged()) {
            throw new PageError(403);
        }
        $messages = new \Qtbat\Model\PrivateMessage($this->database, $this->user);
        $message = $messages->get($data);
        if (empty($message)) {
            throw new PageError(404);
        }
        echo json_encode($message);
        exit();
    }

    public function deleteMessage()
    {
        if (!$this->user->isLogged()) {
            throw new PageError(403);
        }
        $messages = new \Qtbat\Model\PrivateMessage($this->database, $this->user);
        $messageId = filter_input(INPUT_POST, 'message', FILTER_VALIDATE_INT);
        if ($messageId > 0) {
            $deleted = $messages->delete($messageId);
            if ($deleted === true) {
                echo json_encode(['deleted' => $messageId]);
            } else {
                echo json_encode(['deleted' => 0]);
            }
        } else {
            echo json_encode(['deleted' => 0]);
        }
        exit();
    }

    public function getList($withUser = null)
    {
        if (!$this->user->isLogged()) {
            throw new PageError(403);
        }
        if (!empty($withUser)) {
            $user = new \Qtbat\Model\Profile($this->database, $this->user);
            if (empty($user->get($withUser[1]))) {
                throw new PageError(404);
            }
        }
        $messages = new \Qtbat\Model\PrivateMessage($this->database, $this->user);
        $list = $messages->getList($withUser);
        $template = $this->twig->loadTemplate('message/list.html.twig');
        $template->display([
            'notifications' => $this->user->getNotifications(),
            'user' => $this->user->getData(),
            'with' => isset($withUser[1]) ? $withUser[1] : '',
            'messages' => $list
        ]);
    }

    public function getListSent($withUser = null)
    {
        if (!$this->user->isLogged()) {
            throw new PageError(403);
        }
        if (!empty($withUser)) {
            $user = new \Qtbat\Model\Profile($this->database, $this->user);
            if (empty($user->get($withUser[1]))) {
                throw new PageError(404);
            }
        }
        $messages = new \Qtbat\Model\PrivateMessage($this->database, $this->user);
        $list = $messages->getListSent($withUser);
        $template = $this->twig->loadTemplate('message/list_of_sent.html.twig');
        $template->display([
            'notifications' => $this->user->getNotifications(),
            'user' => $this->user->getData(),
            'with' => isset($withUser[1]) ? $withUser[1] : '',
            'messages' => $list
        ]);
    }
}
