<?php
namespace Qtbat\Controller;

use Qtbat\Engine\Controller;
use Qtbat\Exception\PageError;

class CreateContest extends Controller
{
    public function createContest()
    {
        if (!$this->user->isLogged())
            throw new PageError(403);

        $create_contest = new \Qtbat\Model\CreateContest($this->database, $this->user);
        $results = $create_contest->display();

        $template = $this->twig->loadTemplate('/create_contest/create_contest.html.twig');
        $template->display([
            'notifications' => $this->user->getNotifications(),
            'user' => $this->user->getData(),
            'groups_list' => $results['groups_list'],
            'categories' => $results['categories']
        ]);
    }

    public function appendElement()
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        $loggeduser_Id = $this->user->getId();

        if (isset($_POST['first_come_first_served'])) {
            echo file_get_contents(DIR.'templates/create_contest/first_come_first_served.html.twig');
        } elseif (isset($_POST['first_few'])) {
            echo file_get_contents(DIR.'templates/create_contest/first_few.html.twig');
        } elseif (isset($_POST['draw'])) {
            echo file_get_contents(DIR.'templates/create_contest/draw.html.twig');
        } elseif (isset($_POST['select_winner'])) {
            echo file_get_contents(DIR.'templates/create_contest/select_winner.html.twig');
        } elseif (isset($_POST['tournament'])) {
            echo file_get_contents(DIR.'templates/create_contest/tournament.html.twig');
        } else
            throw new PageError(404);
    }

    public function sendForm()
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        if (empty($_POST))
            throw new PageError(404);

        $loggeduser_Id = $this->user->getId();
        $uploadfile = new \Qtbat\Engine\UploadFile($this->database);
        $create_contest = new \Qtbat\Model\CreateContest($this->database, $this->user);

        try {
            $values = array();

            if (isset($_POST['avaible_for'])) {
                if ($_POST['avaible_for']=='groups') {
                    if (isset($_POST['select_groups'])) {
                        $avaible_for = 1;
                        $values['groups'] = $_POST['select_groups'];
                    } else {
                        throw new \Exception('Wygląda na to, że nie uzupełniłeś/aś formularza do końca :)');
                    }
                } elseif ($_POST['avaible_for']=='normal') {
                        $avaible_for = 2;
                } else {
                    throw new \Exception('Wystąpił błąd z poprawnym wczytaniem twojego formularza. Przeładuj stronę i spróbuj ponownie.');
                }
                $values['avaible_for'] = $avaible_for;

                if (isset($_POST['title'])) {
                    if (empty($_POST['title'])) {
                        throw new \Exception('Tytuł nie może być pusty!');
                    } else {
                        validateInput($_POST['title'], 'tytuł', 'title');
                        $values['title'] = clearString($_POST['title']);
                    }
                } else {
                    throw new Exception('Wygląda na to, że nie uzupełniłeś/aś formularza do końca :)');
                }

                if (isset($_POST['link'])) {
                    if (empty($_POST['link'])) {
                        $link = iconv('utf-8', 'ascii//TRANSLIT', $_POST['title']);
                        $link = trim($link);
                        $link = str_replace(' ', '-', $link);
                        $link = preg_replace('/[^A-Za-z0-9\-]/', '', $link);
                        $link = preg_replace('/-+/', '-', $link);
                        $values['link'] = strtolower($link);
                    } else {
                        $values['link'] = strtolower($_POST['link']);
                        validateInput($values['link'], 'link', 'link');
                    }
                }
                if (isset($_POST['description'])) {
                    if (empty($_POST['description'])) {
                        $values['description'] = NULL;
                    } else {
                        $values['description'] = $_POST['description'];
                    }
                } else {
                    throw new \Exception('Wystąpił błąd z poprawnym wczytaniem twojego formularza. Przeładuj stronę i spróbuj ponownie.');
                }
                if (isset($_POST['select_category'])) {
                    $count_categories = $create_contest->countCategories();
                    if (ctype_digit($_POST['select_category']) && (int)$_POST['select_category']<=$count_categories && (int)$_POST['select_category']>0) {
                        $values['category'] = $_POST['select_category'];
                    } else {
                        throw new \Exception('Wystąpił błąd z poprawnym wczytaniem twojego formularza. Przeładuj stronę i spróbuj ponownie.');
                    }
                } else {
                    throw new \Exception('Wystąpił błąd z poprawnym wczytaniem twojego formularza. Przeładuj stronę i spróbuj ponownie.');
                }

                if (isset($_POST['contest_type'])) {

                    if ($_POST['contest_type']=='first_come_first_served' || $_POST['contest_type']=='first_few') {
                        $values['contest_type'] = 1;

                        if (isset($_POST['question'])) {
                            if (empty($_POST['question'])) {
                                throw new \Exception('Pytanie nie może być puste!');
                            } else {
                                $values['question'] = clearString($_POST['question']);
                                validateInput($values['question'], 'pytanie', 'question');
                            }
                        } else {
                            throw new \Exception('Wygląda na to, że nie uzupełniłeś/aś formularza do końca :)');
                        }

                        if (isset($_POST['answer'])) {
                            if (empty($_POST['answer'])) {
                                throw new \Exception('Odpowiedź nie może być pusta!');
                            } else {
                                $values['answer'] = clearString($_POST['answer']);
                                validateInput($values['answer'], 'odpowiedź', 'answer');
                            }
                        } else {
                            throw new \Exception('Wygląda na to, że nie uzupełniłeś/aś formularza do końca :)');
                        }

                        if (isset($_POST['answer_form'])) {
                            if (empty($_POST['answer_form'])) {
                                throw new \Exception('Forma odpowiedzi nie może być pusta!');
                            } else {
                                $values['answer_form'] = clearString($_POST['answer_form']);
                                validateInput($values['answer_form'], 'forma odpowiedzi', 'answer_form');
                            }
                        } else {
                            throw new \Exception('Wygląda na to, że nie uzupełniłeś/aś formularza do końca :)');
                        }

                        if ($_POST['contest_type']=='first_few') {
                            if (isset($_POST['number_of_winners'])) {
                                if (filter_input(INPUT_POST, 'number_of_winners', FILTER_VALIDATE_INT)!==false) {
                                    if ($_POST['number_of_winners']>5) {
                                        throw new \Exception('Maksymalna ilość zwycięzców wynosi 5.');
                                    } elseif ($_POST['number_of_winners']<2) {
                                        throw new \Exception('Minimalna ilośc zwycięzców wynosi 2. Jeśli chcesz udostępnić nagrodę dla jednego zwycięzcy, skorzystaj z "Kto pierwszy ten lepszy" :)');
                                    } else {
                                        $values['number_of_winners'] = clearString($_POST['number_of_winners']);
                                    }
                                } else {
                                    throw new \Exception('Ilość zwycięzców musi być liczbą całkowitą!');
                                }
                            } else {
                                throw new \Exception('Wygląda na to, że nie uzupełniłeś/aś formularza do końca :)');
                            }
                        } else {
                            $values['number_of_winners'] = 1;
                        }

                    } elseif ($_POST['contest_type']=='draw') {
                        $values['contest_type'] = 2;
                        if (isset($_POST['requirement'])) {
                            if ($_POST['requirement']=='picture') {
                                $values['requirement'] = 1;
                            } elseif ($_POST['requirement']=='video') {
                                $values['requirement'] = 2;
                            } elseif ($_POST['requirement']=='text') {
                                $values['requirement'] = 3;
                            } elseif ($_POST['requirement']=='file') {
                                $values['requirement'] = 4;
                            } elseif ($_POST['requirement']=='none') {
                                $values['requirement'] = null;
                            } else {
                                throw new \Exception('Wystąpił błąd');
                            }

                            if (isset($_POST['method'])) {
                                if ($_POST['method']=='number_of_applications') {
                                    if (isset($_POST['number_of_applications'])) {
                                        if (filter_input(INPUT_POST, 'number_of_applications', FILTER_VALIDATE_INT)!==false) {

                                            $values['method'] = 3;
                                            $values['number_of_applications'] = $_POST['number_of_applications'];
                                        } else {
                                            throw new \Exception('Ilość osób, które mogą się zgłosić musi być liczbą całkowitą');
                                        }
                                    } else {
                                        throw new \Exception('Uzupełnij formularz do końca');
                                    }
                                } elseif ($_POST['method']=='button_click') {
                                    $values['method'] = 2;
                                    $values['number_of_applications'] = null;
                                } elseif ($_POST['method']=='date') {
                                    $values['method'] = 1;
                                    $values['number_of_applications'] = null;
                                } else {
                                    throw new \Exception('Wystąpił błąd');
                                }

                                if (isset($_POST['number_of_winners'])) {
                                    if (filter_input(INPUT_POST, 'number_of_winners', FILTER_VALIDATE_INT)!==false) {
                                        if ($_POST['number_of_winners']>4) {
                                            throw new \Exception('Maksymalna ilość zwycięzców wynosi 4.');
                                        } elseif ($_POST['number_of_winners']<1) {
                                            throw new \Exception('Minimalna ilośc zwycięzców wynosi 1.');
                                        } else {
                                            $values['number_of_winners'] = clearString($_POST['number_of_winners']);
                                        }
                                    } else {
                                        throw new \Exception('Ilość zwycięzców musi być liczbą całkowitą!');
                                    }
                                } else {
                                    throw new \Exception('Wygląda na to, że nie uzupełniłeś/aś formularza do końca :)');
                                }
                            } else {
                                throw new \Exception('Uzupełnij formularz do końca');
                            }

                        } else {
                            throw new \Exception('Musisz uzupełnić formularz do końca');
                        }

                    } elseif ($_POST['contest_type']=='select_winner') {
                        $values['contest_type'] = 3;

                        if (isset($_POST['requirements'])) {
                            if (is_array($_POST['requirements'])) {
                                $picture_requirement = in_array('picture', $_POST['requirements']);

                                if ($picture_requirement) {
                                    if (isset($_POST['picture_dimensions'])) {
                                        if ($_POST['picture_dimensions']=='none') {
                                            $values['requirements']['picture'] = [
                                                'min_width'=>5,
                                                'min_height'=>5,
                                                'max_width'=>1000,
                                                'max_height'=>1000
                                            ];
                                        } elseif ($_POST['picture_dimensions']=='custom') {
                                            if (isset($_POST['min_width'])) {
                                                if ($_POST['min_width']!="") {
                                                    if (filter_input(INPUT_POST, 'min_width', FILTER_VALIDATE_INT)!==false) {
                                                        $min_width = filter_input(INPUT_POST, 'min_width', FILTER_VALIDATE_INT);
                                                        if ($min_width<5 || $min_width>1000) {
                                                            throw new \Exception('Minimalna dopuszczalna szerokość obrazka nie mieści się w dopuszczalnym przedziale (5px-1000px)');
                                                        }
                                                    } else {
                                                        throw new \Exception('Minimalna szerokość obrazka musi być liczbą całkowitą');
                                                    }
                                                } else {
                                                    throw new \Exception('Pole minimalna szerokość obrazka nie zostało uzupełnione');
                                                }
                                            } else {
                                                throw new \Exception('Wystąpił błąd');
                                            }
                                            if (isset($_POST['min_height'])) {
                                                if ($_POST['min_height']!="") {
                                                    if (filter_input(INPUT_POST, 'min_height', FILTER_VALIDATE_INT)!==false) {
                                                        $min_height = filter_input(INPUT_POST, 'min_height', FILTER_VALIDATE_INT);
                                                        if ($min_height<5) {
                                                            throw new \Exception('Minimalna dopuszczalna wysokość obrazka nie mieści się w dopuszczalnym przedziale (5px-1000px)');
                                                        }
                                                    } else {
                                                        throw new \Exception('Minimalna wysokość obrazka musi być liczbą całkowitą');
                                                    }
                                                } else {
                                                    throw new \Exception('Pole minimalna wysokość obrazka nie zostało uzupełnione');
                                                }
                                            } else {
                                                throw new \Exception('Wystąpił błąd');
                                            }
                                            if (isset($_POST['max_width'])) {
                                                if ($_POST['max_width']!="") {
                                                    if (filter_input(INPUT_POST, 'max_width', FILTER_VALIDATE_INT)!==false) {
                                                        $max_width = filter_input(INPUT_POST, 'max_width', FILTER_VALIDATE_INT);
                                                        if ($max_width>1000) {
                                                            throw new \Exception('Maksymalna dopuszczalna szerokość obrazka nie mieści się w dopuszczalnym przedziale (5px-1000px)');
                                                        }
                                                    } else {
                                                        throw new \Exception('Maksymalna szerokość obrazka musi być liczbą całkowitą');
                                                    }
                                                } else {
                                                    throw new \Exception('Pole maksymalna szerokość obrazka nie zostało uzupełnione');
                                                }
                                            } else {
                                                throw new \Exception('Wystąpił błąd');
                                            }
                                            if (isset($_POST['max_height'])) {
                                                if ($_POST['max_height']!="") {
                                                    if (filter_input(INPUT_POST, 'max_height', FILTER_VALIDATE_INT)!==false) {
                                                        $max_height = filter_input(INPUT_POST, 'max_height', FILTER_VALIDATE_INT);
                                                        if ($min_width>1000) {
                                                            throw new \Exception('Maksymalna dopuszczalna wysokość obrazka nie mieści się w dopuszczalnym przedziale (5px-1000px)');
                                                        }
                                                    } else {
                                                        throw new \Exception('Maksymalna wysokość obrazka musi być liczbą całkowitą');
                                                    }
                                                } else {
                                                    throw new \Exception('Pole maksymalna wysokość obrazka nie zostało uzupełnione');
                                                }
                                            } else {
                                                throw new \Exception('Wystąpił błąd');
                                            }

                                            if ($min_width>$max_width) {
                                                throw new \Exception('Minimalna szerokość obrazka nie może być większa od maksymalnej szerokości obrazka');
                                            }
                                            if ($min_height>$max_height) {
                                                throw new \Exception('Minimalna wysokość obrazka nie może być większa od maksymalnej wysokości obrazka');
                                            }

                                            $values['requirements']['picture'] = [
                                                'min_width'=>$min_width,
                                                'min_height'=>$min_height,
                                                'max_width'=>$max_width,
                                                'max_height'=>$max_height
                                            ];
                                        } else {
                                            throw new \Exception('Wystąpił błąd');
                                        }
                                    } else {
                                        throw new \Exception('Formularz nie jest do końca uzupełniony');
                                    }
                                }

                                $video_requirement = in_array('video', $_POST['requirements']);

                                if ($video_requirement) {
                                    $values['requirements']['video'] = true;
                                }

                                $text_requirement = in_array('text', $_POST['requirements']);

                                if ($text_requirement) {
                                    $values['requirements']['text'] = true;
                                }

                                $files_requirement = in_array('files', $_POST['requirements']);

                                if ($files_requirement) {
                                    if (isset($_POST['custom_file_extensions'])) {
                                        if ($_POST['custom_file_extensions']=='yes') {
                                            if (isset($_POST['permitted_file_extensions'])) {
                                                $extensions = explode(" ", $_POST['permitted_file_extensions']);

                                                if (count($extensions)<1) {
                                                    throw new \Exception('Musisz podać przynajmniej jedno rozszerzenie pliku');
                                                } elseif (count($extensions)>15) {
                                                    throw new \Exception('Maksymalna ilość rozszerzeń wynosi 15');
                                                } else {
                                                    foreach ($extensions as $extension) {
                                                        if (!preg_match('/^(\.[a-z0-9]{1,6})$/', $extension)) {
                                                            throw new \Exception('Jedno z podanych rozszerzeń jest niedozwolone');
                                                        }
                                                    }

                                                    $values['requirements']['files'] = [
                                                        'extensions'=>$_POST['permitted_file_extensions']
                                                    ];
                                                }
                                            } else {
                                                throw new \Exception('Formularz nie został uzupełniony do końca');
                                            }
                                        } elseif ($_POST['custom_file_extensions']=='no') {
                                            $values['requirements']['files'] = true;
                                        } else {
                                            throw new \Exception('Wystąpił błąd');
                                        }
                                    } else {
                                        throw new \Exception('Formularz nie został uzupełniony do końca');
                                    }
                                }

                                if (!isset($values['requirements']['picture']) && !isset($values['requirements']['video']) && !isset($values['requirements']['text']) && !isset($values['requirements']['files'])) {
                                    throw new \Exception('Musisz wybrać przynajmniej jedno wymaganie konkursowe.');
                                }

                                $values['requirements'] = json_encode($values['requirements']);

                                if (isset($_POST['number_of_winners'])) {
                                    if (filter_input(INPUT_POST, 'number_of_winners', FILTER_VALIDATE_INT)!==false) {
                                        if ($_POST['number_of_winners']>4) {
                                            throw new \Exception('Maksymalna ilość zwycięzców wynosi 4.');
                                        } elseif ($_POST['number_of_winners']<1) {
                                            throw new \Exception('Minimalna ilośc zwycięzców wynosi 1.');
                                        } else {
                                            $values['number_of_winners'] = clearString($_POST['number_of_winners']);
                                        }
                                    } else {
                                        throw new \Exception('Ilość zwycięzców musi być liczbą całkowitą!');
                                    }
                                } else {
                                    throw new \Exception('Wygląda na to, że nie uzupełniłeś/aś formularza do końca :)');
                                }
                            } else {
                                throw new \Exception('Wystąpił błąd');
                            }

                        } else {
                            throw new \Exception('Musisz wybrać przynajmniej jedno wymaganie konkursowe.');
                        }

                        if (isset($_POST['time'])) {
                            if ($_POST['time']=='yes') {
                                $values['limit_submission_date'] = 1;
                            } elseif ($_POST['time']=='no') {
                                $values['limit_submission_date'] = 0;
                            } else {
                                throw new \Exception('Wystąpił błąd');
                            }
                        } else {
                            throw new \Exception('Formularz nie został uzupełniony do końca');
                        }

                    } elseif ($_POST['contest_type']=='tournament') {
                        $values['contest_type'] = 4;

                    } else {
                        throw new \Exception('Wystąpił błąd z poprawnym wczytaniem twojego formularza. Przeładuj stronę i spróbuj ponownie.');
                    }
                } else {
                    throw new \Exception('Wygląda na to, że nie uzupełniłeś/aś formularza do końca :)');
                }
            } else {
                throw new \Exception('Wygląda na to, że nie uzupełniłeś/aś formularza do końca :)');
            }

            if (isset($_POST['delivery'])) {
                if ($_POST['delivery']=='by_author') {
                    $prize_delivery = 1;
                    $values['prize'] = NULL;
                } elseif ($_POST['delivery']=='automatic') {
                    $prize_delivery = 2;
                    if (isset($_POST['prize'])) {

                        if (is_array($_POST['prize'])) {
                            $is_prize_not_null = true;

                            for ($i=0; $i<=count($_POST['prize'])-1; $i++) {
                                if (!isset($_POST['prize'][$i])) {
                                    throw new \Exception('Wystąpił błąd');
                                } else {
                                    $_POST['prize'][$i] = clearString($_POST['prize'][$i]);
                                    if (clearString($_POST['prize'][$i]=="")) {
                                        $is_prize_not_null = false;
                                    }
                                }
                            }
                            if ($is_prize_not_null) {
                                $count_prizes = count($_POST['prize']);

                                if ($count_prizes<$values['number_of_winners']) {
                                    throw new \Exception('Nie podałeś/aś wszystkich nagród.');
                                } elseif ($count_prizes>$values['number_of_winners']) {
                                    throw new \Exception('Wystąpił błąd.');
                                } else {
                                    $values['prize'] = json_encode($_POST['prize']);
                                }
                            } else {
                                throw new \Exception('Nie podałeś/aś nagród!');
                            }
                        } else {
                            throw new \Exception('Wystąpił błąd');
                        }
                    } else {
                        throw new \Exception('Wystąpił błąd z poprawnym wczytaniem twojego formularza. Przeładuj stronę i spróbuj ponownie.');
                    }
                } else {
                    throw new \Exception('Wystąpił błąd z poprawnym wczytaniem twojego formularza. Przeładuj stronę i spróbuj ponownie.');
                }
                $values['prize_delivery'] = $prize_delivery;
            } else {
                throw new \Exception('Wygląda na to, że nie uzupełniłeś/aś formularza do końca :)');
            }

            $count = 0;

            $create_contest->uploadContest($values);

            $status = 'success';
            $message = null;

        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        if ($status=='success') {
            echo json_encode(array('message'=>$message, 'status'=>$status, 'link'=>$values['link']));
        } elseif ($status=='error') {
            echo json_encode(array('message'=>$message, 'status'=>$status));
        }

    }
}
