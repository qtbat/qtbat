<?php
namespace Qtbat\Controller;

use Qtbat\Engine\Controller;
use Qtbat\Exception\PageError;

class ContestFunctionality extends Controller
{
    public function sendAnswer($href)
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        if (!isset($_POST['answer']))
            throw new PageError(404);

        $contest_functionality = new \Qtbat\Model\ContestFunctionality($this->database, $this->user);
        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);

        try {
            $basic_data = $display_contest->getBasicData($href[1], $href[2]);
            $unique_data = $display_contest->firstFew($basic_data['data']['id_contest']);
            $has_permission = $display_contest->hasPermission($basic_data['data']['id_contest'], $basic_data['data']['avaible_for'], $basic_data['creator_id'], $basic_data['data']['status']);

            if ($basic_data['creator_id']!=$this->user->getId()) {
                if ($has_permission==true) {
                    if (!empty($unique_data)) {
                        if ($basic_data['data']['status']==1) {
                            $took_part = $contest_functionality->tookPart($basic_data['data']['id_contest']);

                            if ($took_part==null) {
                                if (!isset($_POST['prize'])) {
                                    $_POST['prize'] = 1;
                                }

                                if (is_int((int)$_POST['prize'])) {
                                    if (empty($_POST['answer'])) {
                                        throw new \Exception('Nie podałeś żadnej odpowiedzi.');
                                    } elseif (strtolower($_POST['answer'])==strtolower($unique_data['data']['answer'])) {
                                        $winner = true;
                                    } else {
                                        $winner = false;
                                    }

                                    if (isset($winner)) {
                                        $prizes_left = $display_contest->firstFewPrizesLeft($basic_data['data']['id_contest'], $basic_data['data']['number_of_winners']);

                                        if (in_array($_POST['prize'], $prizes_left['prizes_left'])) {
                                            $contest_functionality->firstFewSelectWinner($winner, $basic_data['data']['id_contest'], $basic_data['data']['number_of_winners'], $prizes_left['prizes_left'], $_POST['prize'], $basic_data['creator_id']);

                                            $message = null;
                                            $status = 'success';
                                        } else {
                                            throw new \Exception('Nagroda, którą wskazałeś została już zabrana.');
                                        }
                                    }
                                } else {
                                    throw new \Exception('Wystąpił błąd');
                                }
                            } else {
                                throw new \Exception('Już wziąłeś udział w tym konkursie.');
                            }
                        } elseif ($basic_data['data']['status']==0) {
                            throw new \Exception('Ten konkurs się jeszcze nie rozpoczął');
                        } else {
                            throw new \Exception('Ten konkurs już się skończył.');
                        }
                    } else {
                        throw new \Exception('Wystąpił błąd.');
                    }
                } else {
                    throw new \Exception('Ten konkurs jest udostępniony dla grupy, do której nie należysz');
                }
            } else {
                throw new \Exception('Nie możesz brać udziału w tym konkursie, ponieważ jesteś jego autorem.');
            }
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        echo json_encode(array('status'=>$status, 'message'=>$message));
    }

    public function selectWinnerTakePart($href)
    {
        if (!isset($_FILES['take_part_image']) && !isset($_POST['take_part_video']) && !isset($_POST['take_part_text']) && !isset($_FILES['take_part_files']))
            throw new PageError(404);

        try {
            if ($this->user->isLogged()) {
                $contest_functionality = new \Qtbat\Model\ContestFunctionality($this->database, $this->user);
                $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
                $basic_data = $display_contest->getBasicData($href[1], $href[2]);
                $has_permission = $display_contest->hasPermission($basic_data['data']['id_contest'], $basic_data['data']['avaible_for'], $basic_data['creator_id'], $basic_data['data']['status']);

                if ($basic_data['creator_id']!=$this->user->getId()) {
                    if ($has_permission==true) {
                        $unique_data = $display_contest->selectWinner($basic_data['data']['id_contest']);
                        $requirements = json_decode($unique_data['data']['requirements'], true);

                        if ($unique_data['data']['limit_submission_date']==1) {
                            if (strtotime($unique_data['data']['submission_date']) < strtotime($this->user->getTime())) {
                                throw new \Exception('Czas na zgłaszanie się do tego konkursu już minął');
                            }
                        }

                        if (!empty($unique_data)) {
                            if ($basic_data['data']['status']==1) {
                                $took_part = $contest_functionality->tookPart($basic_data['data']['id_contest']);

                                if ($took_part==null) {
                                    if (isset($_FILES['take_part_image'])) {
                                        if ($requirements['picture']!==false) {
                                            $uploadfile = new \Qtbat\Engine\UploadFile($this->database);
                                            $file_info = $uploadfile->takePartContestImage($this->user->getId(), $_FILES['take_part_image'], $requirements['picture']['min_width'], $requirements['picture']['min_height'], $requirements['picture']['max_width'], $requirements['picture']['max_height']);
                                            $image_array = ['picture' => true];

                                            $id_file = $contest_functionality->selectWinnerTakePart($image_array, $basic_data['data']['id_contest'], $file_info);
                                            $contest_functionality->prepareFileToUnlink($id_file);

                                            $status = 'success';
                                            $message = null;
                                        } else {
                                            throw new \Exception('Wystąpił błąd');
                                        }
                                    } elseif (isset($_POST['take_part_video'])) {
                                        if ($requirements['video']!==false) {
                                            if (!preg_match('/^(https?:\/\/)?(www\.)?(youtube\.com\/watch\?v=|youtu.be\/)([a-zA-Z0-9\-\_]+)$/', $_POST['take_part_video'])) {
                                                throw new \Exception('Niepoprawny link');
                                            } else {
                                                preg_match('/^(https?:\/\/)?(www\.)?(youtube\.com\/watch\?v=|youtu.be\/)([a-zA-Z0-9\-\_]+)$/', $_POST['take_part_video'], $matches);

                                                $video_link = ['video' => $matches[4]];

                                                $contest_functionality->selectWinnerTakePart($video_link, $basic_data['data']['id_contest'], null);

                                                $status = 'success';
                                                $message = null;
                                            }
                                        } else {
                                            throw new \Exception('Wystąpił błąd');
                                        }
                                    } elseif (isset($_POST['take_part_text'])) {
                                        if ($requirements['text']!==false) {
                                            validateInput($_POST['take_part_text'], 'tekst zgłoszenia', 'select_winner_text');

                                            $value = ['text' => $_POST['take_part_text']];
                                            $contest_functionality->selectWinnerTakePart($value, $basic_data['data']['id_contest'], null);

                                            $status = 'success';
                                            $message = null;
                                        } else {
                                            throw new \Exception('Wystąpił błąd');
                                        }
                                    } elseif (isset($_FILES['take_part_files'])) {
                                        if ($requirements['files']!==false) {
                                            $count_files = count($_FILES['take_part_files']['tmp_name']);
                                            if ($count_files>15) {
                                                throw new \Exception('Próbujesz wysłać zbyt dużą ilość plików.');
                                            } elseif ($count_files==1 && $_FILES['take_part_files']['tmp_name'][0] == "") {
                                                throw new \Exception('Musisz wybrać conajmniej jeden plik');
                                            } else {
                                                $sum_size = 0;
                                                for ($i=0; $i<=$count_files-1; $i++) {
                                                    $sum_size += $_FILES['take_part_files']['size'][$i];
                                                }
                                                if (($sum_size / 1024)>400) {
                                                    throw new \Exception('Łączna wielkość plików, które próbujesz wysłać jest zbyt duża (4 MB)');
                                                } else {
                                                    foreach ($_FILES['take_part_files']['name'] as $name) {
                                                        if (strlen(pathinfo($name, PATHINFO_EXTENSION))>6) {
                                                            throw new \Exception('Niedozwolone rozszerzenie pliku');
                                                        }
                                                        if (isset($requirements['files']['extensions'])) {
                                                            if (strpos($requirements['files']['extensions'], pathinfo($name, PATHINFO_EXTENSION)) === false) {
                                                                throw new \Exception('Niedozwolone rozszerzenie pliku');
                                                            }
                                                        }
                                                    }

                                                    $uploadfile = new \Qtbat\Engine\UploadFile($this->database);
                                                    $file_info = [];
                                                    for ($i=0; $i<=$count_files-1; $i++) {
                                                        $file_info[$i] = $uploadfile->takePartContestFile($this->user->getId(), $_FILES['take_part_files']['tmp_name'][$i], $_FILES['take_part_files']['size'][$i], $_FILES['take_part_files']['name'][$i]);
                                                    }
                                                    $files_array = ['files' => true];

                                                    $id_files = $contest_functionality->selectWinnerTakePart($files_array, $basic_data['data']['id_contest'], $file_info);
                                                    foreach ($id_files as $id_file) {
                                                        $contest_functionality->prepareFileToUnlink($id_file);
                                                    }

                                                    $status = 'success';
                                                    $message = null;
                                                }
                                            }
                                        } else {
                                            throw new \Exception('Wystąpił błąd');
                                        }
                                    } else {
                                        throw new \Exception('Wystąpił błąd');
                                    }
                                } else {
                                    throw new \Exception('Już wziąłeś udział w tym konkursie.');
                                }
                            } elseif ($basic_data['data']['status']==0) {
                                throw new \Exception('Ten konkurs się jeszcze nie rozpoczął');
                            } else {
                                throw new \Exception('Ten konkurs już się skończył.');
                            }
                        } else {
                            throw new \Exception('Wystąpił błąd.');
                        }
                    } else {
                        throw new \Exception('Ten konkurs jest udostępniony dla grupy, do której nie należysz');
                    }
                } else {
                    throw new \Exception('Nie możesz brać udziału w tym konkursie, ponieważ jesteś jego autorem.');
                }
            }
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        echo json_encode(['status'=>$status, 'message'=>$message]);
    }

    public function selectWinnerApplicationHighlight($href)
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        if (!isset($_POST['highlight']) && !isset($_POST['order_number']))
            throw new PageError(404);

        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $basic_data = $display_contest->getBasicData($href[1], $href[2]);

        if ($this->user->getId()!=$basic_data['creator_id'] || $basic_data['data']['status']!=1)
            throw new PageError(404);

        $contest_functionality = new \Qtbat\Model\ContestFunctionality($this->database, $this->user);
        $contest_functionality->selectWinnerApplicationHighlight($basic_data['data']['id_contest'], $basic_data['data']['number_of_winners']);
    }

    public function selectWinnerConfirmWinners($href)
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        if (!isset($_POST['confirm_winners']))
            throw new PageError(404);

        $contest_functionality = new \Qtbat\Model\ContestFunctionality($this->database, $this->user);
        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $basic_data = $display_contest->getBasicData($href[1], $href[2]);

        if ($this->user->getId()!=$basic_data['creator_id'] || $basic_data['data']['status']!=1)
            throw new PageError(404);

        $unique_data = $display_contest->selectWinner($basic_data['data']['id_contest']);

        $contest_functionality->selectWinnerConfirmWinners($basic_data['data']['id_contest'], $unique_data['data']['limit_submission_date'], $unique_data['data']['submission_date'], $basic_data['creator_id'], $basic_data['data']['status']);
    }

    public function drawTakePart($href)
    {
        if (!isset($_FILES['take_part_image']) && !isset($_POST['take_part_video']) && !isset($_POST['take_part_text']) && !isset($_FILES['take_part_file']) && !isset($_POST['take_part']))
            throw new PageError(404);

        try {
            if ($this->user->isLogged()) {
                $contest_functionality = new \Qtbat\Model\ContestFunctionality($this->database, $this->user);
                $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
                $basic_data = $display_contest->getBasicData($href[1], $href[2]);
                $has_permission = $display_contest->hasPermission($basic_data['data']['id_contest'], $basic_data['data']['avaible_for'], $basic_data['creator_id'], $basic_data['data']['status']);

                if ($basic_data['creator_id']!=$this->user->getId()) {
                    if ($has_permission==true) {
                        $unique_data = $display_contest->draw($basic_data['data']['id_contest']);
                        if (!empty($unique_data)) {
                            if ($basic_data['data']['status']==1) {
                                if ($unique_data['data']['status']==0) {

                                    $took_part = $contest_functionality->tookPart($basic_data['data']['id_contest']);

                                    if ($unique_data['data']['method']==3) { //żeby nie robić ciągle tych samych selectów, przekazują się informacje już wcześniej wybrane
                                        $draw_info = [
                                            'number_of_applications' => $unique_data['data']['number_of_applications'],
                                            'number_of_winners' => $basic_data['data']['number_of_winners'],
                                            'requirement' => $unique_data['data']['requirement']                                    ];
                                    } else {
                                        $draw_info = null;
                                    }

                                    if ($took_part==null) {
                                        if (isset($_FILES['take_part_image'])) {
                                            if ($unique_data['data']['requirement']==1) {
                                                $uploadfile = new \Qtbat\Engine\UploadFile($this->database);
                                                $file_info = $uploadfile->takePartContestImage($this->user->getId(), $_FILES['take_part_image'], 10, 10, 1000, 1000);
                                                $image_array = ['picture' => true];

                                                $id_file = $contest_functionality->drawTakePart($image_array, $basic_data['data']['id_contest'], $file_info, $draw_info);
                                                $contest_functionality->prepareFileToUnlink($id_file);

                                                $status = 'success';
                                                $message = null;
                                            } else {
                                                throw new \Exception('Wystąpił błąd');
                                            }
                                        } elseif (isset($_POST['take_part_video'])) {
                                            if ($unique_data['data']['requirement']==2) {
                                                if (!preg_match('/^(https?:\/\/)?(www\.)?(youtube\.com\/watch\?v=|youtu.be\/)([a-zA-Z0-9\-\_]+)$/', $_POST['take_part_video'])) {
                                                    throw new \Exception('Niepoprawny link');
                                                } else {
                                                    preg_match('/^(https?:\/\/)?(www\.)?(youtube\.com\/watch\?v=|youtu.be\/)([a-zA-Z0-9\-\_]+)$/', $_POST['take_part_video'], $matches);

                                                    $video_link = ['video' => $matches[4]];

                                                    $contest_functionality->drawTakePart($video_link, $basic_data['data']['id_contest'], null, $draw_info);

                                                    $status = 'success';
                                                    $message = null;
                                                }
                                            } else {
                                                throw new \Exception('Wystąpił błąd');
                                            }
                                        } elseif (isset($_POST['take_part_text'])) {
                                            if ($unique_data['data']['requirement']==3) {
                                                validateInput($_POST['take_part_text'], 'tekst zgłoszenia', 'select_winner_text');

                                                $value = ['text' => $_POST['take_part_text']];
                                                $contest_functionality->drawTakePart($value, $basic_data['data']['id_contest'], null, $draw_info);

                                                $status = 'success';
                                                $message = null;
                                            } else {
                                                throw new \Exception('Wystąpił błąd');
                                            }
                                        } elseif (isset($_FILES['take_part_file'])) {
                                            if ($unique_data['data']['requirement']==4) {
                                                if (strlen(pathinfo($_FILES['take_part_file']['name'], PATHINFO_EXTENSION))>6) {
                                                    throw new \Exception('Niedozwolone rozszerzenie pliku');
                                                } else {
                                                    $uploadfile = new \Qtbat\Engine\UploadFile($this->database);
                                                    $file_info = $uploadfile->takePartContestFile($this->user->getId(), $_FILES['take_part_file']['tmp_name'], $_FILES['take_part_file']['size'], $_FILES['take_part_file']['name']);
                                                    $file_array = ['file' => true];
                                                    $id_file = $contest_functionality->drawTakePart($file_array, $basic_data['data']['id_contest'], $file_info, $draw_info);
                                                    $contest_functionality->prepareFileToUnlink($id_file);

                                                    $status = 'success';
                                                    $message = null;
                                                }
                                            } else {
                                                throw new \Exception('Wystąpił błąd1');
                                            }
                                        } elseif (isset($_POST['take_part'])) {
                                            $contest_functionality->drawTakePart(null, $basic_data['data']['id_contest'], null, $draw_info);

                                            $status = 'success';
                                            $message = null;
                                        } else {
                                            throw new \Exception('Wystąpił błąd2');
                                        }
                                    } else {
                                        throw new \Exception('Już wziąłeś udział w tym konkursie.');
                                    }
                                } else {
                                    throw new \Exception('Możliwość zapisywania się na to losowanie już się skończyła');
                                }
                            } elseif ($basic_data['data']['status']==0) {
                                throw new \Exception('Ten konkurs się jeszcze nie rozpoczął');
                            } else {
                                throw new \Exception('Ten konkurs już się skończył.');
                            }
                        } else {
                            throw new \Exception('Wystąpił błąd.');
                        }
                    } else {
                        throw new \Exception('Ten konkurs jest udostępniony dla grupy, do której nie należysz');
                    }
                } else {
                    throw new \Exception('Nie możesz brać udziału w tym konkursie, ponieważ jesteś jego autorem.');
                }
            }
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        echo json_encode(['status'=>$status, 'message'=>$message]);
    }

    public function startDraw($href)
    {
        $contest_functionality = new \Qtbat\Model\ContestFunctionality($this->database, $this->user);
        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $basic_data = $display_contest->getBasicData($href[1], $href[2]);
        $unique_data = $display_contest->draw($basic_data['data']['id_contest']);

        if ($this->user->getId()==$basic_data['creator_id']) {
            if ($basic_data['data']['status']==1) {
                if ($unique_data['data']['status']==0) {
                    if (isset($_POST['start'])) {
                        if ($unique_data['data']['requirement']==null) {
                            $status_value = 2;
                        } else {
                            $status_value = 1;
                        }
                        $contest_functionality->startDraw($basic_data['data']['id_contest'], $basic_data['data']['number_of_winners'], $status_value, $basic_data['creator_id']);
                    } else {
                        throw new PageError(404);
                    }
                } elseif ($unique_data['data']['status']==1) {
                    if (isset($_POST['application_id'])) {
                        $contest_functionality->repeatDraw($basic_data['data']['id_contest'], $_POST['application_id']);
                    } else {
                        throw new PageError(404);
                    }
                }
            } else {
                throw new PageError(404);
            }
        } else {
            throw new PageError(404);
        }
    }

    public function confirmDraw($href)
    {
        if (!isset($_POST['confirm_draw']))
            throw new PageError(404);

        $contest_functionality = new \Qtbat\Model\ContestFunctionality($this->database, $this->user);
        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $basic_data = $display_contest->getBasicData($href[1], $href[2]);
        $draw_status = $display_contest->drawStatus($basic_data['data']['id_contest']);

        if ($this->user->getId()==$basic_data['creator_id'] && $draw_status==1 && $basic_data['data']['status']==1) {
            $contest_functionality->confirmDraw($basic_data['data']['id_contest']);
        } else {
            throw new PageError(404);
        }
    }

    public function leaveOpinion($href)
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        if(!isset($_POST['rate']) && !isset($_POST['comment'])) {
            throw new PageError(404);
        }
        $contest_functionality = new \Qtbat\Model\ContestFunctionality($this->database, $this->user);
        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);

        $basic_data = $display_contest->getBasicData($href[1], $href[2]);

        if ($basic_data['data']['status']!=2)
            throw new PageError(404);

        $contest_functionality->leaveOpinion($basic_data['data']['id_contest'], $basic_data['creator_id']);
    }
}
