<?php
namespace Qtbat\Controller;

use Qtbat\Engine\Controller;
use Qtbat\Exception\PageError;

class Index extends Controller
{
    public function index()
    {
        if ($this->user->isLogged()) {
            $groups = new \Qtbat\Model\Groups($this->database, $this->user);
            $groups_data = $groups->panelGroups();

            $template = $this->twig->loadTemplate('base/panel_base.html.twig');
            $template->display([
                'notifications' => $this->user->getNotifications(),
                'user' => $this->user->getData(),
                'groups_list' => $groups_data['groups_list']
            ]);
        } else {
            $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
            $popular_contests = $display_contest->indexMostPopular();

            $template = $this->twig->loadTemplate('index/index_logged_out.html.twig');
            $template->display([
                'is_main_page' => true,
                'popular_contests' => $popular_contests
            ]);
        }
    }
}
