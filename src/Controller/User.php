<?php
namespace Qtbat\Controller;

use Qtbat\Engine\Controller;
use Qtbat\Exception\PageError;
use Exception;

class User extends Controller
{
    public function login()
    {
        if (empty($_POST)) {
            throw new PageError(404);
        }
        if ($this->user->isLogged()) {
            throw new PageError(403);
        }
        if (isset($_POST['login'])) {
            try {
                $this->user->login();
                $status = 'success';
                $message = null;
            } catch (Exception $e) {
                $status = 'error';
                $message = 'Nie można zalogować! '.$e->getMessage();
            }
        }
        echo json_encode(['status'=>$status, 'message'=>$message]);
    }

    public function register()
    {
        if (empty($_POST)) {
            throw new PageError(404);
        }
        if ($this->user->isLogged()) {
            throw new PageError(403);
        }
        if (isset($_POST['login'])) {
            try {
                $this->user->register();
                $status = 'success';
                $message = 'Pomyślnie zarejestrowano! Aktywuj konto klikając w link wysłany w wiadomości email.';
            } catch (Exception $e) {
                $status = 'error';
                $message = 'Nie można zarejestrować! '.$e->getMessage();
            }
        }
        echo json_encode(['status'=>$status, 'message'=>$message]);
    }

    public function logout()
    {
        if (!$this->user->isLogged()) {
            throw new PageError(403);
        }
        $this->user->logout();
        $this->redirect('');
    }

    public function activate($matches)
    {
        if ($this->user->isLogged()) {
            throw new PageError(403);
        }
        $result = $this->user->activate($matches[1]);
        $template = $this->twig->loadTemplate('user/activate_end.html.twig');
        $template->display([
            'notifications' => $this->user->getNotifications(),
            'activation' => $result
        ]);
    }

    public function changeEmail($matches)
    {
        $result = $this->user->changeEmail($matches[1]);
        $template = $this->twig->loadTemplate('user/change_email_end.html.twig');
        $template->display([
            'notifications' => $this->user->getNotifications(),
            'change' => $result
        ]);
    }

    public function readNotifications()
    {
        if (!isset($_POST['read_notifications']))
            throw new PageError(404);

        $this->user->readNotifications();
    }

    public function loadNotifications()
    {
        if (!isset($_POST['start']))
            throw new PageError(404);

        if (filter_input(INPUT_POST, 'start', FILTER_VALIDATE_INT)===false)
            throw new PageError(404);

        if (filter_input(INPUT_POST, 'page', FILTER_VALIDATE_INT)===false || $_POST['page']<1)
            throw new PageError(404);

        $data = $this->user->getNotificationsList($_POST['start'], $_POST['page']);

        $template = $this->twig->loadTemplate('profile/load_notifications.html.twig');
        $html_response = $template->render([
            'user' => $this->user->getData(),
            'notifications_list' => $data['notifications_list']
        ]);

        echo json_encode(['notifications_list' => $html_response, 'start' => $data['start'], 'stop' => $data['stop']]);
    }
}
