<?php
namespace Qtbat\Controller;

use Qtbat\Engine\Controller;
use Qtbat\Exception\PageError;

class Contest extends Controller
{
    public function displayContest($href)
    {
        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $contest_functionality = new \Qtbat\Model\ContestFunctionality($this->database, $this->user);
        $basic_data = $display_contest->getBasicData($href[1], $href[2]);

        if (empty($basic_data['data']))
            throw new PageError(404);

        $universal_data = $display_contest->getUniversalData($basic_data['data']['id_contest']);
        $took_part = $contest_functionality->tookPart($basic_data['data']['id_contest']);

        $has_permission = $display_contest->hasPermission($basic_data['data']['id_contest'], $basic_data['data']['avaible_for'], $basic_data['creator_id'], $basic_data['data']['status']);

        if ($has_permission==false) {
            throw new PageError(403);
        }

        if (!empty($took_part) && $took_part['position']>0 && $took_part['position']!='a') {
            $took_part_prize_index = $took_part['position']-1;
            $prize = $universal_data['prize'][$took_part_prize_index];
        } else {
            $prize = null;
        }

        if ($basic_data['data']['contest_type']==1) {
            $unique_data = $display_contest->firstFew($basic_data['data']['id_contest']);
            $prizes_left = $display_contest->firstFewPrizesLeft($basic_data['data']['id_contest'], $basic_data['data']['number_of_winners']);

            $template = $this->twig->loadTemplate('/contest/first_few.html.twig');
            $template->display([
                //uniwersalne rzeczy
                'notifications' => $this->user->getNotifications(),
                'user' => $this->user->getData(),
                'basic_data' => $basic_data,
                'data' => $universal_data['data'],
                'all_prizes' => $universal_data['prize'],
                'pictures' => $universal_data['pictures'],
                'banner' => $universal_data['banner'],
                'took_part' => $took_part,
                'href_data' => $href,
                'prize' => $prize,
                //unikalne rzeczy dla danego typu
                'unique_data' => $unique_data['data'],
                'number_of_participants' => $unique_data['number_of_participants'],
                'winners_logins' => $unique_data['winners_logins'],
                'prizes_left' => $prizes_left['prizes_left']
            ]);
        } elseif ($basic_data['data']['contest_type']==2) {
            $unique_data = $display_contest->draw($basic_data['data']['id_contest']);

            if ($unique_data['data']['status']!=0) {
                $winners = $display_contest->drawWinners($basic_data['data']['id_contest'], $unique_data['data']['status'], $unique_data['data']['requirement']);
            } else {
                $winners = null;
            }

            $template = $this->twig->loadTemplate('/contest/draw.html.twig');
            $template->display([
                'notifications' => $this->user->getNotifications(),
                'user' => $this->user->getData(),
                'basic_data' => $basic_data,
                'data' => $universal_data['data'],
                'pictures' => $universal_data['pictures'],
                'banner' => $universal_data['banner'],
                'took_part' => $took_part,
                'href_data' => $href,
                'prize' => $prize,
                //unikalne rzeczy dla danego typu
                'unique_data' => $unique_data['data'],
                'count_applications' => $unique_data['count_applications'],
                'winning_applications' => $winners
            ]);

        } elseif ($basic_data['data']['contest_type']==3) {
            $unique_data = $display_contest->selectWinner($basic_data['data']['id_contest']);
            $requirements = json_decode($unique_data['data']['requirements'], true);

            $allow_applications = true;

            if ($unique_data['data']['limit_submission_date'] == 1 && strtotime($unique_data['data']['submission_date']) < strtotime($this->user->getTime())) {
                $allow_applications = false;
            }

            if ($unique_data['data']['show_winners']!=null || $this->user->getId()==$basic_data['creator_id']) {
                $winning_applications = $display_contest->selectWinnerWinnersList($basic_data['data']['id_contest']);
            } else {
                $winning_applications = null;
            }

            $template = $this->twig->loadTemplate('/contest/select_winner.html.twig');
            $template->display([
                'notifications' => $this->user->getNotifications(),
                'user' => $this->user->getData(),
                'basic_data' => $basic_data,
                'data' => $universal_data['data'],
                'pictures' => $universal_data['pictures'],
                'banner' => $universal_data['banner'],
                'took_part' => $took_part,
                'href_data' => $href,
                'prize' => $prize,
                //unikalne rzeczy dla danego typu
                'requirements' => $requirements,
                'unique_data' => $unique_data['data'],
                'allow_applications' => $allow_applications,
                'winning_applications' => $winning_applications
            ]);

        } elseif ($basic_data['data']['contest_type']==4) {
            //$unique_data = $display_contest->

            $template = $this->twig->loadTemplate('/contest/tournament.html.twig');
            $template->display([
                'notifications' => $this->user->getNotifications(),
                'user' => $this->user->getData(),
                'basic_data' => $basic_data,
                'data' => $universal_data['data'],
                'pictures' => $universal_data['pictures'],
                'banner' => $universal_data['banner'],
                'took_part' => $took_part,
                'href_data' => $href,
                'prize' => $prize
                //unikalne rzeczy dla danego typu
            ]);

        } else {
            throw new PageError(404);
        }
    }

    public function settings($href)
    {
        if (!isset($_POST['load_settings']))
            throw new PageError(404);

        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $basic_data = $display_contest->getBasicData($href[1], $href[2]);

        if ($basic_data['creator_id']!=$this->user->getId())
            throw new PageError(403);

        $create_contest = new \Qtbat\Model\CreateContest($this->database, $this->user);

        $edit_info = $create_contest->display();
        $count_groups = count($edit_info['groups_list'])-1;
        $data = $display_contest->getSettingsData($basic_data['data']['id_contest']);

        for ($i=0; $i<=$count_groups; $i++) {
            if (in_array($edit_info['groups_list'][$i]['id_group'], $data['groups'])) {
                $edit_info['groups_list'][$i]['is_shared_for_contest'] = true;
            } else {
                $edit_info['groups_list'][$i]['is_shared_for_contest'] = false;
            }
        }

        $template = $this->twig->loadTemplate('/contest/settings.html.twig');
        $template->display([
            'groups_list' => $edit_info['groups_list'],
            'categories' => $edit_info['categories'],
            'data' => $data['data'],
            'basic_data' => $basic_data
        ]);
    }

    public function editContest($href)
    {
        $contest_modification = new \Qtbat\Model\ContestModification($this->database, $this->user);
        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $basic_data = $display_contest->getBasicData($href[1], $href[2]);
        $contest_modification->editContest($basic_data['data']['id_contest'], $basic_data['creator_id'], $basic_data['data']['status']);
    }

    public function editContestBanner($href)
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        if (!isset($_FILES['contest_banner']) && !isset($_POST['delete_banner']))
            throw new PageError(404);

        try {
            $upload_file = new \Qtbat\Engine\UploadFile($this->database);
            $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
            $basic_data = $display_contest->getBasicData($href[1], $href[2]);

            if ($basic_data['creator_id']==$this->user->getId()) {
                if ($basic_data['data']['status']==0) {
                    if (isset($_POST['delete_banner'])) {
                        $current_banner = $display_contest->getContestBanner($basic_data['data']['id_contest']);

                        if (!empty($current_banner)) {
                            $file_management = new \Qtbat\Engine\FileManagement();
                            $contest_modification = new \Qtbat\Model\ContestModification($this->database, $this->user);

                            $path = $file_management->getPath($current_banner[0]['disk']);
                            $upload_file->unlinkContestImage($path, $current_banner[0]['folder'], $current_banner[0]['name']);
                            $contest_modification->deleteContestBanner($basic_data['data']['id_contest']);
                            $status = 'success';
                            $message = null;
                        } else {
                            throw new \Exception('Ten konkurs nie ma ustawionego bannera');
                        }
                    } else {
                        $upload_file->uploadContestBanner($basic_data['data']['id_contest'], $_FILES['contest_banner']);
                        $status = 'success';
                        $message = null;
                    }
                } else {
                    throw new \Exception('Nie możesz zmienić tej informacji, ponieważ konkurs już się rozpoczął.');
                }
            } else {
                throw new \Exception('Nie możesz modyfikować tego konkursu, ponieważ nie jesteś jego autorem.');
            }
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        echo json_encode(array('status'=>$status, 'message'=>$message));
    }

    public function editContestImages($href)
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        if (!isset($_FILES['img1']) && !isset($_FILES['img2']) && !isset($_FILES['img3']) && !isset($_POST['delete_img1']) && !isset($_POST['delete_img2']) && !isset($_POST['delete_img1']))
            throw new PageError(404);

        try {
            $uploadfile = new \Qtbat\Engine\UploadFile($this->database);
            $contest_modification = new \Qtbat\Model\ContestModification($this->database, $this->user);
            $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
            $basic_data = $display_contest->getBasicData($href[1], $href[2]);
            $loggeduser_Id = $this->user->getId();

            if ($basic_data['creator_id']==$loggeduser_Id) {
                if ($basic_data['data']['status']==0) {
                    $current_images = $display_contest->getCurrentContestImages($basic_data['data']['id_contest']);

                    $count = 0;
                    $images = [];

                    $images_status = 'success';

                    if (isset($_FILES['img1']) && $_FILES['img1']['tmp_name']!="") {
                        $validateContestImage1 = $uploadfile->validateContestImage($_FILES['img1']);
                        if ($validateContestImage1!='success') {
                            throw new \Exception($validateContestImage1);
                            $images_status = 'error';
                        }
                    }
                    if (isset($_FILES['img2']) && $_FILES['img2']['tmp_name']!="") {
                        $validateContestImage2 = $uploadfile->validateContestImage($_FILES['img2']);
                        if ($validateContestImage2!='success') {
                            throw new \Exception($validateContestImage2);
                            $images_status = 'error';
                        }
                    }
                    if (isset($_FILES['img3']) && $_FILES['img3']['tmp_name']!="") {
                        $validateContestImage3 = $uploadfile->validateContestImage($_FILES['img3']);
                        if ($validateContestImage3!='success') {
                            throw new \Exception($validateContestImage3);
                            $images_status = 'error';
                        }
                    }

                    if ($images_status!='error') {

                        $file_management = new \Qtbat\Engine\FileManagement();

                        if (isset($_POST['delete_img1'])) {
                            if (isset($current_images[0])) {
                                $path = $file_management->getPath($current_images[0]['disk']);
                                $uploadfile->unlinkContestImage($path, $current_images[0]['folder'], $current_images[0]['name']);
                                $img1 = ['unlink' => true, 'name' => $current_images[0]['name']];
                                $images[$count] = $img1;
                                $count++;
                            } else {
                                throw new \Exception('Nie możesz usunąć tego zdjęcia.');
                            }
                        } else {
                            if ($_FILES['img1']['tmp_name']!="") {
                                if (isset($current_images[0])) {
                                    $path = $file_management->getPath($current_images[0]['disk']);
                                    $uploadfile->unlinkContestImage($path, $current_images[0]['folder'], $current_images[0]['name']);
                                    $uploadfile->changeContestImage($path, $current_images[0]['folder'], $current_images[0]['name'], $_FILES['img1']);
                                } else {
                                    $img1 = $uploadfile->uploadContestImage($loggeduser_Id, $_FILES['img1']);
                                    $images[$count] = $img1;
                                    $count++;
                                }
                            }
                        }

                        if (isset($_POST['delete_img2'])) {
                            if (isset($current_images[1])) {
                                $path = $file_management->getPath($current_images[1]['disk']);
                                $uploadfile->unlinkContestImage($path, $current_images[1]['folder'], $current_images[1]['name']);
                                $img2 = ['unlink' => true, 'name' => $current_images[1]['name']];
                                $images[$count] = $img2;
                                $count++;
                            } else {
                                throw new \Exception('Nie możesz usunąć tego zdjęcia.');
                            }
                        } else {
                            if ($_FILES['img2']['tmp_name']!="") {
                                if (isset($current_images[1])) {
                                    $path = $file_management->getPath($current_images[1]['disk']);
                                    $uploadfile->unlinkContestImage($path, $current_images[1]['folder'], $current_images[1]['name']);
                                    $uploadfile->changeContestImage($path, $current_images[1]['folder'], $current_images[1]['name'], $_FILES['img2']);
                                } else {
                                    $img2 = $uploadfile->uploadContestImage($loggeduser_Id, $_FILES['img2']);
                                    $images[$count] = $img2;
                                    $count++;
                                }
                            }
                        }

                        if (isset($_POST['delete_img3'])) {
                            if (isset($current_images[2])) {
                                $path = $file_management->getPath($current_images[2]['disk']);
                                $uploadfile->unlinkContestImage($path, $current_images[2]['folder'], $current_images[2]['name']);
                                $img3 = ['unlink' => true, 'name' => $current_images[2]['name']];
                                $images[$count] = $img3;
                                $count++;
                            } else {
                                throw new \Exception('Nie możesz usunąć tego zdjęcia.');
                            }
                        } else {
                            if ($_FILES['img3']['tmp_name']!="") {
                                if (isset($current_images[2])) {
                                    $path = $file_management->getPath($current_images[2]['disk']);
                                    $uploadfile->unlinkContestImage($path, $current_images[2]['folder'], $current_images[2]['name']);
                                    $uploadfile->changeContestImage($path, $current_images[2]['folder'], $current_images[2]['name'], $_FILES['img3']);
                                } else {
                                    $img3 = $uploadfile->uploadContestImage($loggeduser_Id, $_FILES['img3']);
                                    $images[$count] = $img3;
                                    $count++;
                                }
                            }
                        }

                        if (!empty($images)) {
                            $contest_modification->editContestImages($basic_data['data']['id_contest'], $images);
                        }

                        $status = 'success';
                        $message = null;
                    }
                } else {
                    throw new \Exception('Nie możesz zmienić tej informacji, ponieważ konkurs już się rozpoczął.');
                }
            } else {
                throw new \Exception('Nie możesz modyfikować tego konkursu, ponieważ nie jesteś jego autorem.');
            }
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        echo json_encode(array('status'=>$status, 'message'=>$message));
    }

    public function startContest($href)
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        if (!isset($_POST['start_contest']))
            throw new PageError(404);

        $contest_modification = new \Qtbat\Model\ContestModification($this->database, $this->user);
        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $basic_data = $display_contest->getBasicData($href[1], $href[2]);
        $contest_modification->startContest($basic_data['data']['id_contest'], $basic_data['creator_id'], $basic_data['data']['status'], $basic_data['data']['contest_type']);
    }

    public function deleteContest($href)
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        if (!isset($_POST['delete_contest']))
            throw new PageError(404);

        $contest_modification = new \Qtbat\Model\ContestModification($this->database, $this->user);
        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $basic_data = $display_contest->getBasicData($href[1], $href[2]);
        $contest_modification->deleteContest($basic_data['data']['id_contest'], $basic_data['creator_id'], $basic_data['data']['status'], $basic_data['data']['contest_type']);
    }

    public function selectWinnerApplicationsList($href)
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $basic_data = $display_contest->getBasicData($href[1], $href[2]);

        if ($this->user->getId()!=$basic_data['creator_id'])
            throw new PageError(403);

        if (empty($basic_data['data']))
            throw new PageError(404);

        if ($basic_data['data']['contest_type']!=3)
            throw new PageError(404);

        if ($href[3]!='applications-list' && $href[3]!='positive-applications' && $href[3]!='negative-applications' && $href[3]!='awarded-applications') {
            throw new PageError(404);
        }

        if ($basic_data['data']['status']!=1)
            throw new PageError(404);

        $applications_stats = $display_contest->selectWinnerApplicationsStats($basic_data['data']['id_contest'], $href[3]);

        $unique_data = $display_contest->selectWinner($basic_data['data']['id_contest']);

        $allow_confirm_winners = true;

        if ($unique_data['data']['limit_submission_date'] == 1 && strtotime($unique_data['data']['submission_date']) > strtotime($this->user->getTime())) {
            $allow_confirm_winners = false;
        }

        $template = $this->twig->loadTemplate('/contest/select_winner_applications_list.html.twig');
        $template->display([
            'notifications' => $this->user->getNotifications(),
            'user' => $this->user->getData(),
            'basic_data' => $basic_data,
            'href_data' => $href,
            'count_all' => $applications_stats['count_all'],
            'count_positive' => $applications_stats['count_positive'],
            'count_negative' => $applications_stats['count_negative'],
            'count_awarded' => $applications_stats['count_awarded'],
            'records_in_bookmark' => $applications_stats['records_in_bookmark'],
            'unique_data' => $unique_data['data'],
            'allow_confirm_winners' => $allow_confirm_winners,
            'start_from_here' => $applications_stats['start_from_here']
        ]);
    }

    public function selectWinnerLoadApplications($href)
    {
        if (!isset($_POST['current_page']) && !isset($_POST['start']) && !isset($_POST['count_records'])) {
            throw new PageError(404);
        } elseif (filter_input(INPUT_POST, 'current_page', FILTER_VALIDATE_INT)===false || filter_input(INPUT_POST, 'count_records', FILTER_VALIDATE_INT)===false) {
            throw new PageError(404);
        } else {
            $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
            $basic_data = $display_contest->getBasicData($href[1], $href[2]);

            if ($this->user->getId()!=$basic_data['creator_id']) {
                throw new PageError(404);
            }

            $applications_list = $display_contest->selectWinnerApplicationsList($basic_data['data']['id_contest'], $href[3], $_POST['current_page'], $_POST['start'], $_POST['count_records']);

            $template = $this->twig->loadTemplate('/contest/select_winner_load_applications.html.twig');
            $html_response = $template->render([
                'applications_list' => $applications_list['applications_list'],
                'href_data' => $href
            ]);

            echo json_encode(['applications_list' => $html_response, 'start' => $applications_list['start']]);
        }
    }

    public function selectWinnerApplicationDetails($href)
    {
        if (!$this->user->isLogged())
            throw new PageError(404);

        if (!isset($_POST['order_number']))
            throw new PageError(404);

        $display_contest = new \Qtbat\Model\DisplayContest($this->database, $this->user);
        $basic_data = $display_contest->getBasicData($href[1], $href[2]);

        if ($this->user->getId()!=$basic_data['creator_id'] || $basic_data['data']['status']!=1)
            throw new PageError(404);

        $display_contest->selectWinnerApplicationDetails($basic_data['data']['id_contest'], $_POST['order_number']);
    }
}
