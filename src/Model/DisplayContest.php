<?php
namespace Qtbat\Model;

use Qtbat\Exception\PageError;
use Exception;

class DisplayContest
{
    function __construct($database, $user)
    {
        $this->database = $database;
        $this->user = $user;
    }

    public function getBasicData($login, $link)
    {
        $creator_id = $this->database->selectWithWhere('SELECT id_user FROM users WHERE lower(login)=lower(?)', [$login]);
        if (!isset($creator_id[0]['id_user'])) {
            throw new PageError(404);
        }

        $data = $this->database->selectWithwhere('SELECT contest_type, id_contest, avaible_for, status, number_of_winners
            FROM contests
            WHERE link=? AND creator_id=?', [$link, $creator_id[0]['id_user']]);
        if (!isset($data[0]))
            $data[0] = null;

        return ['data'=>$data[0], 'creator_id'=>$creator_id[0]['id_user']];
    }

    public function getUniversalData($id_contest)
    {
        $data = $this->database->selectWithwhere('SELECT u.login as creator_login, c.start_date::timestamp, c.end_date::timestamp, c.create_date::timestamp, c.title, c.description, cat.name as category_name, c.prize_delivery, c.prize
            FROM contests as c
            JOIN categories as cat ON c.id_category=cat.id_category
            JOIN users as u ON c.creator_id=u.id_user
            WHERE c.id_contest=?', [$id_contest]);
        if (!isset($data[0]))
            $data[0] = null;

        $prize = json_decode($data[0]['prize']);

        $pictures = $this->database->selectWithWhere('SELECT uf.name
            FROM uploaded_files as uf
            JOIN contests_images as ci ON uf.id_file=ci.id_file
            JOIN contests as c ON c.id_contest=ci.id_contest
            WHERE c.id_contest=? AND ci.type=0', [$id_contest]);

        $banner = $this->database->selectWithWhere('SELECT uf.name
            FROM uploaded_files as uf
            JOIN contests_images as ci ON uf.id_file=ci.id_file
            JOIN contests as c ON c.id_contest=ci.id_contest
            WHERE c.id_contest=? AND ci.type=1', [$id_contest]);

        if (!isset($banner[0]['name'])) {
            $banner[0]['name'] = null;
        }

        return ['data'=>$data[0], 'prize'=>$prize, 'pictures'=>$pictures, 'banner'=>$banner[0]['name']];
    }

    public function hasPermission($id_contest, $avaible_for, $creator_id, $contest_status)
    {
        if ($avaible_for==1) {
            $groups_id = $this->database->selectWithWhere('SELECT id_group
                FROM contests_groups
                WHERE id_contest=?', [$id_contest]);

            $count = count($groups_id)-1;

            $is_in_group = [];

            for ($i=0; $i<=$count; $i++) {
                $get_data = $this->database->selectWithWhere('SELECT id_user
                    FROM users_groups
                    WHERE id_group=? AND member_status=1 AND id_user=?', [$groups_id[$i]['id_group'], $this->user->getId()]);

                if (!empty($get_data)) {
                    $is_in_group[$i] = $get_data;
                }
            }

            if ($this->user->getId()==$creator_id) {
                $status = true;
            } else {
                if ($contest_status==0) {
                    $status = false;
                } else {
                    if (empty($is_in_group)) {
                        $status = false;
                    } else {
                        $status = true;
                    }
                }
            }
        } elseif ($avaible_for==2) {
            if ($contest_status==0) {
                if ($this->user->getId()==$creator_id) {
                    $status = true;
                } else {
                    $status = false;
                }
            } else {
                $status = true;
            }
        }

        return $status;
    }

    public function getCurrentContestImages($id_contest)
    {
        $images = $this->database->selectWithWhere('SELECT uf.name, uf.disk, uf.folder
            FROM uploaded_files as uf
            JOIN contests_images as ci ON uf.id_file=ci.id_file
            JOIN contests as c ON c.id_contest=ci.id_contest
            WHERE c.id_contest=? AND ci.type=0', [$id_contest]);

        return $images;
    }

    public function getContestBanner($id_contest)
    {
        $banner = $this->database->selectWithWhere('SELECT uf.name, uf.disk, uf.folder
            FROM uploaded_files as uf
            JOIN contests_images as ci ON uf.id_file=ci.id_file
            JOIN contests as c ON c.id_contest=ci.id_contest
            WHERE c.id_contest=? AND ci.type=1', [$id_contest]);

        return $banner;
    }

    public function getSettingsData($id_contest)
    {
        $data = $this->database->selectWithWhere('SELECT c.prize, c.prize_delivery, cat.name as category_name
            FROM contests as c
            JOIN categories as cat ON c.id_category=cat.id_category
            WHERE id_contest=?', [$id_contest]);

        $data[0]['prize'] = json_decode($data[0]['prize']);

        $groups = $this->database->selectWithWhere('SELECT id_group FROM contests_groups WHERE id_contest=?', array($id_contest));
        $groups_array = [];

        foreach ($groups as $groups) {
            $groups_array[] = $groups['id_group'];
        }

        return ['data' => $data[0], 'groups' => $groups_array];
    }

    public function firstFew($id_contest)
    {
        $data = $this->database->selectWithWhere('SELECT question, answer, answer_form
            FROM first_few
            WHERE id_contest=?', [$id_contest]);

        $winners_logins = $this->database->selectWithWhere('SELECT u.login, cp.position
            FROM contests as c
            JOIN contests_participants as cp ON cp.id_contest=c.id_contest
            JOIN users as u ON cp.id_user=u.id_user
            WHERE c.id_contest=? AND cp.status=1
            ORDER BY cp.status ASC', [$id_contest]);
        if (!isset($winner_login[0])) {
            $winner_login[0] = null;
        }

        $number_of_participants = $this->database->selectWithWhere('SELECT COUNT(id_user) as number
            FROM contests_participants
            WHERE id_contest=? AND status=0', [$id_contest]);

        return ['data'=>$data[0], 'number_of_participants'=>$number_of_participants[0]['number'], 'winners_logins'=>$winners_logins];
    }

    public function firstFewPrizesLeft($id_contest, $number_of_winners)
    {
        $prizes_taken = $this->database->selectWithWhere('SELECT position
            FROM contests_participants
            WHERE id_contest=? AND status=1', [$id_contest]);
        if (!isset($prizes_taken)) {
            $prizes_taken = 0;
        }

        $count = count($prizes_taken)-1;
        $string = null;

        for ($i=0; $i<=$count; $i++) {
            $string .= $prizes_taken[$i]['position'];
        }

        $prizes_left = [];

        for($i=1; $i<=$number_of_winners; $i++) {
            if (strpos($string, strval($i)) === false) {
                $prizes_left[$i] = $i;
            }
        }

        return ['prizes_left'=>$prizes_left];
    }

    public function draw($id_contest)
    {
        $data = $this->database->selectWithWhere('SELECT requirement, method, date::timestamp, number_of_applications, status
            FROM draw
            WHERE id_contest=?', [$id_contest]);

        $count_applications = $this->database->selectWithWhere('SELECT COUNT(id_application) as count
            FROM contests_participants
            WHERE id_contest=?', [$id_contest]);

        return ['data'=>$data[0], 'count_applications'=>$count_applications[0]['count']];
    }

    public function drawStatus($id_contest)
    {
        $status = $this->database->selectWithWhere('SELECT status
            FROM draw
            WHERE id_contest=?', [$id_contest]);

        return $status[0]['status'];
    }

    public function drawWinners($id_contest, $status, $requirement)
    {
        if ($status == 1) {
            $winners = $this->database->selectWithWhere("SELECT cp.id_application, cp.content, u.login, cp.id_user
                FROM contests_participants as cp
                JOIN users as u ON u.id_user=cp.id_user
                WHERE cp.position='w' AND cp.id_contest=?", [$id_contest]);
        } elseif ($status == 2) {
            $winners = $this->database->selectWithWhere("SELECT cp.id_application, cp.content, u.login, cp.id_user
                FROM contests_participants as cp
                JOIN users as u ON u.id_user=cp.id_user
                WHERE cp.status=1 AND cp.id_contest=?", [$id_contest]);
        }

        if ($requirement==1 || $requirement==4) {
            $files = $this->database->selectWithWhere("SELECT cp.id_application, uf.original_name, uf.name
                FROM uploaded_files as uf
                JOIN applications_files as af ON af.id_file=uf.id_file
                JOIN contests_participants as cp ON cp.id_application=af.id_application
                WHERE cp.id_contest=? AND cp.position='w'", [$id_contest]);
        }

        foreach ($winners as &$application) {
            $application['content'] = json_decode($application['content'], true);

            if (isset($application['content']['picture'])) {
                foreach ($files as $file_key => $file) {
                    if ($file['id_application'] == $application['id_application'])
                        $application['content'] = ['picture' => $files[$file_key]['name']];
                }
            } elseif (isset($application['content']['files'])) {
                $application['content']['files'] = [];
                foreach ($files as $file_key => $file) {
                    if ($file['id_application'] == $application['id_application'])
                        array_push($application['content']['files'], ['original_name' => $file['original_name'], 'name' => $file['name']]);
                }
            }
        }

        return $winners;
    }

    public function selectWinner($id_contest)
    {
        $data = $this->database->selectWithWhere('SELECT requirements, limit_submission_date, submission_date::timestamp, show_winners FROM select_winner WHERE id_contest=?', [$id_contest]);

        return ['data'=>$data[0]];
    }

    public function selectWinnerApplicationsStats($id_contest, $href)
    {
        $count_all = $this->database->selectWithWhere("SELECT COUNT(swa.id_application) as count
            FROM select_winner_applications as swa
            JOIN contests_participants as cp ON cp.id_application=swa.id_application
            WHERE cp.id_contest=?", [$id_contest]);
        $count_positive = $this->database->selectWithWhere("SELECT COUNT(swa.id_application) as count
            FROM select_winner_applications as swa
            JOIN contests_participants as cp ON cp.id_application=swa.id_application
            WHERE cp.id_contest=? AND swa.highlight='p'", [$id_contest]);
        $count_negative = $this->database->selectWithWhere("SELECT COUNT(swa.id_application) as count
            FROM select_winner_applications as swa
            JOIN contests_participants as cp ON cp.id_application=swa.id_application
            WHERE cp.id_contest=? AND swa.highlight='n'", [$id_contest]);
        $count_awarded = $this->database->selectWithWhere("SELECT COUNT(swa.id_application) as count
            FROM select_winner_applications as swa
            JOIN contests_participants as cp ON cp.id_application=swa.id_application
            WHERE cp.id_contest=? AND (swa.highlight='1' OR swa.highlight='2' OR swa.highlight='3' OR swa.highlight='4' OR swa.highlight='a')", [$id_contest]);

        if ($href=='applications-list') {
            $start_from_here = $this->database->selectWithWhere("SELECT swa.order_number
                FROM contests_participants as cp
                JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
                WHERE cp.id_contest=? AND swa.highlight='s'", [$id_contest]);

            if (!isset($start_from_here[0]['order_number'])) {
                $last_viable_record = $this->database->selectWithWhere("SELECT swa.order_number
                    FROM contests_participants as cp
                    JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
                    WHERE (swa.highlight!='n' OR swa.highlight IS NULL) AND cp.id_contest=?
                    ORDER BY swa.order_number DESC
                    LIMIT 1", [$id_contest]);
                if (!isset($last_viable_record[0]['order_number'])) {
                    $last_viable_record[0]['order_number'] = 0;
                }
                $start_from_here[0]['order_number'] = $last_viable_record[0]['order_number'];
            }
            $all_records = $count_all[0]['count'];
        } elseif ($href=='negative-applications') {
            $start_from_here = $this->database->selectWithWhere("SELECT swa.order_number
                FROM select_winner_applications as swa
                JOIN contests_participants as cp ON cp.id_application=swa.id_application
                WHERE cp.id_contest=? AND swa.highlight='n'
                ORDER BY swa.order_number DESC
                LIMIT 1", [$id_contest]);
            if (!isset($start_from_here[0]['order_number'])) {
                $start_from_here[0]['order_number'] = 0;
            }

            $all_records = $count_negative[0]['count'];
        } elseif ($href=='positive-applications') {
            $start_from_here = $this->database->selectWithWhere("SELECT swa.order_number
                FROM select_winner_applications as swa
                JOIN contests_participants as cp ON cp.id_application=swa.id_application
                WHERE cp.id_contest=? AND swa.highlight='p'
                ORDER BY swa.order_number DESC
                LIMIT 1", [$id_contest]);
            if (!isset($start_from_here[0]['order_number'])) {
                $start_from_here[0]['order_number'] = 0;
            }

            $all_records = $count_positive[0]['count'];
        } elseif ($href='awarded-applications') {
            $start_from_here = $this->database->selectWithWhere("SELECT swa.order_number
                FROM select_winner_applications as swa
                JOIN contests_participants as cp ON cp.id_application=swa.id_application
                WHERE cp.id_contest=? AND (swa.highlight='1' OR swa.highlight='2' OR swa.highlight='3' OR swa.highlight='4' OR swa.highlight='a')
                ORDER BY swa.highlight DESC
                LIMIT 1", [$id_contest]);
            if (!isset($start_from_here[0]['order_number'])) {
                $start_from_here[0]['order_number'] = 0;
            }

            $all_records = $count_awarded[0]['count'];
        }

        return ['start_from_here'=>$start_from_here[0]['order_number'],
                'count_all'=>$count_all[0]['count'],
                'count_positive'=>$count_positive[0]['count'],
                'count_negative'=>$count_negative[0]['count'],
                'count_awarded'=>$count_awarded[0]['count'],
                'records_in_bookmark'=>$all_records];
    }

    public function selectWinnerApplicationsList($id_contest, $href, $current_page, $start, $all_records)
    {
        $records_per_page = 12;
        $limit_applications = $records_per_page + 1;

        if ($href=='applications-list') {

            //all records without negatives
            $all_viable_records = $this->database->selectWithWhere("SELECT COUNT(cp.id_application) as count
                FROM contests_participants as cp
                JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
                WHERE cp.id_contest=? AND (swa.highlight!='n' OR swa.highlight IS NULL)", [$id_contest]);

            $start_from_here = $this->database->selectWithWhere("SELECT swa.order_number
                FROM contests_participants as cp
                JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
                WHERE cp.id_contest=? AND swa.highlight='s'", [$id_contest]);

            if (!isset($start_from_here[0]['order_number'])) {
                $start_from_here[0]['order_number'] = $all_records;
            }

            //pages that dont have any negatives inside
            $number_of_pages = $all_viable_records[0]['count'] / $records_per_page;
            if (is_float($number_of_pages)) {
                $number_of_pages = floor($number_of_pages + 1);
            }

            if ($current_page<$number_of_pages) {
                $condition ='AND (order_number<=' . $start . ' AND order_number>=1)';
            } elseif ($current_page==$number_of_pages) {
                if ($start>$start_from_here[0]['order_number']) {
                    $condition = 'AND order_number<=' . $start . ' AND order_number>' . $start_from_here[0]['order_number'];
                } else {
                    $condition = 'AND order_number<=' . $start . ' AND order_number<=' . $start_from_here[0]['order_number'];
                }
            } else {
                $condition = '';
                $limit_applications = 0;
            }

            $applications_list = $this->database->selectWithWhere("SELECT cp.id_application, cp.id_user, cp.content, swa.order_number, swa.highlight
                FROM contests_participants as cp
                JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
                WHERE (swa.highlight!='n' OR swa.highlight IS NULL) AND cp.id_contest=? $condition
                ORDER BY swa.order_number DESC
                LIMIT ?", [$id_contest, $limit_applications]);

            $al_i = 0;
            $al_lenght = count($applications_list);

            foreach ($applications_list as $key => $application) {
                if ($al_i == $al_lenght-1) {
                    $start = $application['order_number'];
                    if ($al_lenght>$records_per_page) {
                        unset($applications_list[$key]);
                    }
                } else {
                    $al_i++;
                }
            }

            if ($al_lenght==$records_per_page && $current_page==$number_of_pages) {
                $start = 'n';
            }

            //attach to applications_list if it ends in the lowest record
            //nie da sie obliczyć ile w dół rekordów ma wybrać ze zwyklego $applications_list

            //only 1 page
            if ($al_lenght<$all_viable_records[0]['count'] && $current_page==1 && $number_of_pages==1) {

                $attach_rest = $records_per_page - $al_lenght;

                $from_last = $this->database->selectWithWhere("SELECT cp.id_application, cp.id_user, cp.content, swa.order_number, swa.highlight
                    FROM contests_participants as cp
                    JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
                    WHERE (swa.highlight!='n' OR swa.highlight IS NULL) AND cp.id_contest=? AND swa.order_number>?
                    ORDER BY swa.order_number DESC
                    LIMIT ?", [$id_contest, $start_from_here[0]['order_number'], $attach_rest]);

                foreach ($from_last as $key => $val) {
                    array_push($applications_list, $val);
                }

                //update after attaching
                $al_lenght = count($applications_list);
            }

            //for multiple pages
            if ($al_lenght<$records_per_page && $current_page!=$number_of_pages && $al_lenght!=0) {

                $attach_rest = $records_per_page - $al_lenght;

                $from_last = $this->database->selectWithWhere("SELECT cp.id_application, cp.id_user, cp.content, swa.order_number, swa.highlight
                    FROM contests_participants as cp
                    JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
                    WHERE (swa.highlight!='n' OR swa.highlight IS NULL) AND cp.id_contest=? AND swa.order_number<=?
                    ORDER BY swa.order_number DESC
                    LIMIT ?", [$id_contest, $all_records, $attach_rest+1]);

                $fl_i= 0;
                $fl_lenght = count($from_last);

                foreach ($from_last as $key => $application) {
                    if ($fl_i == $attach_rest) {
                        $start = $application['order_number'];
                        unset($from_last[$key]);
                    } else {
                        $fl_i++;
                    }
                }

                foreach ($from_last as $key => $val) {
                    array_push($applications_list, $val);
                }
            }

            if (isset($fl_lenght) && ($fl_lenght + $al_lenght) == $records_per_page && $current_page == $number_of_pages) {
                $start = 'n';
            }

            if ($al_lenght<$records_per_page && $current_page >= $number_of_pages) {
                if ($al_lenght==0) {
                    $negatives_in_page = $records_per_page;
                } else {
                    $negatives_in_page = $records_per_page - $al_lenght;
                }

                if ($start != 'n' && $al_lenght==0) {
                    $negatives_condition = 'AND order_number<=' . $start;
                } else {
                    $negatives_condition = '';
                }

                $negative_applications = $this->database->selectWithWhere("SELECT cp.id_application, cp.id_user, cp.content, swa.order_number, swa.highlight
                    FROM contests_participants as cp
                    JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
                    WHERE swa.highlight='n' AND cp.id_contest=? $negatives_condition
                    ORDER BY swa.order_number DESC
                    LIMIT ?", [$id_contest, $negatives_in_page+1]);

                $negative_rows = 0;

                foreach ($negative_applications as $key => $application) {
                    if ($negative_rows == $negatives_in_page) {
                        $start = $application['order_number'];
                        unset($negative_applications[$key]);
                    } else {
                        $negative_rows++;
                    }
                }

                foreach ($negative_applications as $key => $val) {
                    array_push($applications_list, $val);
                }
            }

            $files = $this->database->selectWithWhere('SELECT cp.id_application, uf.original_name, uf.name
                FROM uploaded_files as uf
                JOIN applications_files as af ON af.id_file=uf.id_file
                JOIN contests_participants as cp ON cp.id_application=af.id_application
                WHERE cp.id_contest=?', [$id_contest]);

            foreach ($applications_list as &$application) {
                $application['content'] = json_decode($application['content'], true);

                if (isset($application['content']['picture'])) {
                    foreach ($files as $file_key => $file) {
                        if ($file['id_application'] == $application['id_application'])
                            $application['content'] = ['picture' => $files[$file_key]['name']];
                    }
                } elseif (isset($application['content']['files'])) {
                    $application['content']['files'] = [];
                    foreach ($files as $file_key => $file) {
                        if ($file['id_application'] == $application['id_application'])
                            array_push($application['content']['files'], ['original_name' => $file['original_name']]);
                    }
                }
            }

        } elseif ($href=='positive-applications' || $href=='negative-applications') {
            if ($href=='positive-applications') {
                $highlight = 'p';
            } else {
                $highlight = 'n';
            }

            $applications_list = $this->database->selectWithWhere("SELECT cp.id_application, cp.id_user, cp.content, swa.order_number, swa.highlight
            FROM contests_participants as cp
            JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
            WHERE cp.id_contest=? AND swa.highlight=? AND order_number<=? AND order_number>=1
            ORDER BY order_number DESC
            LIMIT ?", [$id_contest, $highlight, $start, $limit_applications]);

            $al_i = 0;
            $al_lenght = count($applications_list);

            foreach ($applications_list as $key => $application) {
                if ($al_i == $al_lenght-1) {
                    $start = $application['order_number'];
                    if ($al_lenght>$records_per_page) {
                        unset($applications_list[$key]);
                    }
                } else {
                    $al_i++;
                }
            }

            $files = $this->database->selectWithWhere('SELECT cp.id_application, uf.original_name, uf.name
                FROM uploaded_files as uf
                JOIN applications_files as af ON af.id_file=uf.id_file
                JOIN contests_participants as cp ON cp.id_application=af.id_application
                JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
                WHERE cp.id_contest=? AND swa.highlight=?', [$id_contest, $highlight]);

            foreach ($applications_list as &$application) {
                $application['content'] = json_decode($application['content'], true);

                if (isset($application['content']['picture'])) {
                    foreach ($files as $file_key => $file) {
                        if ($file['id_application'] == $application['id_application'])
                            $application['content'] = ['picture' => $files[$file_key]['name']];
                    }
                } elseif (isset($application['content']['files'])) {
                    $application['content']['files'] = [];
                    foreach ($files as $file_key => $file) {
                        if ($file['id_application'] == $application['id_application'])
                            array_push($application['content']['files'], ['original_name' => $file['original_name']]);
                    }
                }
            }
        } elseif ($href=='awarded-applications') {

            if ($current_page==1) {
                $condition = "(swa.highlight='1' OR swa.highlight='2' OR swa.highlight='3' OR swa.highlight='4' OR swa.highlight='a')";
            } else {
                $condition = "swa.highlight='a'";
            }
            $applications_list = $this->database->selectWithWhere("SELECT cp.id_application, cp.id_user, cp.content, swa.order_number, swa.highlight
            FROM contests_participants as cp
            JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
            WHERE cp.id_contest=? AND $condition AND swa.order_number<=?
            ORDER BY swa.highlight ASC, swa.order_number DESC
            LIMIT ?", [$id_contest, $start, $limit_applications]);

            $al_i = 0;
            $al_lenght = count($applications_list);

            foreach ($applications_list as $key => $application) {
                if ($al_i == $al_lenght-1) {
                    $start = $application['order_number'];
                    if ($al_lenght>$records_per_page) {
                        unset($applications_list[$key]);
                    }
                } else {
                    $al_i++;
                }
            }

            $files = $this->database->selectWithWhere("SELECT cp.id_application, uf.original_name, uf.name
                FROM uploaded_files as uf
                JOIN applications_files as af ON af.id_file=uf.id_file
                JOIN contests_participants as cp ON cp.id_application=af.id_application
                JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
                WHERE cp.id_contest=? AND (swa.highlight='1' OR swa.highlight='2' OR swa.highlight='3' OR swa.highlight='4' OR swa.highlight='a')", [$id_contest]);


            foreach ($applications_list as &$application) {
                $application['content'] = json_decode($application['content'], true);

                if (isset($application['content']['picture'])) {
                    foreach ($files as $file_key => $file) {
                        if ($file['id_application'] == $application['id_application'])
                            $application['content'] = ['picture' => $files[$file_key]['name']];
                    }
                } elseif (isset($application['content']['files'])) {
                    $application['content']['files'] = [];
                    foreach ($files as $file_key => $file) {
                        if ($file['id_application'] == $application['id_application'])
                            array_push($application['content']['files'], ['original_name' => $file['original_name']]);
                    }
                }
            }
        }
        return ['applications_list' => $applications_list, 'start' => $start];
    }

    public function selectWinnerApplicationDetails($id_contest, $order_number)
    {
        $details = $this->database->selectWithWhere('SELECT u.login, cp.id_user, swa.highlight, cp.content
            FROM contests_participants as cp
            JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
            JOIN users as u ON u.id_user=cp.id_user
            WHERE cp.id_contest=? AND swa.order_number=?', [$id_contest, $order_number]);

        $details[0]['content'] = json_decode($details[0]['content'], true);

        if (isset($details[0]['content']['files']) || isset($details[0]['content']['picture'])) {

            if (isset($details[0]['content']['files'])) {
                $files = $this->database->selectWithWhere('SELECT uf.name, uf.original_name
                    FROM uploaded_files as uf
                    JOIN applications_files as af ON af.id_file=uf.id_file
                    JOIN contests_participants as cp ON cp.id_application=af.id_application
                    JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
                    WHERE cp.id_contest=? AND swa.order_number=?', [$id_contest, $order_number]);

                $count_files = 0;
                $details[0]['content']['files'] = [];
                foreach ($files as $file) {
                    $count_files++;
                    array_push($details[0]['content']['files'], ['original_name' => $file['original_name'], 'name' => $file['name']]);
                }
                $details[0]['count_files'] = $count_files;
            } elseif (isset($details[0]['content']['picture'])) {
                $files = $this->database->selectWithWhere('SELECT uf.name
                    FROM uploaded_files as uf
                    JOIN applications_files as af ON af.id_file=uf.id_file
                    JOIN contests_participants as cp ON cp.id_application=af.id_application
                    JOIN select_winner_applications as swa ON swa.id_application=cp.id_application
                    WHERE cp.id_contest=? AND swa.order_number=?', [$id_contest, $order_number]);

                $details[0]['content']['picture'] = $files[0]['name'];
            }
        }

        echo json_encode(['status'=>'success', 'details' => $details[0]]);
    }

    public function selectWinnerWinnersList($id_contest)
    {
        $winners = $this->database->selectWithWhere("SELECT cp.id_application, cp.content, u.login, cp.position, cp.id_user
            FROM contests_participants as cp
            JOIN users as u ON u.id_user=cp.id_user
            WHERE (cp.position='1' OR cp.position='2' OR cp.position='3' OR cp.position='4' OR cp.position='a') AND cp.id_contest=?
            ORDER BY cp.position ASC", [$id_contest]);

        $files = $this->database->selectWithWhere("SELECT cp.id_application, uf.original_name, uf.name
            FROM uploaded_files as uf
            JOIN applications_files as af ON af.id_file=uf.id_file
            JOIN contests_participants as cp ON cp.id_application=af.id_application
            WHERE cp.id_contest=? AND (cp.position='1' OR cp.position='2' OR cp.position='3' OR cp.position='4' OR cp.position='a')", [$id_contest]);

        foreach ($winners as &$application) {
            $application['content'] = json_decode($application['content'], true);

            if (isset($application['content']['picture'])) {
                foreach ($files as $file_key => $file) {
                    if ($file['id_application'] == $application['id_application'])
                        $application['content'] = ['picture' => $files[$file_key]['name']];
                }
            } elseif (isset($application['content']['files'])) {
                $application['content']['files'] = [];
                foreach ($files as $file_key => $file) {
                    if ($file['id_application'] == $application['id_application'])
                        array_push($application['content']['files'], ['original_name' => $file['original_name'], 'name' => $file['name']]);
                }
            }
        }

        return $winners;
    }

    public function loggedUserContest()
    {
        $newest_contest = $this->database->selectWithWhere('SELECT id_contest
            FROM contests
            WHERE creator_id=?
            ORDER BY id_contest DESC
            LIMIT 1', [$this->user->getId()]);

        if (!isset($newest_contest[0]['id_contest']))
            $newest_contest[0]['id_contest'] = 0;

        return ['newest_contest'=>$newest_contest[0]['id_contest']];
    }

    public function displayLoggedUserContests($start, $page)
    {
        $records_per_page = 8;

        if ($page==1) {
            $start = $this->database->selectWithWhere('SELECT id_contest
                FROM contests
                WHERE creator_id=?
                ORDER BY id_contest DESC
                LIMIT 1', [$this->user->getId()]);

            if (!isset($start[0]['id_contest'])) {
                $start = 0;
            } else {
                $start = $start[0]['id_contest'];
            }
        }

        $contests = $this->database->selectWithWhere('SELECT c.id_contest, c.contest_type, c.link, c.avaible_for, c.title, c.status, COUNT(cp.id_application) as count_participants
            FROM contests as c
            LEFT JOIN contests_participants as cp ON cp.id_contest=c.id_contest
            WHERE c.creator_id=? AND c.id_contest<=?
            GROUP BY c.id_contest
            ORDER BY c.id_contest DESC
            LIMIT ?', [$this->user->getId(), $start, $records_per_page+1]);

        $i = 0;
        $lenght = count($contests);

        foreach ($contests as $key => $list) {
            if ($i == $lenght-1) {
                $start = $list['id_contest'];
                if ($lenght>$records_per_page) {
                    unset($contests[$key]);
                }
            } else {
                $i++;
            }
        }

        foreach ($contests as &$contest) {
            $img_name = $this->database->selectWithWhere('SELECT uf.name
                FROM uploaded_files as uf
                JOIN contests_images as ci ON ci.id_file=uf.id_file
                JOIN contests as c ON c.id_contest=ci.id_contest
                WHERE c.id_contest=? AND ci.type=0
                LIMIT 1', [$contest['id_contest']]);

            if (!isset($img_name[0]['name'])) {
                $img_name[0]['name'] = null;
            }
            $contest['img_name'] = $img_name[0]['name'];
        }

        if ($lenght<=$records_per_page) {
            $stop = true;
        } else {
            $stop = false;
        }

        return ['contests_list'=>$contests, 'start'=>$start, 'stop'=>$stop];
    }

    public function indexMostPopular()
    {
        $contests = $this->database->selectWithWhere("SELECT c.id_contest, COUNT(cp.id_contest) as count_participants, c.title, cat.name as category_name, c.link, u.login as creator_login
            FROM contests as c
            JOIN contests_participants as cp ON cp.id_contest=c.id_contest
            JOIN categories as cat ON c.id_category=cat.id_category
            JOIN users as u ON u.id_user=c.creator_id
            WHERE c.status=1 AND c.avaible_for=2
            GROUP BY c.id_contest, cat.name, u.login
            HAVING COUNT(cp.id_application)>=10
            ORDER BY COUNT(cp.id_contest) DESC
            LIMIT ?", [5]);

        if (count($contests)>=3) {
            foreach ($contests as &$contest) {
                $img_name = $this->database->selectWithWhere('SELECT uf.name
                    FROM uploaded_files as uf
                    JOIN contests_images as ci ON ci.id_file=uf.id_file
                    JOIN contests as c ON c.id_contest=ci.id_contest
                    WHERE c.id_contest=?
                    LIMIT 1', [$contest['id_contest']]);

                if (!isset($img_name[0]['name'])) {
                    $img_name[0]['name'] = null;
                }
                $contest['img_name'] = $img_name[0]['name'];
            }
        } else {
            $contests = null;
        }

        return $contests;
    }
}
