<?php
namespace Qtbat\Model;

use Qtbat\Exception\PageError;
use Exception;

class Groups
{
    function __construct($database, $user)
    {
        $this->database = $database;
        $this->user = $user;
    }

    public function data($group_name)
    {
        $loggeduser_Id = $this->user->getId();

        $group_id =  $this->database->selectWithWhere('SELECT id_group
            FROM groups
            WHERE lower(unique_name)=lower(?)', [$group_name]);

        if (!isset($group_id[0]['id_group']))
            throw new PageError(404);

        $data = $this->database->selectWithWhere('SELECT name, unique_name, type, description, create_date::timestamp::date
            FROM groups
            WHERE id_group=?', [$group_id[0]['id_group']]);

        if (strtolower($data[0]['unique_name'])!=strtolower($group_name)) {
            throw new PageError(404);
        } else {
            $user_info = $this->database->selectWithWhere('SELECT u_g.id_user, u_g.rank
                FROM users_groups as u_g
                WHERE id_group=? AND member_status=1 AND id_user=?', [$group_id[0]['id_group'], $loggeduser_Id]);

            if (empty($user_info) && $data[0]['type']==1)
                throw new PageError(403);

            if (empty($user_info)) {
                $user_info[0] = null;
            }

            if (empty($user_info[0])) {
                $allow_join = true;
            } else {
                $allow_join = false;
            }

            return ['group_id' => $group_id[0]['id_group'], 'data' => $data[0], 'user_info' => $user_info[0], 'allow_join' => $allow_join];
        }
    }

    public function displayContestsList($group_name, $start, $href, $current_page)
    {
        $records_per_page = 6;

        if ($href=='ongoing-contests') {
            $condition = 'c.status=1';
        } elseif ($href=='finished-contests') {
            $condition = 'c.status=2';
        } else {
            $condition = '(c.status=1 OR c.status=2)';
        }

        $group_id =  $this->database->selectWithWhere('SELECT id_group
            FROM groups
            WHERE lower(unique_name)=lower(?)', [$group_name]);
        $group_id = $group_id[0]['id_group'];

        if ($current_page==1) {
            $start = $this->database->selectWithWhere('SELECT id_contest
                FROM contests_groups
                WHERE id_group=?
                ORDER BY id_contest DESC
                LIMIT 1', [$group_id]);

            if (!isset($start[0]['id_contest'])) {
                $start = 0;
            } else {
                $start = $start[0]['id_contest'];
            }
        }

        if ($current_page==1 && $href!='ongoing-contests' && $href!='finished-contests') {
            $count_attached = $this->database->selectWithWhere('SELECT COUNT(id_contest) as count
                FROM contests_groups
                WHERE id_group=? AND attached=true', [$group_id]);
            $records_per_page -= $count_attached[0]['count'];

            $attached_contests = $this->database->selectWithWhere("SELECT cg.attached, c.id_contest, c.number_of_winners, c.contest_type as type, c.title, cat.name as category_name, c.status, c.link, c.creator_id, u.login as creator_login
                FROM contests as c
                JOIN categories as cat ON c.id_category=cat.id_category
                JOIN users as u ON u.id_user=c.creator_id
                JOIN contests_groups as cg ON cg.id_contest=c.id_contest
                WHERE cg.id_group=? AND cg.attached=true
                ORDER BY c.id_contest ASC", [$group_id]);
        }

        $group_contests = $this->database->selectWithWhere("SELECT cg.attached, c.id_contest, c.number_of_winners, c.contest_type as type, c.title, cat.name as category_name, c.status, c.link, c.creator_id, u.login as creator_login
            FROM contests as c
            JOIN categories as cat ON c.id_category=cat.id_category
            JOIN users as u ON u.id_user=c.creator_id
            JOIN contests_groups as cg ON cg.id_contest=c.id_contest
            WHERE cg.id_group=? AND cg.id_contest<=? AND cg.attached=false AND $condition
            ORDER BY c.id_contest DESC
            LIMIT ?", [$group_id, $start, $records_per_page+1]);

        if ($current_page==1 && $href!='ongoing-contests' && $href!='finished-contests') {
            foreach ($attached_contests as $key => $list) {
                array_unshift($group_contests, $attached_contests[$key]);
            }
        }

        $gc_i = 0;
        $gc_lenght = count($group_contests);

        foreach ($group_contests as $key => $list) {
            if ($gc_i == $gc_lenght-1) {
                $start = $list['id_contest'];
                if ($gc_lenght>$records_per_page) {
                    unset($group_contests[$key]);
                }
            } else {
                $gc_i++;
            }
        }

        foreach ($group_contests as &$contest) {
            $img_name = $this->database->selectWithWhere('SELECT uf.name
                FROM uploaded_files as uf
                JOIN contests_images as ci ON ci.id_file=uf.id_file
                JOIN contests as c ON c.id_contest=ci.id_contest
                WHERE c.id_contest=? AND ci.type=0
                LIMIT 1', [$contest['id_contest']]);

            if (!isset($img_name[0]['name'])) {
                $img_name[0]['name'] = null;
            }
            $contest['img_name'] = $img_name[0]['name'];
        }

        if ($gc_lenght<=$records_per_page) {
            $stop = true;
        } else {
            $stop = false;
        }

        return ['contests_list' => $group_contests, 'start' => $start, 'stop' => $stop];
    }

    public function membersStats($group_id)
    {
        $count_group_members = $this->database->selectWithwhere('SELECT COUNT(id_user) as count
            FROM users_groups
            WHERE id_group=?
            GROUP BY id_group', [$group_id]);
        $count_admins = $this->database->selectWithWhere('SELECT COUNT(id_user) as count FROM users_groups WHERE id_group=? AND rank=1 GROUP BY id_group', [$group_id]);

        return ['count_group_members' => $count_group_members[0]['count'], 'count_admins' => $count_admins[0]['count']];
    }

    public function displayMembers($group_name, $start, $search_value)
    {
        $records_per_page = 15;

        $group_id =  $this->database->selectWithWhere('SELECT id_group
            FROM groups
            WHERE lower(unique_name)=lower(?)', [$group_name]);
        $group_id = $group_id[0]['id_group'];

        $members_list = $this->database->selectWithWhere("SELECT u.id_user, u.login, u_g.rank, u_g.joined_date::timestamp
            FROM users as u
            INNER JOIN users_groups as u_g ON u.id_user=u_g.id_user
            WHERE u_g.id_group=? AND u_g.member_status=1 AND u_g.id_user>=? AND lower(u.login) LIKE(?)
            ORDER BY u_g.rank DESC, u_g.id_user ASC
            LIMIT ?", [$group_id, $start, "$search_value%", $records_per_page+1]);

        $ml_i = 0;
        $ml_lenght = count($members_list);

        foreach ($members_list as $key => $list) {
            if ($ml_i == $ml_lenght-1) {
                $start = $list['id_user'];
                if ($ml_lenght>$records_per_page) {
                    unset($members_list[$key]);
                }
            } else {
                $ml_i++;
            }
        }

        if ($ml_lenght<=$records_per_page) {
            $stop = true;
        } else {
            $stop = false;
        }

        if (empty($members_list)) {
            $is_empty = true;
        } else {
            $is_empty = false;
        }

        foreach ($members_list as $list) {
            $avatar_path[$list['id_user']] = $this->user->getAvatarPath($list['id_user']);
        }
        if (empty($avatar_path)) {
            $avatar_path = NULL;
        }

        return ['members_list'=>$members_list, 'avatar_path'=>$avatar_path, 'is_empty'=>$is_empty, 'start' => $start,'stop' => $stop];
    }

    public function userInteraction($group_name)
    {
        $loggeduser_Id = $this->user->getId();
        $group_id =  $this->database->selectWithWhere('SELECT id_group FROM groups WHERE lower(unique_name)=lower(?)', [$group_name[1]]);
        if (!isset($group_id[0]['id_group']))
            throw new PageError(404);

        $group_id = $group_id[0]['id_group'];

        if (isset($_POST['join']) && $_POST['join']!=null) {
            try {
                $is_group_open = $this->database->selectWithWhere('SELECT type FROM groups WHERE id_group=?', [$group_id]);

                if ($is_group_open[0]['type'] == 0) {
                    $member_status = $this->database->selectWithWhere('SELECT member_status FROM users_groups WHERE id_group=? AND id_user=?', [$group_id, $loggeduser_Id]);

                    if (isset($member_status[0]['member_status'])) {
                        if ($member_status[0]['member_status']==0) {
                            $this->database->deleteOrUpdateWhere('UPDATE users_groups SET member_status=1 WHERE id_group=? AND id_user=? AND member_status=0', [$group_id, $loggeduser_Id]);

                            $this->database->deleteOrUpdateWhere('DELETE FROM notifications as n
                                USING groups_notifications as gn
                                WHERE gn.id_notification=n.id_notification AND gn.id_group=? AND n.id_receiver=?', [$group_id, $loggeduser_Id]);

                            $status = 'success';
                            $message = null;
                        } else {
                            throw new \Exception('Jesteś już członkiem tej grupy');
                        }
                    } else {
                        $this->database->insert('users_groups', [
                            'id_user'=>$loggeduser_Id,
                            'id_group'=>$group_id,
                            'rank'=>0,
                            'member_status'=>1]);

                        $this->database->deleteOrUpdateWhere('DELETE FROM notifications as n
                            USING groups_notifications as gn
                            WHERE gn.id_notification=n.id_notification AND gn.id_group=? AND n.id_receiver=?', [$group_id, $loggeduser_Id]);

                        $status = 'success';
                        $message = null;
                    }
                } else {
                    throw new \Exception('Nie możesz dołączyć do tej grupy, ponieważ jest ona prywatna.');
                }
            } catch (\Exception $e) {
                $status = 'error';
                $message = $e->getMessage();
            }
            echo json_encode(['message'=>$message, 'status'=>$status]);

        } elseif (isset($_POST['leave']) && $_POST['leave']!=null) {

            try {
                $is_group_admin = $this->database->selectWithWhere('SELECT rank FROM users_groups WHERE id_user=? AND id_group=?', [$this->user->getId(), $group_id]);

                if (empty($is_group_admin)) {
                    throw new \Exception('Nie jesteś członkiem tej grupy');
                } else {
                    if ($is_group_admin[0]['rank']==1) {
                        $count_admins = $this->database->selectWithWhere('SELECT COUNT(id_user) as count FROM users_groups WHERE rank=1 AND id_group=?', [$group_id]);
                    } else {
                        $count_admins[0]['count'] = null;
                    }

                    $count_members = $this->database->selectWithwhere('SELECT COUNT(id_user) as count FROM users_groups WHERE id_group=? GROUP BY id_group', [$group_id]);

                    if ($is_group_admin[0]['rank']==1 && $count_admins[0]['count']==1 && $count_members[0]['count']>1) {
                        throw new \Exception('Nie możesz opuścić tej grupy, ponieważ jesteś jej jedynym administratorem.');
                    } else {
                        if ($count_members[0]['count'] == 1) {
                            $this->database->deleteOrUpdateWhere('DELETE FROM contests_groups WHERE id_group=?', [$group_id]);
                            $this->database->deleteOrUpdateWhere('DELETE FROM users_groups WHERE id_group=?', [$group_id]);
                            $this->database->deleteOrUpdateWhere('DELETE FROM groups WHERE id_group=?', [$group_id]);
                        } else {
                            $this->database->deleteOrUpdateWhere('DELETE FROM users_groups WHERE id_user=? AND id_group=?', [$this->user->getId(), $group_id]);
                        }
                        $status = 'success';
                        $message = null;
                    }
                }
            } catch (\Exception $e) {
                $status = 'error';
                $message = $e->getMessage();
            }
            echo json_encode(['message'=>$message, 'status'=>$status]);

        } else {
            throw new PageError(404);
        }
    }

    public function edit($group_name)
    {
        $loggeduser_Id = $this->user->getId();
        $group_id =  $this->database->selectWithWhere('SELECT id_group FROM groups WHERE lower(unique_name)=lower(?)', [$group_name[1]]);
        if (!isset($group_id[0])) {
            throw new PageError(404);
        } else {
            $group_id = $group_id[0]['id_group'];

            if (isset($_POST['rank'])) {
                try {
                    $is_group_admin = $this->database->selectWithWhere('SELECT rank FROM users_groups WHERE id_user=? AND id_group=?', [$this->user->getId(), $group_id]);

                    if ($is_group_admin[0]['rank']==1) {
                        if ($_POST['rank']=='0' || $_POST['rank']=='1') {
                            $this->database->deleteOrUpdateWhere('UPDATE users_groups SET rank=? WHERE id_user=? AND id_group=?', [$_POST['rank'], $_POST['id_user'], $group_id]);
                            $status = 'success';
                            $message = null;
                        } else {
                            throw new \Exception('Wystąpił błąd');
                        }
                    } else {
                        throw new \Exception('Nie masz uprawnień aby zmieniać rangi członkom tej grupy.');
                    }
                } catch (\Exception $e) {
                    $status = 'error';
                    $message = $e->getMessage();
                }
                echo json_encode(['status'=>$status, 'message'=>$message]);

            } elseif (isset($_POST['group_name'])) {
                try {
                    $is_group_admin = $this->database->selectWithWhere('SELECT rank FROM users_groups WHERE id_user=? AND id_group=?', [$this->user->getId(), $group_id]);

                    if ($is_group_admin[0]['rank']==1) {
                        $message = null;
                        $status = 'success';
                        validateInput($_POST['group_name'], 'nazwa', 'group_name', $this->database);
                        $this->database->deleteOrUpdateWhere('UPDATE groups SET name=? WHERE id_group=?', [clearString($_POST['group_name']), $group_id]);
                    } else {
                        throw new \Exception('Nie masz uprawnień aby edytować tę grupę.');
                    }
                } catch (\Exception $e) {
                    $message = $e->getMessage();
                    $status = 'error';
                }
                echo json_encode(['status'=>$status, 'message'=>$message]);

            } elseif (isset($_POST['description'])) {
                $this->database->deleteOrUpdateWhere('UPDATE groups SET description=? WHERE id_group=?', [$_POST['description'], $group_id]);

            } elseif (isset($_POST['type'])) {
                try {
                    $is_group_admin = $this->database->selectWithWhere('SELECT rank FROM users_groups WHERE id_user=? AND id_group=?', [$this->user->getId(), $group_id]);

                    if ($is_group_admin[0]['rank']==1) {
                        if ($_POST['type']=='0' || $_POST['type']=='1') {
                            $status = 'success';
                            $message = null;
                            $this->database->deleteOrUpdateWhere('UPDATE groups SET type=? WHERE id_group=?', [$_POST['type'], $group_id]);
                        } else {
                            throw new \Exception('Wystąpił błąd');
                        }
                    } else {
                        throw new \Exception('Nie możesz modyfikować tej grupy, ponieważ nie jesteś jej administratorem.');
                    }
                } catch (\Exception $e) {
                    $status = 'error';
                    $message = $e->getMessage();
                }
                echo json_encode(['status'=>$status, 'message'=>$message]);

            } elseif (isset($_POST['new_member_login'])) {
                try {
                    $is_group_admin = $this->database->selectWithWhere('SELECT rank FROM users_groups WHERE id_user=? AND id_group=?', [$this->user->getId(), $group_id]);
                    if (!isset($is_group_admin[0]['rank'])) {
                        throw new PageError(404);
                    } else {
                        if ($is_group_admin[0]['rank']==1) {
                            $id_user = $this->database->selectWithWhere('SELECT id_user FROM users WHERE lower(login)=lower(?)', [$_POST['new_member_login']]);

                            if (!empty($id_user)) {
                                $info = $this->database->selectWithWhere('SELECT id_user, member_status FROM users_groups WHERE id_user=? AND id_group=?', [$id_user[0]['id_user'], $group_id]);
                                if (!empty($info) && $info[0]['member_status'] == 0) {
                                    throw new \Exception('Zaproszenie zostało już wcześniej wysłane.');
                                } elseif (!empty($info) && $info[0]['member_status'] == 1) {
                                    throw new \Exception('Użytkownik jest już członkiem tej grupy.');
                                } else {
                                    $this->database->insert('users_groups', [
                                        'id_user'=>$id_user[0]['id_user'],
                                        'id_group'=>$group_id,
                                        'member_status'=>0]);

                                    //notifications
                                    $this->user->insertNotification(6, $group_id, $id_user[0]['id_user']);

                                    $message = true;
                                    $status = 'success';
                                }
                            } else {
                                throw new \Exception('Podany użytkownik nie istnieje!');
                            }
                        } else {
                            throw new \Exception('Nie masz uprawnień do tego, aby zapraszać użytkowników do tej grupy.');
                        }
                    }
                } catch (\Exception $e) {
                    $message = $e->getMessage();
                    $status = 'error';
                }
                echo json_encode(['status'=>$status, 'message'=>$message]);

            } elseif (isset($_POST['id_delete']) && $_POST['id_delete']!=null) {
                try {
                    $is_group_admin = $this->database->selectWithWhere('SELECT rank FROM users_groups WHERE id_user=? AND id_group=?', [$this->user->getId(), $group_id]);

                    if ($is_group_admin[0]['rank']==1) {
                        $is_user_admin = $this->database->selectWithWhere('SELECT rank FROM users_groups WHERE id_user=? AND id_group=?', [$_POST['id_delete'], $group_id]);
                        if (!empty($is_user_admin)) {
                            if ($is_user_admin[0]['rank']==1) {
                                throw new \Exception('Nie możesz usunąć tego użytkownika, ponieważ jest on administratorem tej grupy');
                            } else {
                                $this->database->deleteOrUpdateWhere('DELETE FROM users_groups WHERE id_user=? AND id_group=?', [$_POST['id_delete'], $group_id]);
                                $status = 'success';
                                $message = null;
                            }
                        } else {
                            throw new \Exception('Ten użytkownik nie jest członkiem tej grupy');
                        }
                    } else {
                        throw new \Exception('Nie masz uprawnień do tego, aby usuwać użytkowników w tej grupie.');
                    }
                } catch (\Exception $e) {
                    $status = 'error';
                    $message = $e->getMessage();
                }
                echo json_encode(['status'=>$status, 'message'=>$message]);

            } elseif (isset($_POST['delete_group']) && $_POST['delete_group']!=null) {
                try {
                    $is_group_admin = $this->database->selectWithWhere('SELECT rank FROM users_groups WHERE id_user=? AND id_group=?', [$this->user->getId(), $group_id]);
                    if (!isset($is_group_admin[0]['rank'])) {
                           $is_group_admin[0]['rank'] = NULL;
                    }

                    if ($is_group_admin[0]['rank']==1) {
                        $this->database->deleteOrUpdateWhere('DELETE FROM contests_groups WHERE id_group=?', [$group_id]);
                        $this->database->deleteOrUpdateWhere('DELETE FROM users_groups WHERE id_group=?', [$group_id]);
                        $this->database->deleteOrUpdateWhere('DELETE FROM notifications as n
                            USING groups_notifications as gn
                            WHERE gn.id_notification=n.id_notification AND gn.id_group=?', [$group_id]);
                        $this->database->deleteOrUpdateWhere('DELETE FROM groups WHERE id_group=?', [$group_id]);
                        $status = 'success';
                        $message = null;
                    } else {
                        throw new \Exception('Nie masz uprawnień do tego, aby usunąć tą grupę.');
                    }
                } catch (\Exception $e) {
                    $status = 'error';
                    $message = $e->getMessage();
                }
                echo json_encode(['message'=>$message, 'status'=>$status]);

            } elseif (isset($_POST['attach_contest'])) {
                try {
                    $is_group_admin = $this->database->selectWithWhere('SELECT rank FROM users_groups WHERE id_user=? AND id_group=?', [$this->user->getId(), $group_id]);
                    if ($is_group_admin[0]['rank']==1) {
                        $count_attached = $this->database->selectWithWhere('SELECT COUNT(id_contest) as count
                            FROM contests_groups
                            WHERE id_group=? AND attached=true', [$group_id]);
                        if ($count_attached[0]['count']<3) {
                            $is_attached = $this->database->selectWithWhere('SELECT attached FROM contests_groups WHERE id_group=? AND id_contest=?', [$group_id, $_POST['attach_contest']]);
                            if (isset($is_attached[0]['attached'])) {
                                if ($is_attached[0]['attached']===false) {
                                    $this->database->deleteOrUpdateWhere('UPDATE contests_groups SET attached=true WHERE id_contest=? AND id_group=?', [$_POST['attach_contest'], $group_id]);
                                    $status = 'success';
                                    $message = null;
                                } else {
                                    throw new \Exception('Tej konkurs jest już przyczepiony');
                                }
                            } else {
                                throw new \Exception('Konkurs, który chcesz przyczepić nie został udostępniony dla tej grupy');
                            }
                        } else {
                            throw new \Exception('Maksymalnie możesz przyczepić 3 konkursy');
                        }
                    } else {
                        throw new \Exception('Nie jesteś administratorem tej grupy');
                    }
                } catch (\Exception $e) {
                    $status = 'error';
                    $message = $e->getMessage();
                }
                echo json_encode(['message'=>$message, 'status'=>$status]);

            } elseif (isset($_POST['detach_contest'])) {
                try {
                    $is_group_admin = $this->database->selectWithWhere('SELECT rank FROM users_groups WHERE id_user=? AND id_group=?', [$this->user->getId(), $group_id]);
                    if ($is_group_admin[0]['rank']==1) {
                        $is_attached = $this->database->selectWithWhere('SELECT attached FROM contests_groups WHERE id_group=? AND id_contest=?', [$group_id, $_POST['detach_contest']]);
                        if (isset($is_attached[0]['attached'])) {
                            if ($is_attached[0]['attached']===true) {
                                $this->database->deleteOrUpdateWhere('UPDATE contests_groups SET attached=false WHERE id_contest=? AND id_group=?', [$_POST['detach_contest'], $group_id]);
                                $status = 'success';
                                $message = null;
                            } else {
                                throw new \Exception('Tej konkurs nie jest przyczepiony');
                            }
                        } else {
                            throw new \Exception('Konkurs, który chcesz przyczepić nie został udostępniony dla tej grupy');
                        }
                    } else {
                        throw new \Exception('Nie jesteś administratorem tej grupy');
                    }
                } catch (\Exception $e) {
                    $status = 'error';
                    $message = $e->getMessage();
                }
                echo json_encode(['message'=>$message, 'status'=>$status]);

            } else {
                throw new PageError(404);
            }
        }
    }

    public function createGroup()
    {
        $loggeduser_Id = $this->user->getId();

        validateInput($_POST['unique_name'], 'unikalna nazwa', 'group_unique_name', $this->database);
        validateInput(clearString($_POST['name']), 'nazwa', 'group_name', $this->database);

        if (empty($_POST['description']) || !isset($_POST['description'])) {
            $description = NULL;
        } else {
            $description = $_POST['description'];
        }
        $this->database->insert('groups', [
            'unique_name' => $_POST['unique_name'],
            'name' => clearString($_POST['name']),
            'description' => $description,
            'type' => $_POST['type']]);
        $added_group_id=$this->database->selectWithWhere('SELECT id_group FROM groups WHERE unique_name=?', [$_POST['unique_name']]);
        $this->database->insert('users_groups', [
            'id_user' => $loggeduser_Id,
            'id_group' => $added_group_id[0]['id_group'],
            'rank' => 1,
            'member_status' => 1]);
    }

    public function panelGroups()
    {
        $groups_list = $this->database->selectWithWhere('SELECT g.name, g.unique_name
            FROM users_groups as u_g
            INNER JOIN groups as g ON g.id_group=u_g.id_group
            WHERE u_g.id_user=? AND u_g.member_status=1
            ORDER BY u_g.joined_date DESC', [$this->user->getId()]);

        return ['groups_list'=>$groups_list];
    }

    public function invitationConfirm()
    {
        try {
            $loggeduser_Id = $this->user->getId();

            if (isset($_POST['id_accept'])) {
                if (filter_input(INPUT_POST, 'id_accept', FILTER_VALIDATE_INT)===false) {
                    throw new PageError(404);
                } else {
                    $id_group = $this->database->selectWithWhere('SELECT id_group
                        FROM groups_notifications
                        WHERE id_notification=?', [$_POST['id_accept']]);

                    if (isset($id_group[0]['id_group'])) {
                        $does_user_own_notification = $this->database->selectWithWhere('SELECT id_receiver
                            FROM notifications
                            WHERE id_notification=? AND id_receiver=?', [$_POST['id_accept'], $loggeduser_Id]);

                        if (!isset($does_user_own_notification[0]['id_receiver'])) {
                            throw new PageError(404);
                        } else {
                            $group_exists = $this->database->selectWithWhere("SELECT id_group
                                FROM groups
                                WHERE id_group=?", [$id_group[0]['id_group']]);

                            if (isset($group_exists[0]['id_group'])) {
                                $is_already_member = $this->database->selectWithWhere('SELECT id_user
                                    FROM users_groups
                                    WHERE id_group=? AND id_user=? AND member_status=1', [$id_group[0]['id_group'], $loggeduser_Id]);
                                if (!isset($is_already_member[0]['id_user'])) {
                                    $this->database->deleteOrUpdateWhere('UPDATE users_groups
                                        SET member_status=1
                                        WHERE id_user=? AND id_group=?', [$loggeduser_Id, $id_group[0]['id_group']]);

                                    //notifications
                                    $this->database->deleteOrUpdateWhere('DELETE FROM groups_notifications
                                        WHERE id_notification=?', [$_POST['id_accept']]);
                                    $this->database->deleteOrUpdateWhere('DELETE FROM notifications
                                        WHERE id_notification=?', [$_POST['id_accept']]);

                                    $status = 'success';
                                    $message = null;
                                } else {
                                    throw new \Exception('Jesteś już członkiem tej grupy');
                                }
                            } else {
                                throw new \Exception('Grupa, do której próbujesz dołączyć nie istnieje.');
                            }
                        }
                    } else {
                        throw new PageError(404);
                    }
                }
            } elseif (isset($_POST['id_reject'])) {
                if (filter_input(INPUT_POST, 'id_reject', FILTER_VALIDATE_INT)===false) {
                    throw new PageError(404);
                } else {
                    $id_group = $this->database->selectWithWhere('SELECT id_group
                        FROM groups_notifications
                        WHERE id_notification=?', [$_POST['id_reject']]);

                    if (isset($id_group[0]['id_group'])) {
                        $group_exists = $this->database->selectWithWhere("SELECT id_group
                            FROM groups
                            WHERE id_group=?", [$id_group[0]['id_group']]);

                        if (isset($group_exists[0]['id_group'])) {
                            $does_user_own_notification = $this->database->selectWithWhere('SELECT id_receiver
                                FROM notifications
                                WHERE id_notification=? AND id_receiver=?', [$_POST['id_reject'], $loggeduser_Id]);

                            if (!isset($does_user_own_notification[0]['id_receiver'])) {
                                throw new PageError(404);
                            } else {
                                $this->database->deleteOrUpdateWhere('DELETE FROM users_groups
                                    WHERE id_user=? AND id_group=?', [$loggeduser_Id, $id_group[0]['id_group']]);

                                //notifications
                                $this->database->deleteOrUpdateWhere('DELETE FROM groups_notifications
                                    WHERE id_notification=?', [$_POST['id_reject']]);
                                $this->database->deleteOrUpdateWhere('DELETE FROM notifications
                                    WHERE id_notification=?', [$_POST['id_reject']]);
                            }
                        } else {
                            throw new \Exception('Ta grupa nie istnieje');
                        }
                    } else {
                        throw new PageError(404);
                    }
                }
            } else {
                throw new PageError(404);
            }
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        echo json_encode(['status'=>$status, 'message'=>$message]);
    }
}
