<?php
namespace Qtbat\Model;

use Exception;

class User
{
    private $database;
    private $config;
    private $id = null;
    private $logged = false;
    private $data = null;

    public function __construct($database, $config)
    {
        $this->database = $database;
        $this->config = $config;
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
                $this->logged = true;
                $this->id = $_SESSION['user_id'];
            }
        }
    }

    public function isLogged()
    {
        return $this->logged;
    }

    public function getId()
    {
        return $this->id;
    }

    public function register()
    {
        $login = filter_input(INPUT_POST, 'login');
        $email = filter_input(INPUT_POST, 'email');
        $password = filter_input(INPUT_POST, 'password');
        $repeatpassword = filter_input(INPUT_POST, 'repeatpassword');
        $name = filter_input(INPUT_POST, 'name');
        $location = filter_input(INPUT_POST, 'location');
        $rules = filter_input(INPUT_POST, 'rules');
        if ($rules=='1') {
            validateInput($login, 'login', 'login', $this->database);
            validateInput($email, 'e-mail', 'email', $this->database);
            validateInput($password, 'hasło', 'password');
            if ($password == $repeatpassword) {
                if ($login=='groups' || $login=='login' || $login=='register' || $login=='activate' || $login=='logout') {
                    throw new \Exception('Niedozwolony login!');
                } else {
                    if (!empty($name)) {
                        validateInput($location, 'imię i nazwisko', 'name');
                    } else {
                        $name = NULL;
                    }
                    if (!empty($location)) {
                        validateInput($name, 'lokalizacja', 'location');
                    } else {
                        $location = NULL;
                    }
                    $codePassword = password_hash($password, PASSWORD_BCRYPT);
                    $activationHash = hashFromTimeAdd(30, $email);
                    $this->database->insert('users', ['login'=>$login, 'email'=>$email, 'password'=>$codePassword, 'name'=>$name, 'location'=>$location, 'register_ip'=>$_SERVER['REMOTE_ADDR'], 'activation_hash'=>$activationHash]);
                    $mail = new \Qtbat\Engine\Mail();
                    $mail->set($email, $login, 'Rejestracja w '.APP_NAME, 'activate.html.twig', [
                        'hash' => $activationHash
                    ]);
                    $mail->send();
                    return true;
                }
            } else {
                throw new Exception('Obydwa hasła muszą być takie same!');
            }
        } else {
            throw new Exception('Akceptacja regulaminu jest konieczna!');
        }
    }

    public function activate($hash)
    {
        $query = $this->database->deleteOrUpdateWhere('UPDATE users SET status=1, activation_hash=null WHERE activation_hash=?', [$hash], true);
        return ($query == 1) ? true : false;
    }

    public function login()
    {
        $login = filter_input(INPUT_POST, 'login');
        $password = filter_input(INPUT_POST, 'password');
        //SELECT encode(bytea_data, 'hex') FROM my_table;
        $check = $this->database->selectWithWhere('SELECT id_user, password, status FROM users WHERE login=? OR email=?', [$login, $login]);
        if (!empty($check[0][0])) {
            if (password_verify($password, $check[0]['password'])) {
                if ($check[0]['status'] == '1') {
                    $this->logged = true;
                    $this->id = $check[0]['id_user'];
                    $_SESSION['user_logged'] = true;
                    $_SESSION['user_id'] = $check[0]['id_user'];
                    $this->database->deleteOrUpdateWhere("UPDATE users SET last_date=timezone('UTC'::text, now()::timestamp(0) without time zone), last_ip=? WHERE id_user=?", [$_SERVER['REMOTE_ADDR'], $this->id]);
                } else {
                    throw new Exception('Twoje konto nie zostało jeszcze aktywowane!');
                }
            } else {
                throw new Exception('Błędne dane!');
            }
        } else {
            throw new Exception('Błędne dane!');
        }
    }

    public function getData()
    {
        if (empty($this->data)) {
            $data = $this->database->selectWithWhere('SELECT id_user, login, email, name, status, user_group FROM users WHERE id_user=?', [$this->id]);
            if (!empty($data[0][0]) && $data[0]['status'] == '1') {
                $this->data = $data[0];
            } else {
                $this->logout();
            }
        }
        return $this->data;
    }

    public function changeEmail($hash)
    {
        $change = $this->database->selectWithWhere('SELECT id_user, change_email FROM users WHERE LEFT(change_email, 30)=?', [$hash]);
        if (!empty($change)) {
            $newEmail = explode('|', $change[0]['change_email']);
            $query = $this->database->deleteOrUpdateWhere('UPDATE users SET email=?, change_email=null WHERE id_user=?', [$newEmail[1], $change[0]['id_user']], true);
            if ($query == 1) {
                return true;
            }
        }
        return false;
    }

    public function logout()
    {
        $this->id = null;
        $this->logged = false;
        $_SESSION['user_id'] = null;
        $_SESSION['user_logged'] = false;
    }

    function passwordValidation($old_password, $new_password, $repeatnew_password)
    {
        $results = $this->database->selectWithWhere('SELECT password FROM users WHERE id_user=?', array($this->id));
        $results = $results[0];
        if ($new_password==$repeatnew_password && password_verify($old_password, $results['password'])==true) {
            return true;
        } elseif ($new_password!=$repeatnew_password) {
            throw new Exception("Podane hasła nie są identyczne.");
        } elseif (!empty($old_password) && password_verify($old_password, $results['password']) == false) {
            throw new Exception("Podane hasło nie zgadza się ze starym hasłem.");
        }
    }

	public function getAvatarPath($user_Id)
    {
        $avatar_name = $this->database->selectWithWhere('SELECT uf.name
            FROM uploaded_files as uf
            JOIN users_avatars as ua ON uf.id_file=ua.id_file
            JOIN users as u ON u.id_user=ua.id_user
            WHERE u.id_user=?', [$user_Id]);

        if (isset($avatar_name[0]['name'])) {
            return $avatar_name[0]['name'];
        } else {
            return null;
        }
	}

    public function insertNotification($type, $id, $id_user)
    {
        $notification_hash = substr(md5(date('l jS F Y h:i:s A') . $this->id . $type),0,4);

        $this->database->insert('notifications', [
            'id_receiver' => $id_user,
            'type' => $type,
            'hash' => $notification_hash
        ]);

        $id_notification = $this->database->selectWithWhere('SELECT id_notification
            FROM notifications
            WHERE hash=?', [$notification_hash]);

        if ($type!=6) {
            $this->database->insert('contests_notifications', [
                'id_notification' => $id_notification[0]['id_notification'],
                'id_contest' => $id
            ]);
        } else {
            $this->database->insert('groups_notifications', [
                'id_notification' => $id_notification[0]['id_notification'],
                'id_group' => $id
            ]);
        }

        $this->database->deleteOrUpdateWhere('UPDATE notifications
            SET hash=null
            WHERE id_notification=?', [$id_notification[0]['id_notification']]);
    }

    public function getNotifications()
    {
        $count_cg_notifications = $this->database->selectWithWhere('SELECT COUNT(id_notification) as count
            FROM notifications
            WHERE id_receiver=? AND status=1', [$this->id]);
        $count_pm = $this->database->selectWithWhere('SELECT COUNT(id_message) as count
            FROM private_messages
            WHERE recipient_id=? AND status=1', [$this->id]);

        return ['contests_groups'=>$count_cg_notifications[0]['count'], 'pm'=>$count_pm[0]['count']];
    }

    public function readNotifications()
    {
        $this->database->deleteOrUpdateWhere('UPDATE notifications
            SET status=0
            WHERE id_receiver=?', [$this->id]);
    }

    public function getNotificationsList($start, $page)
    {
        $records_per_page = 6;

        if ($page==1) {
            $start = $this->database->selectWithWhere('SELECT id_notification
                FROM notifications
                WHERE id_receiver=?
                ORDER BY id_notification DESC
                LIMIT 1', [$this->id]);
            if (!isset($start[0]['id_notification'])) {
                $start = 0;
            } else {
                $start = $start[0]['id_notification'];
            }
        }

        $notifications = $this->database->selectWithWhere('SELECT id_notification, type, status, date::timestamp
            FROM notifications
            WHERE id_receiver=? AND id_notification<=?
            ORDER BY id_notification DESC
            LIMIT ?', [$this->id, $start, $records_per_page+1]);

        $i = 0;
        $lenght = count($notifications);

        foreach ($notifications as $key => $list) {
            if ($i == $lenght-1) {
                $start = $list['id_notification'];
                if ($lenght>$records_per_page) {
                    unset($notifications[$key]);
                }
            } else {
                $i++;
            }
        }

        foreach ($notifications as &$notification) {
            if ($notification['type']==6) {
                $group_data = $this->database->selectWithWhere('SELECT g.name, g.id_group, g.unique_name
                    FROM users_groups as u_g
                    JOIN groups as g ON g.id_group=u_g.id_group
                    JOIN groups_notifications as gn ON gn.id_group=g.id_group
                    WHERE gn.id_notification=?', [$notification['id_notification']]);

                $notification['group_data'] = $group_data[0];
            } else {
                $contest_info = $this->database->selectWithWhere('SELECT u.login as creator_login, c.link, c.contest_type, c.title, c.id_contest
                    FROM contests as c
                    JOIN users as u ON u.id_user=c.creator_id
                    JOIN contests_notifications as cn ON cn.id_contest=c.id_contest
                    WHERE cn.id_notification=?', [$notification['id_notification']]);

                $notification['contest_data'] = $contest_info[0];

                $img_name = $this->database->selectWithWhere('SELECT uf.name
                    FROM uploaded_files as uf
                    JOIN contests_images as ci ON ci.id_file=uf.id_file
                    JOIN contests as c ON c.id_contest=ci.id_contest
                    WHERE c.id_contest=? AND ci.type=0
                    LIMIT 1', [$notification['contest_data']['id_contest']]);

                if (!isset($img_name[0]['name'])) {
                    $img_name[0]['name'] = null;
                }
                $notification['contest_data']['img_name'] = $img_name[0]['name'];

                if ($notification['type']==0) {
                    $data = $this->database->selectWithWhere('SELECT contest_type
                        FROM contests
                        WHERE id_contest=?', [$notification['contest_data']['id_contest']]);

                    $notification['contest_data']['contest_type'] = $data[0]['contest_type'];
                } elseif ($notification['type']==1) {
                    $data = $this->database->selectWithWhere('SELECT prize_delivery
                        FROM contests
                        WHERE id_contest=?', [$notification['contest_data']['id_contest']]);

                    $notification['contest_data']['prize_delivery'] = $data[0]['prize_delivery'];
                } elseif ($notification['type']==3) {
                    $data = $this->database->selectWithWhere('SELECT status
                        FROM draw
                        WHERE id_contest=?', [$notification['contest_data']['id_contest']]);

                    $notification['contest_data']['draw_status'] = $data[0]['status'];
                }
            }
        }

        if ($lenght<=$records_per_page) {
            $stop = true;
        } else {
            $stop = false;
        }

        return ['notifications_list'=>$notifications, 'start'=>$start, 'stop'=>$stop];
    }

    public function getTime()
    {
        $user_time = $this->database->selectWithWhere("SELECT date_trunc('second', now()::timestamp) as current_time");

        return $user_time[0]['current_time'];
    }
}
