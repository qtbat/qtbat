<?php
namespace Qtbat\Model;

use Qtbat\Exception\PageError;
use Exception;

class ContestFunctionality
{
    function __construct($database, $user)
    {
        $this->database = $database;
        $this->user = $user;
    }

    public function tookPart($id_contest)
    {
        $took_part = $this->database->selectWithWhere('SELECT id_user, position, status
            FROM contests_participants
            WHERE id_contest=? AND id_user=?', array($id_contest, $this->user->getId()));

        if (!isset($took_part[0]))
            $took_part[0]=null;

        return $took_part[0];
    }

    public function firstFewSelectWinner($winner, $id_contest, $number_of_winners, $prizes_left, $prize, $creator_id)
    {
        $loggeduser_Id = $this->user->getId();

        if ($winner==true) {
            $this->database->insert('contests_participants', [
                'id_user' => $loggeduser_Id,
                'id_contest' => $id_contest,
                'position' => $prize,
                'status' => 1
            ]);

            //notifications
            $this->user->insertNotification(4, $id_contest, $creator_id);

            $count_prizes_left = count($prizes_left);

            if ($count_prizes_left==1) {
                $this->database->deleteOrUpdateWhere("UPDATE contests
                    SET status=2, end_date=date_trunc('second', now())
                    WHERE id_contest=?", [$id_contest]);

                //notifications
                $this->user->insertNotification(2, $id_contest, $creator_id);
            }

        } elseif ($winner==false) {
            $this->database->insert('contests_participants', [
                'id_user' => $loggeduser_Id,
                'id_contest' => $id_contest,
                'position' => null,
                'status' => 0
            ]);
        }
    }

    public function selectWinnerTakePart($value, $id_contest, $details)
    {
        $this->database->insert('contests_participants', [
            'id_user' => $this->user->getId(),
            'id_contest' => $id_contest,
            'content' => json_encode($value),
            'position' => NULL,
            'status' => 0
        ]);

        $last_order_number = $this->database->selectWithWhere('SELECT swa.order_number
            FROM select_winner_applications as swa
            JOIN contests_participants as cp ON cp.id_application=swa.id_application
            WHERE cp.id_contest=?
            ORDER BY swa.order_number DESC
            LIMIT 1', [$id_contest]);

        if (!isset($last_order_number[0]['order_number'])) {
            $last_order_number[0]['order_number'] = 0;
        }

        $id_application = $this->database->selectWithWhere('SELECT id_application
            FROM contests_participants
            WHERE id_user=? AND id_contest=?', [$this->user->getId(), $id_contest]);

        $this->database->insert('select_winner_applications', [
            'id_application' => $id_application[0]['id_application'],
            'order_number' => $last_order_number[0]['order_number']+1
        ]);

        if (array_key_exists('picture', $value)) {
            $this->database->insert('uploaded_files', [
                'original_name' => $details['original_name'],
                'name' => $details['name'],
                'disk' => $details['disk'],
                'folder' => $details['folder']
            ]);

            $id_file = $this->database->selectWithWhere('SELECT id_file FROM uploaded_files WHERE name=?', [$details['name']]);

            $this->database->insert('applications_files', [
                'id_application' => $id_application[0]['id_application'],
                'id_file' => $id_file[0]['id_file']
            ]);

            return $id_file[0]['id_file'];
        } elseif (array_key_exists('files', $value)) {
            $id_files = [];
            foreach ($details as $file) {
                $this->database->insert('uploaded_files', [
                    'original_name' => $file['original_name'],
                    'name' => $file['name'],
                    'disk' => $file['disk'],
                    'folder' => $file['folder']
                ]);

                $id_file = $this->database->selectWithWhere('SELECT id_file FROM uploaded_files WHERE name=?', [$file['name']]);

                $this->database->insert('applications_files', [
                    'id_application' => $id_application[0]['id_application'],
                    'id_file' => $id_file[0]['id_file']
                ]);

                $id_files[] = $id_file[0]['id_file'];
            }
            return $id_files;
        }
    }

    public function selectWinnerApplicationHighlight($id_contest, $number_of_winners)
    {
        try {
            $highlight = $_POST['highlight'];
            $order_number = $_POST['order_number'];

            if (filter_input(INPUT_POST, 'highlight', FILTER_VALIDATE_INT)!==false) {
                $prizes_to_take = [];

                for ($i=1; $i<=$number_of_winners; $i++) {
                    $prizes_to_take[] = $i;
                }
                if (!in_array($highlight, $prizes_to_take)) {
                    throw new \Exception;
                }
            } else {
                if ($highlight!='p' && $highlight!='n' && $highlight!='a' && $highlight!='s' && $highlight!='null') {
                    throw new \Exception();
                }
            }

            if ($highlight=='null') {
                $highlight = NULL;
            }
            $does_order_number_exist = $this->database->selectWithWhere('SELECT swa.order_number
                FROM select_winner_applications as swa
                JOIN contests_participants as cp ON swa.id_application=cp.id_application
                WHERE cp.id_contest=? AND swa.order_number=?', [$id_contest, $order_number]);

            if (!empty($does_order_number_exist)) {
                if (filter_input(INPUT_POST, 'order_number', FILTER_VALIDATE_INT)!==false) {
                    if ($highlight=='s' || $highlight=='1' || $highlight=='2' || $highlight=='3' || $highlight=='4') {
                        $this->database->deleteOrUpdateWhere("UPDATE select_winner_applications as swa
                        SET highlight=NULL
                        FROM contests_participants as cp
                        WHERE cp.id_application=swa.id_application AND cp.id_contest=? AND swa.highlight=?", [$id_contest, $highlight]);
                    }
                    $this->database->deleteOrUpdateWhere('UPDATE select_winner_applications as swa
                        SET highlight=?
                        FROM contests_participants as cp
                        WHERE cp.id_application=swa.id_application AND cp.id_contest=? AND swa.order_number=?', [$highlight, $id_contest, $order_number]);
                        $status = 'success';
                        $message = null;
                } else {
                    throw new \Exception();
                }
            } else {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            $status = 'error';
        }

        echo json_encode(['status'=>$status]);
    }

    public function selectWinnerConfirmWinners($id_contest, $limit_submission_date, $submission_date, $creator_id, $contest_status)
    {
        try {
            if (!isset($_POST['show_winners'])) {
                throw new \Exception('Uzupełnij wszystkie informacje');
            } else {
                if ($_POST['show_winners']!="yes" && $_POST['show_winners']!="no") {
                    throw new \Exception('Wystąpił błąd');
                } else {
                    if ($contest_status==1) {
                        if ($limit_submission_date == 1 && strtotime($submission_date) > strtotime($this->user->getTime())) {
                            throw new \Exception('Czas na zgłaszanie się do tego konkursu nie minął.');
                        } else {
                            $this->database->deleteOrUpdateWhere("UPDATE contests_participants as cp
                                SET position=swa.highlight, status=1
                                FROM select_winner_applications as swa
                                WHERE swa.id_application=cp.id_application AND (swa.highlight='a' OR swa.highlight='1' OR swa.highlight='2' OR swa.highlight='3' OR swa.highlight='4') AND cp.id_contest=?", [$id_contest]);

                            $this->database->deleteOrUpdateWhere("UPDATE contests
                                SET status=2, end_date=date_trunc('second', now())
                                WHERE id_contest=?", [$id_contest]);

                            if ($_POST['show_winners']=='yes') {
                                $show_winners = 1;
                            } elseif ($_POST['show_winners']=='no') {
                                $show_winners = 0;
                            }

                            $this->database->deleteOrUpdateWhere('UPDATE select_winner
                                SET show_winners=?
                                WHERE id_contest=?', [$show_winners, $id_contest]);

                            $this->database->deleteOrUpdateWhere("DELETE FROM select_winner_applications as swa
                                USING contests_participants as cp
                                WHERE cp.id_application=swa.id_application AND cp.id_contest=?", [$id_contest]);

                            $this->database->deleteOrUpdateWhere("UPDATE contests_participants
                                SET content=NULL
                                WHERE id_contest=? AND position!='a' AND position!='1' AND position!='2' AND position!='3' AND position!='4'", [$id_contest]);

                            $this->database->deleteOrUpdateWhere("DELETE FROM files_to_unlink as ftu
                                USING applications_files as af
                                JOIN contests_participants as cp ON cp.id_application=af.id_application
                                WHERE af.id_file=ftu.id_file AND cp.id_contest=? AND (cp.position!='1' OR cp.position!='2' OR cp.position!='3' OR cp.position!='4' OR cp.position!='a')", [$id_contest]);

                            $this->database->deleteOrUpdateWhere('UPDATE files_to_unlink as ftu
                                SET unlink=true
                                FROM applications_files as af
                                JOIN contests_participants as cp ON cp.id_application=af.id_application
                                WHERE af.id_file=ftu.id_file AND cp.id_contest=?', [$id_contest]);

                            $all_participants = $this->database->selectWithWhere('SELECT id_user, status
                                FROM contests_participants
                                WHERE id_contest=?', [$id_contest]);

                            foreach ($all_participants as $participant) {
                                $this->database->insert('contests_notifications', [
                                    'id_receiver' => $participant['id_user'],
                                    'id_contest' => $id_contest,
                                    'type' => $participant['status']
                                ]);
                            }

                            $status = 'success';
                            $message = null;
                        }
                    } else {
                        throw new \Exception('Wystąpił błąd');
                    }
                }
            }
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        echo json_encode(['status'=>$status, 'message'=>$message]);
    }

    public function drawTakePart($value, $id_contest, $details, $draw_info)
    {
        $this->database->insert('contests_participants', [
            'id_user' => $this->user->getId(),
            'id_contest' => $id_contest,
            'content' => json_encode($value),
            'position' => NULL,
            'status' => 0
        ]);

        if ($value!=null && (array_key_exists('picture', $value) || array_key_exists('file', $value))) {
            $this->database->insert('uploaded_files', [
                'original_name' => $details['original_name'],
                'name' => $details['name'],
                'disk' => $details['disk'],
                'folder' => $details['folder']
            ]);

            $id_file = $this->database->selectWithWhere('SELECT id_file FROM uploaded_files WHERE name=?', [$details['name']]);

            $id_application = $this->database->selectWithWhere('SELECT id_application
                FROM contests_participants
                WHERE id_user=? AND id_contest=?', [$this->user->getId(), $id_contest]);

            $this->database->insert('applications_files', [
                'id_application' => $id_application[0]['id_application'],
                'id_file' => $id_file[0]['id_file']
            ]);

            return $id_file[0]['id_file'];
        }

        if ($draw_info!=null) {
            $count_applications = $this->database->selectWithWhere('SELECT COUNT(id_application) as count
                FROM contests_participants
                WHERE id_contest=?', [$id_contest]);

            if ($count_applications[0]['count']>=$draw_info['number_of_applications']) {
                if ($draw_info['requirement']==null) {
                    $status_value = 2;
                } else {
                    $status_value = 1;
                }
                $this->startDraw($id_contest, $draw_info['number_of_winners'], $status_value);
            }
        }
    }

    public function startDraw($id_contest, $number_of_winners, $status_value, $creator_id) //status_value w przypadku, gdy requirement=null
    {
        $applications = $this->database->selectWithWhere('SELECT id_application
            FROM contests_participants
            WHERE id_contest=?
            ORDER BY random()
            LIMIT ?', [$id_contest, $number_of_winners+$number_of_winners]);

        if (!empty($applications)) {
            $draw = array_rand($applications, $number_of_winners);
            if ($number_of_winners>1) {
                foreach ($draw as $key => $val) {
                    $this->database->deleteOrUpdateWhere("UPDATE contests_participants
                        SET position='w'
                        WHERE id_application=?", [$applications[$key]['id_application']]);
                }
            } else {
                $this->database->deleteOrUpdateWhere("UPDATE contests_participants
                    SET position='w'
                    WHERE id_application=?", [$applications[$draw]['id_application']]);
            }

            if ($status_value==2) {
                $this->database->deleteOrUpdateWhere("UPDATE draw
                    SET status=?
                    WHERE id_contest=?", [2, $id_contest]);
                $this->confirmDraw($id_contest);

            } elseif ($status_value==1) {
                $this->database->deleteOrUpdateWhere("UPDATE draw
                    SET status=?, date=date_trunc('days', now() + interval '2 days')
                    WHERE id_contest=?", [1, $id_contest]);
            }
        } else {
            $this->database->deleteOrUpdateWhere("UPDATE draw
                SET status=2
                WHERE id_contest=?", [$id_contest]);
            $this->confirmDraw($id_contest);
        }

        //notifications
        $this->user->insertNotification(3, $id_contest, $creator_id);
    }

    public function repeatDraw($id_contest, $id_application)
    {
        try {
            $is_application_valid = $this->database->selectWithWhere("SELECT id_application
                FROM contests_participants
                WHERE id_application=? AND position='w'", [$id_application]);

            if (!empty($is_application_valid)) {
                $applications = $this->database->selectWithWhere("SELECT id_application
                    FROM contests_participants
                    WHERE id_contest=? AND (position!='w' OR position IS NULL)", [$id_contest]);

                $draw = array_rand($applications, 1);
                $this->database->deleteOrUpdateWhere("UPDATE contests_participants
                    SET position='w'
                    WHERE id_application=?", [$applications[$draw]['id_application']]);

                $this->database->deleteOrUpdateWhere("UPDATE contests_participants
                    SET position=NULL
                    WHERE id_application=?", [$id_application]);
            } else {
                throw new \Exception('Wystąpił błąd');
            }
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        echo json_encode(['status'=>$status, 'message'=>$message]);
    }

    public function confirmDraw($id_contest)
    {
        $winners = $this->database->selectWithWhere("SELECT id_application
            FROM contests_participants
            WHERE position='w' AND id_contest=?
            ORDER BY random()", [$id_contest]);

        foreach ($winners as $key => $val) {
            echo $key+1 . '<br />';
            $this->database->deleteOrUpdateWhere("UPDATE contests_participants
                SET position=?, status=1
                WHERE id_contest=? AND position='w' AND id_application=?", [$key+1, $id_contest, $val['id_application']]);
        }

        $this->database->deleteOrUpdateWhere("UPDATE contests
            SET status=2, end_date=date_trunc('second', now())
            WHERE id_contest=?", [$id_contest]);

        $this->database->deleteOrUpdateWhere("UPDATE contests_participants
            SET content=NULL
            WHERE id_contest=? AND position!='a'", [$id_contest]);

        $this->database->deleteOrUpdateWhere('UPDATE draw
            SET status=2, date=null
            WHERE id_contest=?', [$id_contest]);

        $this->database->deleteOrUpdateWhere("DELETE FROM files_to_unlink as ftu
            USING applications_files as af
            JOIN contests_participants as cp ON cp.id_application=af.id_application
            WHERE af.id_file=ftu.id_file AND cp.id_contest=? AND cp.status!=1", [$id_contest]);

        $this->database->deleteOrUpdateWhere('UPDATE files_to_unlink as ftu
            SET unlink=true
            FROM applications_files as af
            JOIN contests_participants as cp ON cp.id_application=af.id_application
            WHERE af.id_file=ftu.id_file AND cp.id_contest=?', [$id_contest]);

        //notifications
        $all_participants = $this->database->selectWithWhere('SELECT id_user, status
            FROM contests_participants
            WHERE id_contest=?', [$id_contest]);

        foreach ($all_participants as $participant) {
            $this->user->insertNotification($participant['status'], $id_contest, $participant['id_user']);
        }
    }

    public function leaveOpinion($id_contest, $creator_id)
    {
        try {
            $is_allowed = $this->database->selectWithWhere('SELECT id_user
                FROM contests_participants
                WHERE id_user=? AND id_contest=? AND status>0', array($this->user->getId(), $id_contest));
            $left_opinion = $this->database->selectWithWhere('SELECT id_opinion
                FROM users_opinions
                WHERE id_contest=? AND from_user=?', array($id_contest, $this->user->getId()));

            if (!empty($is_allowed)) {
                if (empty($left_opinion)) {
                    if (isset($_POST['rate'])) {
                        $validate = true;

                        if ($_POST['rate']!='0' && $_POST['rate']!='1') {
                            $validate = false;
                        }

                        if (!isset($_POST['comment'])) {
                            $validate = false;
                        } else {
                            if (empty($_POST['comment'])) {
                                $_POST['comment'] = NULL;
                            }
                        }

                        if ($validate==true) {
                            validateInput($_POST['comment'], 'komentarz', 'opinion_comment');

                            $message = null;
                            $status = 'success';

                            $this->database->insert('users_opinions', [
                                'from_user' => $this->user->getId(),
                                'to_user' => $creator_id,
                                'rate' => $_POST['rate'],
                                'comment' => clearString($_POST['comment']),
                                'id_contest' => $id_contest
                            ]);
                        } else {
                            throw new \Exception('Wystąpił błąd');
                        }
                    } else {
                        throw new \Exception('Nie podałeś oceny');
                    }
                } else {
                    throw new \Exception('Wystawiłeś już wcześniej opinię dotyczącą tego konkursu.');
                }
            } else {
                throw new \Exception('Nie jestes upoważniony aby wystawiać opinię o autorze tego konkursu.');
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $status = "error";
        }

        echo json_encode(['status'=>$status, 'message'=>$message]);
    }

    public function prepareFileToUnlink($id_file)
    {
        $this->database->insert('files_to_unlink', [
            'id_file' => $id_file
        ]);
    }
}
