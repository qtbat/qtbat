<?php
namespace Qtbat\Model;

use Qtbat\Exception\PageError;
use Exception;

class Profile
{
    function __construct($database, $user)
    {
        $this->database = $database;
        $this->user = $user;
    }

    public function get($login)
    {
        $profile_info = $this->database->selectWithWhere("SELECT id_user, login, name, location, register_date::timestamp::date FROM users WHERE lower(login)=lower(?) LIMIT 1", [$login]);
        return $profile_info;
    }

    public function settings($loggeduser_Id)
    {
        $results = $this->database->selectWithWhere('SELECT login, name, location, email, password, contact_email, sex FROM users WHERE id_user=?', array($loggeduser_Id));
        return $results[0];
    }

    public function edit()
    {
        $loggeduser_Id = $this->user->getId();

        if (isset($_POST['sex'])) {

            try {
                if ($_POST['sex']!='f' && $_POST['sex']!='m' && $_POST['sex']!='0') {
                    throw new Exception;
                } else {
                    $status = 'success';
                    $this->database->deleteOrUpdateWhere("UPDATE users SET sex=? WHERE id_user=?", array($_POST['sex'], $loggeduser_Id));
                }
            } catch (\Exception $e) {
                $status = 'error';
            }
            echo $status;

		} elseif (isset($_POST['user_location'])) {

			try {
				$message = null;
				$status = 'success';
				validateInput($_POST['user_location'], 'lokacja', 'location');
				$this->database->deleteOrUpdateWhere("UPDATE users SET location=? WHERE id_user=?", array(clearString($_POST['user_location']), $loggeduser_Id));
			} catch (\Exception $e) {
				$message = $e->getMessage();
				$status = 'error';
			}
			$json = array('message'=>$message, 'status'=>$status);
			echo json_encode($json);

		} elseif (isset($_POST['name'])) {

			try {
				$message = null;
				$status = 'success';
				validateInput($_POST['name'], 'imię i nazwisko', 'name');
				$this->database->deleteOrUpdateWhere("UPDATE users SET name=? WHERE id_user=?", array(clearString($_POST['name']), $loggeduser_Id));
			} catch (\Exception $e) {
				$message = $e->getMessage();
				$status = 'error';
			}
			$json = array('message'=>$message, 'status'=>$status);
			echo json_encode($json);

		} elseif (isset($_POST['old_password'])) {

			try {
				$message = null;
				$status = "success";
				$old_password = $_POST['old_password'];
				$new_password = $_POST['new_password'];
				$repeat_new_password = $_POST['repeat_new_password'];
				validateInput($old_password, 'stare hasło', 'password');
				validateInput($new_password, 'nowe hasło', 'password');
				$password_validation = $this->user->passwordValidation($old_password, $new_password, $repeat_new_password);
				if ($password_validation==true) {
					$this->database->deleteOrUpdateWhere("UPDATE users SET password=COALESCE(NULLif (?, ''),password) WHERE id_user=?", array(password_hash($new_password, PASSWORD_BCRYPT), $loggeduser_Id));
				}
			} catch (\Exception $e) {
				$message = $e->getMessage();
				$status = "error";
			}
			$json = array('message'=>$message, 'status'=>$status);
			echo json_encode($json);

		} elseif (isset($_POST['user_email'])) {

            try {
                $newEmail = filter_input(INPUT_POST, 'user_email', FILTER_VALIDATE_EMAIL);
                if (!empty($newEmail)) {
                    $hash = hashFromTimeAdd(30);
                    $new = $hash.'|'.$newEmail;
                    $this->database->deleteOrUpdateWhere("UPDATE users SET change_email=? WHERE id_user=?", [$new, $this->user->getId()]);
                    $mail = new \Qtbat\Engine\Mail();
                    $mail->set($this->user->getData()['email'], $this->user->getData()['login'], 'Zmiana adresu email w '.APP_NAME, 'change_email.html.twig', [
                        'hash' => $hash,
                        'login' => $this->user->getData()['login'],
                        'old_email' => $this->user->getData()['email'],
                        'new_email' => $newEmail
                    ]);
                    $mail->send();
                    $message = 'Prawie gotowe! Potwierdź zmianę klikając w link w wiadomości wysłanej na obecny adres e-mail.';
    				$status = 'success';
                } else {
                    $message = 'Podano błędny adres e-mail!';
                    $status = "error";
                }
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $status = "error";
            }
            $json = array('message'=>$message, 'status'=>$status);
            echo json_encode($json);

        } elseif (isset($_POST['contact_email'])) {

            $email = filter_input(INPUT_POST, 'contact_email', FILTER_VALIDATE_EMAIL);
            if (!empty($email)) {
                $this->database->deleteOrUpdateWhere("UPDATE users SET contact_email=? WHERE id_user=?", [$email, $this->user->getId()]);
                $message = null;
                $status = 'success';
            } else {
                $message = 'Podano błędny kontaktowy adres e-mail!';
                $status = "error";
            }
            $json = array('message'=>$message, 'status'=>$status);
            echo json_encode($json);

		} else
			throw new PageError(404);
    }

    public function usersOpinions($id_user)
    {
        $data = $this->database->selectWithWhere('SELECT c.contest_type
            FROM users_opinions as o
            JOIN contests as c ON c.id_contest=o.id_contest
            WHERE o.to_user=?', array($id_user));
        $how_many = count($data)-1;

        $opinions = [];

        for($i=0; $i<=$how_many; $i++) {
            $result = $this->database->selectWithWhere('SELECT c.title, u.login, o.rate, o.comment, c.link, o.date::timestamp
                FROM contests as c
                JOIN users_opinions as o ON c.id_contest=o.id_contest
                JOIN users as u ON u.id_user=o.from_user
                WHERE o.to_user=?', array($id_user));

            $opinions[$i] = $result[$i];
        }

        $count_positive = $this->database->selectWithWhere('SELECT COUNT(id_opinion) as count FROM users_opinions WHERE rate=1 AND to_user=?', array($id_user));
        $count_negative = $this->database->selectWithWhere('SELECT COUNT(id_opinion) as count FROM users_opinions WHERE rate=0 AND to_user=?', array($id_user));

        return array('opinions'=>$opinions, 'positive'=>$count_positive[0], 'negative'=>$count_negative[0]);
    }
}
