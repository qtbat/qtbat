<?php
namespace Qtbat\Model;

use Qtbat\Exception\PageError;
use Exception;

class ContestModification
{
    function __construct($database, $user)
    {
        $this->database = $database;
        $this->user = $user;
    }

    public function editContest($id_contest, $creator_id, $contest_status)
    {
        if (isset($_POST['contest_description'])) {
            try {
                if ($creator_id==$this->user->getId()) {
                    if ($contest_status<2) {
                        $this->database->deleteOrUpdateWhere('UPDATE contests SET description=? WHERE id_contest=?', array($_POST['contest_description'], $id_contest));

                        $message = null;
                        $status = 'success';

                    } else {
                        throw new \Exception('Nie możesz edytować informacji o tym konkursie, ponieważ już się zakończył.');
                    }
                } else {
                    throw new \Exception('Nie możesz modyfikować tego konkursu, ponieważ nie jesteś jego autorem.');
                }
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $status = "error";
            }
            $json = array('message'=>$message, 'status'=>$status);
            echo json_encode($json);

        } elseif (isset($_POST['contest_title'])) {
            try {
                if ($creator_id==$this->user->getId()) {
                    validateInput($_POST['contest_title'], 'tytuł', 'title');
                    if ($contest_status<2) {
                        $this->database->deleteOrUpdateWhere('UPDATE contests SET title=? WHERE id_contest=?', array(clearString($_POST['contest_title']), $id_contest));

                        $message = null;
                        $status = 'success';

                    } else {
                        throw new \Exception('Nie możesz edytować informacji o tym konkursie, ponieważ już się zakończył.');
                    }
                } else {
                    throw new \Exception('Nie możesz modyfikować tego konkursu, ponieważ nie jesteś jego autorem.');
                }
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $status = "error";
            }
            $json = array('message'=>$message, 'status'=>$status);
            echo json_encode($json);

        } elseif (isset($_POST['avaible_for'])) {
            try {
                if ($creator_id==$this->user->getId()) {
                    if ($contest_status<2) {
                        if ($_POST['avaible_for']=='groups') {
                            if (isset($_POST['select_groups'])) {
                                $groups = json_decode($_POST['select_groups']);
                                $index = count($groups)-1;

                                $message = null;
                                $status = 'success';

                                for ($i=0; $i<=$index; $i++) {
                                    $is_in_group = $this->database->selectWithWhere('SELECT id_user FROM users_groups WHERE id_group=? AND id_user=? AND member_status=1', array($groups[$i], $this->user->getId()));

                                    if (empty($is_in_group)) {
                                        $status = 'error';
                                    }
                                }
                                if ($status=='success') {
                                    $this->database->deleteOrUpdateWhere('DELETE FROM contests_groups WHERE id_contest=?', array($id_contest));

                                    $this->database->deleteOrUpdateWhere('UPDATE contests SET avaible_for=1 WHERE id_contest=?', array($id_contest));

                                    for ($i=0; $i<=$index; $i++) {
                                        $this->database->insert('contests_groups', [
                                            'id_group' => $groups[$i],
                                            'id_contest' => $id_contest
                                        ]);
                                    }
                                } else {
                                    throw new \Exception('Nie należysz do grupy, dla której chcesz udostępnić ten konkurs!');
                                }
                            } else {
                                throw new \Exception('Nie wybrałeś grup, dla których chcesz udostępnić ten konkurs.');
                            }
                        } elseif ($_POST['avaible_for']=='normal') {
                            $this->database->deleteOrUpdateWhere('DELETE FROM contests_groups WHERE id_contest=?', array($id_contest));
                            $message = null;
                            $status = 'success';

                            $this->database->deleteOrUpdateWhere('UPDATE contests SET avaible_for=2 WHERE id_contest=?', array($id_contest));
                        } else {
                            throw new \Exception('Wystąpił błąd z poprawnym wczytaniem twojego formularza. Przeładuj stronę i spróbuj ponownie.');
                        }
                    } else {
                        throw new \Exception('Nie możesz edytować informacji o tym konkursie, ponieważ już się zakończył.');
                    }
                } else {
                    throw new \Exception('Nie możesz modyfikować tego konkursu, ponieważ nie jesteś jego autorem.');
                }
            } catch (\Exception $e) {
                $status = 'error';
                $message = $e->getMessage();
            }
            echo json_encode(array('status'=>$status, 'message'=>$message));

        } elseif (isset($_POST['select_category'])) {
            try {
                if ($creator_id==$this->user->getId()) {
                    if ($contest_status==0) {
                        $create_contest = new \Qtbat\Model\CreateContest($this->database, $this->user);
                        $count = $create_contest->countCategories();
                        if (ctype_digit($_POST['select_category']) && (int)$_POST['select_category']<=$count && (int)$_POST['select_category']>0) {
                            $this->database->deleteOrUpdateWhere('UPDATE contests SET id_category=? WHERE id_contest=?', array($_POST['select_category'], $id_contest));
                            $status = 'success';
                            $message = null;
                        } else {
                            throw new \Exception('Wystąpił błąd');
                        }
                    } else {
                        throw new \Exception('Nie możesz zmienić tej informacji, ponieważ konkurs już się rozpoczął.');
                    }
                } else {
                    throw new \Exception('Nie możesz modyfikować tego konkursu, ponieważ nie jesteś jego autorem.');
                }
            } catch (\Exception $e) {
                $status = 'error';
                $message = $e->getMessage();
            }

            echo json_encode(array('status'=>$status, 'message'=>$message));
        } elseif (isset($_POST['delivery'])) {
            try {
                if ($contest_status==0) {
                    $values = [];
                    $validation = true;
                    $error_message = NULL;
                    if (isset($_POST['number_of_winners'])) {
                        if (filter_input(INPUT_POST, 'number_of_winners', FILTER_VALIDATE_INT)!==false) {
                            if ($_POST['number_of_winners']>5) {
                                $validation = false;
                                $error_message =  'Maksymalna ilość zwycięzców wynosi 5.';
                            } elseif ($_POST['number_of_winners']<1) {
                                $validation = false;
                                $error_message =  'Minimalna ilośc zwycięzców wynosi 1.';
                            } else {
                                $values['number_of_winners'] = $_POST['number_of_winners'];
                            }
                        } else {
                            $validation = false;
                            $error_message =  'Ilość zwycięzców musi być liczbą całkowitą!';
                        }

                        if ($validation == true) {
                            if ($_POST['delivery']=='by_author') {
                                $values['prize_delivery'] = 1;
                                $values['prize'] = NULL;
                            } elseif ($_POST['delivery']=='automatic') {
                                $values['prize_delivery'] = 2;
                                if (isset($_POST['prize'])) {

                                    if (is_array($_POST['prize'])) {
                                        $is_prize_not_null = true;

                                        for ($i=0; $i<=count($_POST['prize'])-1; $i++) {
                                            if (!isset($_POST['prize'][$i])) {
                                                throw new \Exception('Wystąpił błąd');
                                            } else {
                                                $_POST['prize'][$i] = clearString($_POST['prize'][$i]);
                                                if (clearString($_POST['prize'][$i]=="")) {
                                                    $is_prize_not_null = false;
                                                }
                                            }
                                        }
                                        if ($is_prize_not_null) {
                                            $count_prizes = count($_POST['prize']);

                                            if ($count_prizes<$values['number_of_winners']) {
                                                $validation = false;
                                                $error_message = 'Nie podałeś/aś wszystkich nagród.';
                                            } elseif ($count_prizes>$values['number_of_winners']) {
                                                $validation = false;
                                                $error_message = 'Wystąpił błąd.';
                                            } else {
                                                $values['prize'] = json_encode($_POST['prize']);
                                            }
                                        } else {
                                            $validation = false;
                                            $error_message = 'Nie podałeś/aś nagród!';
                                        }
                                    } else {
                                        throw new \Exception('Wystąpił błąd');
                                    }
                                } else {
                                    $validation = false;
                                    $error_message = 'Wystąpił błąd z poprawnym wczytaniem twojego formularza. Przeładuj stronę i spróbuj ponownie.';
                                }
                            }
                            if ($validation == true) {
                                $this->database->deleteOrUpdateWhere('UPDATE contests SET number_of_winners=?, prize_delivery=?, prize=? WHERE id_contest=?', array($values['number_of_winners'], $values['prize_delivery'], $values['prize'], $id_contest));
                                $status = 'success';
                                $message = null;
                            } else {
                                throw new \Exception($error_message);
                            }
                        } else {
                            throw new \Exception($error_message);
                        }
                    } else {
                        throw new \Exception('Wystąpił błąd');
                    }
                } else {
                    throw new \Exception('Nie możesz modifykować tej informacji, ponieważ ten konkurs już się rozpoczął');
                }
            } catch (\Exception $e) {
                $status = 'error';
                $message = $e->getMessage();
            }

            echo json_encode(array('status'=>$status, 'message'=>$message));
        } else
            throw new PageError(404);
    }

    public function editContestImages($id_contest, $images)
    {
        foreach ($images as $image) {
            if (isset($image['unlink']) && $image['unlink']==true) {
                $id_file = $this->database->selectWithWhere('SELECT id_file FROM uploaded_files WHERE name=?', [$image['name']]);
                $this->database->deleteOrUpdateWhere('DELETE FROM contests_images WHERE id_file=?', [$id_file[0]['id_file']]);
                $this->database->deleteOrUpdateWhere('DELETE FROM uploaded_files WHERE id_file=?', [$id_file[0]['id_file']]);
            } else {
                $this->database->insert('uploaded_files', [
                    'name' => $image['name'],
                    'disk' => $image['disk'],
                    'folder' => $image['folder']
                ]);
                $id_file = $this->database->selectWithWhere('SELECT id_file FROM uploaded_files WHERE name=?', [$image['name']]);
                $this->database->insert('contests_images', [
                    'id_contest' => $id_contest,
                    'id_file' => $id_file[0]['id_file'],
                    'type' => 0
                ]);
            }
        }
    }

    public function startContest($id_contest, $creator_id, $contest_status, $contest_type)
    {
        try {
            if ($contest_status==0 && $this->user->getId()==$creator_id) {
                if ($contest_type==3) {
                    $limit_submission_date = $this->database->selectWithWhere('SELECT limit_submission_date
                        FROM select_winner
                        WHERE id_contest=?', [$id_contest]);
                    if ($limit_submission_date[0]['limit_submission_date']==1) {
                        if (isset($_POST['date']) && $_POST['date']!="") {
                            if (preg_match('/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/', $_POST['date'])) {
                                $date = explode("/", $_POST['date']);
                                if (!checkdate($date[0], $date[1], $date[2])) {
                                    throw new \Exception('Nieprawidłowa data');
                                }
                            } else {
                                throw new \Exception('Niepoprawna forma daty');
                            }
                        } else {
                            throw new \Exception('Nie podałeś daty');
                        }

                        if (isset($_POST['hour'])) {
                            if (filter_input(INPUT_POST, 'hour', FILTER_VALIDATE_INT)!==false) {
                                if ($_POST['hour']<0 && $_POST['hour']>23) {
                                    throw new \Exception('Wystąpił błąd');
                                }
                            } else {
                                throw new \Exception('Godzina musi być przedstawiona jako liczba całkowita');
                            }
                        } else {
                            throw new \Exception('Nie podałeś godziny');
                        }

                        if (isset($_POST['minute'])) {
                            if (filter_input(INPUT_POST, 'minute', FILTER_VALIDATE_INT)!==false) {
                                if ($_POST['minute']<0 && $_POST['minute']>59) {
                                    throw new \Exception('Wystąpił błąd');
                                }
                            } else {
                                throw new \Exception('Minuta musi być przedstawiona jako liczba całkowita');
                            }
                        } else {
                            throw new \Exception('Nie podałeś minuty');
                        }

                        $now = $this->user->getTime();
                        $date_format = $date[2] . '-' . $date[0] . '-' . $date[1] . ' ' . $_POST['hour'] . ':' . $_POST['minute'] . ':00';

                        if (strtotime($date_format) < strtotime($now)) {
                            throw new \Exception('Data podana przez Ciebie jest mniejsza od teraźniejszej daty');
                        } elseif (strtotime($date_format) < (strtotime($now)+(60*5))) {
                            throw new \Exception('Minimalny odstęp teraźniejszej daty i podanej przez Ciebie daty musi wynosić 5 minut');
                        } elseif (strtotime($date_format) > (strtotime($now .' +6 Month'))) {
                            throw new \Exception('Data podana przez Ciebie wybiega zbyt daleko w przyszłość (maksymalny odstęp wynosi 6 miesięcy)');
                        } else {
                            $time_to_insert = $this->database->selectWithWhere("SELECT to_timestamp(?, 'YYYY-MM-DD HH24:MI:SS') AT TIME ZONE 'UTC' as time", [$date_format]);
                            $this->database->deleteOrUpdatewhere('UPDATE select_winner
                                SET submission_date=?
                                WHERE id_contest=?', [$time_to_insert[0]['time'].'+00', $id_contest]);
                        }
                    }
                } elseif ($contest_type==2) {
                    $method = $this->database->selectWithWhere('SELECT method
                        FROM draw
                        WHERE id_contest=?', [$id_contest]);
                    if ($method[0]['method']==1) {
                        if (isset($_POST['date']) && $_POST['date']!="") {
                            if (preg_match('/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/', $_POST['date'])) {
                                $date = explode("/", $_POST['date']);
                                if (!checkdate($date[0], $date[1], $date[2])) {
                                    throw new \Exception('Nieprawidłowa data');
                                }
                            } else {
                                throw new \Exception('Niepoprawna forma daty');
                            }
                        } else {
                            throw new \Exception('Nie podałeś daty');
                        }

                        if (isset($_POST['hour'])) {
                            if (filter_input(INPUT_POST, 'hour', FILTER_VALIDATE_INT)!==false) {
                                if ($_POST['hour']<0 && $_POST['hour']>23) {
                                    throw new \Exception('Wystąpił błąd');
                                }
                            } else {
                                throw new \Exception('Godzina musi być przedstawiona jako liczba całkowita');
                            }
                        } else {
                            throw new \Exception('Nie podałeś godziny');
                        }

                        if (isset($_POST['minute'])) {
                            if (filter_input(INPUT_POST, 'minute', FILTER_VALIDATE_INT)!==false) {
                                if ($_POST['minute']!=0 && $_POST['minute']!=30) {
                                    throw new \Exception('Wystąpił błąd');
                                }
                            } else {
                                throw new \Exception('Minuta musi być przedstawiona jako liczba całkowita');
                            }
                        } else {
                            throw new \Exception('Nie podałeś minuty');
                        }

                        $now = $this->user->getTime();
                        $date_format = $date[2] . '-' . $date[0] . '-' . $date[1] . ' ' . $_POST['hour'] . ':' . $_POST['minute'] . ':00';

                        if (strtotime($date_format) < strtotime($now)) {
                            throw new \Exception('Data podana przez Ciebie jest mniejsza od teraźniejszej daty');
                        } elseif (strtotime($date_format) < (strtotime($now)+(60*20))) {
                            throw new \Exception('Minimalny odstęp teraźniejszej daty i podanej przez Ciebie daty musi wynosić 20 minut');
                        } elseif (strtotime($date_format) > (strtotime($now .' +6 Month'))) {
                            throw new \Exception('Data podana przez Ciebie wybiega zbyt daleko w przyszłość (maksymalny odstęp wynosi 6 miesięcy)');
                        } else {
                            $time_to_insert = $this->database->selectWithWhere("SELECT to_timestamp(?, 'YYYY-MM-DD HH24:MI:SS') AT TIME ZONE 'UTC' as time", [$date_format]);
                            $this->database->deleteOrUpdatewhere('UPDATE draw
                                SET date=?
                                WHERE id_contest=?', [$time_to_insert[0]['time'].'+00', $id_contest]);
                        }
                    }
                }
                    $this->database->deleteOrUpdateWhere("UPDATE contests
                        SET status=1, start_date=date_trunc('second', now())
                        WHERE id_contest=?", array($id_contest));

                    $status = 'success';
                    $message = null;
            } else {
                throw new \Exception('Nie jesteś autorem teog konkursu lub ten konkurs już się rozpoczął');
            }
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        echo json_encode(['status'=>$status, 'message'=>$message]);
    }

    public function deleteContest($id_contest, $creator_id, $contest_status, $contest_type)
    {
        try {
            if ($contest_status==0 && $this->user->getId()==$creator_id) {
                $uploadfile = new \Qtbat\Engine\UploadFile($this->database);

                $images = $this->database->selectWithWhere('SELECT uf.name, uf.disk, uf.folder
                    FROM uploaded_files as uf
                    JOIN contests_images as ci ON uf.id_file=ci.id_file
                    JOIN contests as c ON c.id_contest=ci.id_contest
                    WHERE c.id_contest=?', [$id_contest]);

                $file_management = new \Qtbat\Engine\FileManagement();

                foreach ($images as $image) {
                    $path = $file_management->getPath($image['disk']);
                    $uploadfile->unlinkContestImage($path, $image['folder'], $image['name']);
                }

                $this->database->deleteOrUpdateWhere('DELETE FROM contests_images as ci
                    USING uploaded_files as uf
                    WHERE ci.id_file=uf.id_file AND ci.id_contest=?', [$id_contest]);

                if ($contest_type==1) {
                    $this->database->deleteOrUpdateWhere('DELETE FROM first_few WHERE id_contest=?', array($id_contest));
                } elseif ($contest_type==2) {
                    $this->database->deleteOrUpdateWhere('DELETE FROM draw WHERE id_contest=?', array($id_contest));
                } elseif ($contest_type==3) {
                    $this->database->deleteOrUpdateWhere('DELETE FROM select_winner WHERE id_contest=?', array($id_contest));
                } elseif ($contest_type==4) {
                    $this->database->deleteOrUpdateWhere('DELETE FROM tournament WHERE id_contest=?', array($id_contest));
                }
                $this->database->deleteOrUpdateWhere('DELETE FROM contests_groups WHERE id_contest=?', array($id_contest));
                $this->database->deleteOrUpdateWhere('DELETE FROM contests WHERE id_contest=?', array($id_contest));
                $status = 'success';
            } else {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            $status = 'error';
            echo $e->getMessage();
        }

        echo json_encode(['status'=>$status]);
    }

    public function deleteContestBanner($id_contest)
    {
        $id_file = $this->database->selectWithWhere('SELECT id_file
            FROM contests_images
            WHERE id_contest=? AND type=1', [$id_contest]);

        $this->database->deleteOrUpdateWhere('DELETE FROM contests_images
            WHERE id_file=?', [$id_file[0]['id_file']]);
        $this->database->deleteOrUpdateWhere('DELETE FROM uploaded_files
            WHERE id_file=?', [$id_file[0]['id_file']]);
    }
}
