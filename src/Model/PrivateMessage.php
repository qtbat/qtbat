<?php
namespace Qtbat\Model;

use Qtbat\Exception\PageError;
use Exception;

class PrivateMessage
{
    private $to;
    private $toData;

    public function __construct($database, $user)
    {
        $this->database = $database;
        $this->user = $user;
    }

    public function prepare($to)
    {
        $this->to = $to;
        if (!$this->user->isLogged() || $this->user->getData()['login'] == $to) {
            throw new PageError(403);
        }
        $to = new Profile($this->database, $this->user);
        $this->toData = $to->get($this->to)[0];
        if ($this->toData == false) {
            throw new PageError(404);
        }
    }

    public function send()
    {
        if (!empty($this->to)) {
            $topic = filter_input(INPUT_POST, 'topic', FILTER_SANITIZE_SPECIAL_CHARS);
            $content = filter_input(INPUT_POST, 'content');
            if (empty($topic) || empty($content)) {
                throw new Exception('Wypełnij wszystkie wymagane pola!');
            }
            $query = $this->database->insert('private_messages', [
                'sender_id' => $this->user->getId(),
                'recipient_id' => $this->toData['id_user'],
                'topic' => $topic,
                'content' => bbcodeToHtml($content)
            ]);
            return $query;
        } else {
            throw new Exception('Nie określono odbiorcy wiadomości!');
        }
    }

    public function getList($login = null)
    {
        if (!empty($login)) {
            $result = $this->database->selectWithWhere('SELECT p.id_message, p.topic, p.content, p.send_date::timestamp, u.login AS sender FROM private_messages AS p LEFT JOIN users AS u ON p.sender_id=u.id_user WHERE p.recipient_id=? AND u.login=? AND p.recipient_removed=0 ORDER BY send_date DESC', [$this->user->getId(), $login[1]]);
        } else {
            $result = $this->database->selectWithWhere('SELECT p.id_message, p.topic, p.content, p.send_date::timestamp, u.login AS sender FROM private_messages AS p LEFT JOIN users AS u ON p.sender_id=u.id_user WHERE p.recipient_id=? AND p.recipient_removed=0 ORDER BY send_date DESC', [$this->user->getId()]);

            if ($this->user->getNotifications()['pm'] != 0) {
                $this->database->deleteOrUpdateWhere('UPDATE private_messages SET status=0 WHERE recipient_id=?', [$this->user->getId()]);
            }
        }
        return $result;
    }

    public function getListSent($login = null)
    {
        if (!empty($login)) {
            $result = $this->database->selectWithWhere('SELECT p.id_message, p.topic, p.content, p.send_date::timestamp, u.login AS recipient FROM private_messages AS p LEFT JOIN users AS u ON p.recipient_id=u.id_user WHERE p.sender_id=? AND u.login=? AND p.sender_removed=0 ORDER BY send_date DESC', [$this->user->getId(), $login[1]]);
        } else {
            $result = $this->database->selectWithWhere('SELECT p.id_message, p.topic, p.content, p.send_date::timestamp, u.login AS recipient FROM private_messages AS p LEFT JOIN users AS u ON p.recipient_id=u.id_user WHERE p.sender_id=? AND sender_removed=0 ORDER BY send_date DESC', [$this->user->getId()]);
        }
        return $result;
    }

    public function get($data)
    {
        if ($data[1] > 0) {
            $message = $this->database->selectWithWhere('SELECT p.id_message, p.sender_id, p.sender_removed, p.recipient_id, p.recipient_removed, p.topic, p.content, p.send_date::timestamp, u.login AS sender, ur.login AS recipient FROM private_messages AS p LEFT JOIN users AS u ON p.sender_id=u.id_user LEFT JOIN users AS ur ON p.recipient_id=ur.id_user WHERE p.id_message=?', [$data[1]]);
            if (($message[0]['sender_id'] == $this->user->getId() || $message[0]['recipient_id'] == $this->user->getId())) {
                if ($message[0]['sender_id'] == $this->user->getId() && $message[0]['sender_removed'] == 0) {
                    return [
                        'topic' => $message[0]['topic'],
                        'content' => $message[0]['content'],
                        'send_date' => $message[0]['send_date'],
                        'sender' => ($message[0]['sender_id'] == $this->user->getId()) ? $message[0]['recipient'] : $message[0]['sender']
                    ];
                } elseif ($message[0]['recipient_id'] == $this->user->getId() && $message[0]['recipient_removed'] == 0) {
                    return [
                        'topic' => $message[0]['topic'],
                        'content' => $message[0]['content'],
                        'send_date' => $message[0]['send_date'],
                        'sender' => ($message[0]['sender_id'] == $this->user->getId()) ? $message[0]['recipient'] : $message[0]['sender']
                    ];
                }
            }
        }
        return false;
    }

    public function delete($messageId)
    {
        if ($messageId > 0) {
            $message = $this->database->selectWithWhere('SELECT p.id_message, p.sender_id, p.sender_removed, p.recipient_id, p.recipient_removed FROM private_messages AS p LEFT JOIN users AS u ON p.sender_id=u.id_user WHERE p.id_message=?', [$messageId]);
            if (($message[0]['sender_id'] == $this->user->getId() || $message[0]['recipient_id'] == $this->user->getId())) {
                if ($message[0]['sender_id'] == $this->user->getId() && $message[0]['sender_removed'] == 0) {
                    if ($message[0]['recipient_removed'] == '0') {
                        $this->database->deleteOrUpdateWhere('UPDATE private_messages SET sender_removed=1 WHERE id_message=?', [$messageId]);
                    } else {
                        $this->database->deleteOrUpdateWhere('DELETE FROM private_messages WHERE id_message=?', [$messageId]);
                    }
                    return true;
                } elseif ($message[0]['recipient_id'] == $this->user->getId() && $message[0]['recipient_removed'] == 0) {
                    if ($message[0]['recipient_removed'] == '0') {
                        $this->database->deleteOrUpdateWhere('UPDATE private_messages SET recipient_removed=1 WHERE id_message=?', [$messageId]);
                    } else {
                        $this->database->deleteOrUpdateWhere('DELETE FROM private_messages WHERE id_message=?', [$messageId]);
                    }
                    return true;
                }
            }
        }
        return false;
    }
}
