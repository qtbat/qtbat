<?php
namespace Qtbat\Model;

use Qtbat\Exception\PageError;
use Exception;

class CreateContest
{
    function __construct($database, $user)
    {
        $this->database = $database;
        $this->user = $user;
    }

    public function display()
    {
        $loggeduser_Id = $this->user->getId();
        $groups_list = $this->database->selectWithWhere('SELECT g.id_group, g.name, g.unique_name
            FROM users_groups as u_g
            INNER JOIN groups as g ON g.id_group=u_g.id_group
            WHERE u_g.id_user=? AND u_g.member_status=1', array($loggeduser_Id));
        $categories = $this->database->selectWithWhere('SELECT id_category, name FROM categories');

        return array('groups_list'=>$groups_list, 'categories'=>$categories);
    }

    public function countCategories()
    {
        $count = $this->database->selectWithWhere('SELECT COUNT(id_category) as count FROM categories');

        return $count[0]['count'];
    }

    public function uploadContest($values)
    {
        $loggeduser_Id = $this->user->getId();
        $does_link_exist = $this->database->selectWithWhere('SELECT id_contest FROM contests WHERE creator_id=? AND lower(link)=lower(?)', array($loggeduser_Id, $values['link']));

        if (empty($does_link_exist)) {
            $status = 'success';

            if (!empty($values['groups'])) {
                $groups = json_decode($values['groups']);
                $index = count($groups)-1;

                for ($i=0; $i<=$index; $i++) {
                    $is_in_group = $this->database->selectWithWhere('SELECT id_user FROM users_groups WHERE id_group=? AND id_user=? AND member_status=1', array($groups[$i], $loggeduser_Id));

                    if (empty($is_in_group)) {
                        $status = 'error';
                    }
                }
            }

            if ($status=='success') {

                $this->database->insert('contests', [
                    'creator_id' => $loggeduser_Id,
                    'contest_type' => $values['contest_type'],
                    'link' => $values['link'],
                    'avaible_for' => $values['avaible_for'],
                    'title' => $values['title'],
                    'description' => $values['description'],
                    'id_category' => $values['category'],
                    'number_of_winners' => $values['number_of_winners'],
                    'prize_delivery' => $values['prize_delivery'],
                    'prize' => $values['prize']
                ]);

                $id_contest = $this->database->selectWithWhere('SELECT id_contest FROM contests
                    WHERE creator_id=? AND link=?', array($loggeduser_Id, $values['link']));

                if ($values['contest_type']==1) {
                    $this->database->insert('first_few', [
                        'id_contest' => $id_contest[0]['id_contest'],
                        'question' => $values['question'],
                        'answer' => $values['answer'],
                        'answer_form' => $values['answer_form']
                    ]);
                } elseif ($values['contest_type']==2) {
                    $this->database->insert('draw', [
                        'id_contest' => $id_contest[0]['id_contest'],
                        'requirement' => $values['requirement'],
                        'method' => $values['method'],
                        'number_of_applications' => $values['number_of_applications']
                    ]);
                } elseif ($values['contest_type']==3) {
                    $this->database->insert('select_winner', [
                        'id_contest' => $id_contest[0]['id_contest'],
                        'requirements' => $values['requirements'],
                        'limit_submission_date' => $values['limit_submission_date']
                    ]);
                } elseif ($values['contest_type']==4) {

                }

                if (!empty($values['groups'])) {
                    for ($i=0; $i<=$index; $i++) {
                        $this->database->insert('contests_groups', [
                            'id_group' => $groups[$i],
                            'id_contest' => $id_contest[0]['id_contest']
                        ]);
                    }
                }
            } else {
                throw new \Exception('Nie należysz do grupy, dla której chcesz udostępnić ten konkurs!');
            }

        } else {
            throw new \Exception('Stworzyłeś już wcześniej konkurs o identycznym linku!');
        }
    }
}
