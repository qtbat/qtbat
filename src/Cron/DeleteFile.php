<?php

define('DIR', dirname(__DIR__, 2).'/');
require_once DIR.'vendor/autoload.php';

use Qtbat\Engine\Config;
use Qtbat\Engine\Database;

$config = new Config('config.php');
$disks = new Config('file_management_config.php');
$database = new Database($config);
$errors = [];

$numberFiles = $config->get('application', 'cron_files_delete');

$result = $database->selectWithWhere(
    'SELECT fu.id_file, uf.name, uf.disk, uf.folder
    FROM files_to_unlink AS fu
    RIGHT JOIN uploaded_files AS uf
    ON fu.id_file=uf.id_file
    WHERE fu.unlink=true
    ORDER BY fu.id_file LIMIT '.$numberFiles
);

if (!empty($result)) {
    foreach ($result as $file) {
        $path = $disks->get($file['disk'], 'path');
        if (file_exists($path.$file['folder'].'/'.$file['name'])) {
            if (unlink($path.$file['folder'].'/'.$file['name'])) {
                $database->deleteOrUpdateWhere('DELETE FROM files_to_unlink WHERE id_file=?', [$file['id_file']]);
                $database->deleteOrUpdateWhere('DELETE FROM applications_files WHERE id_file=?', [$file['id_file']]);
                $database->deleteOrUpdateWhere('DELETE FROM uploaded_files WHERE id_file=?', [$file['id_file']]);
            } else {
                $errors[] = '['.$file['disk'].'] Couldn\'t remove "'.$file['folder'].'/'.$file['name'].'"';
            }
        } else {
            $database->deleteOrUpdateWhere('DELETE FROM files_to_unlink WHERE id_file=?', [$file['id_file']]);
            $database->deleteOrUpdateWhere('DELETE FROM applications_files WHERE id_file=?', [$file['id_file']]);
            $database->deleteOrUpdateWhere('DELETE FROM uploaded_files WHERE id_file=?', [$file['id_file']]);
        }
    }
}

if (!empty($errors)) {
    // TODO save errors to log, send email alerts to admin
}
