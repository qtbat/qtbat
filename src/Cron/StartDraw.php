<?php

define('DIR', dirname(__DIR__, 2).'/');
require_once DIR.'vendor/autoload.php';

use Qtbat\Engine\Config;
use Qtbat\Engine\Database;

$config = new Config('config.php');
$database = new Database($config);
$user = null;

$contests = $database->selectWithWhere("SELECT d.id_contest, c.number_of_winners, d.requirement, c.creator_id
    FROM draw as d
    JOIN contests as c ON c.id_contest=d.id_contest
    WHERE d.date<=date_trunc('minute', now()) AND d.status=0");

if (!empty($contests)) {
    $contest_functionality = new \Qtbat\Model\ContestFunctionality($database, $user);

    foreach ($contests as $contest) {
        if ($contest['requirement']==null) {
            $status_value = 2;
        } else {
            $status_value = 1;
        }
        $contest_functionality->startDraw($contest['id_contest'], $contest['number_of_winners'], $status_value, $contest['creator_id']);
    }
}
