<?php

define('DIR', dirname(__DIR__, 2).'/');
require_once DIR.'vendor/autoload.php';

use Qtbat\Engine\Config;
use Qtbat\Engine\Database;

$config = new Config('config.php');
$database = new Database($config);
$user = null;

$contests = $database->selectWithWhere("SELECT d.id_contest, c.creator_id
    FROM draw as d
    JOIN contests as c ON c.id_contest=d.id_contest
    WHERE c.date<=date_trunc('minute', now()) AND d.status=1");

if (!empty($contests)) {
    $contest_functionality = new \Qtbat\Model\ContestFunctionality($database, $user);

    foreach ($contests as $contest) {
        //contests_notifications
        $this->database->insert('contests_notifications', [
            'id_receiver' => $contest['creator_id']
            'id_contest' => $contest['id_contest'],
            'type' => 5
        ]);
        $contest_functionality->confirmDraw($contest['id_contest']);
    }
}
