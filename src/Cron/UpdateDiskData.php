<?php

define('DIR', dirname(__DIR__, 2).'/');
$outputFile = DIR.'disk_data.txt';
$configFile = DIR.'config/file_management_config.php';

$errors = [];
if (file_exists($configFile)) {
    $config = require $configFile;
    $disks = [];

    foreach ($config as $disk => $data) {
        if (file_exists($data['path'])) {
            // files
            $files = exec('find '.$data['path'].' -type f | wc -l');
            if ($files >= 0) {
                $disks[$disk]['files'] = $files;
            } else {
                $errors[] = '['.$disk.'] Couldn\'t count files! Returned: "'.$files.'"';
            }
            // size
            $result = exec('du -sb '.$data['path']);
            preg_match('/^[0-9]+/', $result, $matches);
            if (isset($matches[0]) && $matches[0] >= 0) {
                $disks[$disk]['size'] = round($matches[0]/1048576, 5);
            } else {
                $errors[] = '['.$disk.'] Couldn\'t read disk size! Returned: "'.$result.'"';
            }
            // folders
            $disks[$disk]['folders'] = [];
            $folders = glob($data['path'].'*' , GLOB_ONLYDIR);
            if (!empty($folders)) {
                foreach ($folders as $folder) {
                    $filesInFolder = exec('find '.$folder.' -type f | wc -l');
                    $folder = basename($folder);
                    if ($filesInFolder >= 0) {
                        $disks[$disk]['folders'][$folder] = ['files' => $filesInFolder];
                    } else {
                        $errors[] = '['.$disk.'] Couldn\'t count files in folder "'.$folder.'"! Returned: "'.$filesInFolder.'"';
                    }
                }
            } else {
                // empty disk - only first folder
                $folder = substr(md5(date('l jS F Y h:i:s A')),0,5);
                $oldmask = umask(0);
                mkdir($data['path'].$folder.'/', 0777, true); //TODO file permissions
                umask($oldmask);
                $disks[$disk]['folders'][$folder] = ['files' => 0];
            }
        } else {
            $errors[] = '['.$disk.'] disk doesnt\'t exist!';
        }
    }

    // save JSON to file
    file_put_contents($outputFile, json_encode($disks));
    chmod($outputFile, 0777); //TODO file permissions
} else {
    $errors[] = 'Config file "'.$configFile.'" doesn\'t exist!';
}

if (!empty($errors)) {
    // TODO save errors to log, send email alerts to admin
}
