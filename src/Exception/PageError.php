<?php
namespace Qtbat\Exception;

use Exception;

class PageError extends Exception
{
    protected $errors = [
        '403' => 'Forbidden',
        '404' => 'Not Found'
    ];
    public function __construct($code)
    {
        $description = (isset($this->errors[$code])) ? $this->errors[$code] : '';
        header('HTTP/1.0 '.$code.' '.$description);
        parent::__construct('<h1>Page error '.$code.'</h1><h2>'.$description.'</h2>');
    }
}
