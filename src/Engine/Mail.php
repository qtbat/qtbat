<?php
namespace Qtbat\Engine;

use Exception;

class Mail
{
    protected $transport;
    protected $message;

    public function __construct()
    {
        $config = new Config();
        $this->transport = \Swift_SmtpTransport::newInstance($config->get('mail', 'server'), $config->get('mail', 'port'));
        $this->transport->setUsername($config->get('mail', 'username'));
        $this->transport->setPassword($config->get('mail', 'password'));
        $this->message = \Swift_Message::newInstance();
        $this->message->setFrom($config->get('mail', 'fromEmail'), $config->get('mail', 'fromName'));
    }

    public function set($toEmail, $toName, $subject, $templateName, $templateArray)
    {
        $this->message->setTo($toEmail, $toName);
        $this->message->setSubject($subject);
        $twigConfig = (empty(TWIG_CACHE)) ? [] : ['cache' => DIR.'cache'];
        $loader = new \Twig_Loader_Filesystem(DIR.'templates/email/');
        $twig = new \Twig_Environment($loader, $twigConfig);
        $twig->addGlobal('path', PATH);
        $twig->addGlobal('app_name', APP_NAME);
        $twig->addGlobal('name', $toName);
        $twig->addGlobal('subject', $subject);
        $template = $twig->render($templateName, $templateArray);
        $this->message->setBody($template, 'text/html');
    }

    public function send()
    {
        $mailer = \Swift_Mailer::newInstance($this->transport);
        $mailer->send($this->message);
        return true;
    }
}
