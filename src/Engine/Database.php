<?php
namespace Qtbat\Engine;

use PDO;
use PDOException;

class Database
{
    private $pdo;
    private $config;

    public function __construct($config)
    {
        try {
            $this->pdo = new PDO($config->get('database', 'dsn'), $config->get('database', 'username'), $config->get('database', 'password'));
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->query("SET CLIENT_ENCODING TO 'UTF8'");

            if (php_sapi_name() !== 'cli') {
                if (!isset($_SESSION['timezone'])) {
                    $timezone = 'UTC'; //TODO API for timezones
                    $_SESSION['timezone'] = $timezone;
                } else {
                    $timezone = $_SESSION['timezone'];
                }

                $this->pdo->query("SET TIMEZONE TO '".$timezone."'");
            }
        } catch (PDOException $e) {
            die('BŁĄD połączenia z bazą danych: '.$e->getMessage());
        }
    }

    public function selectWithWhere($sql, $values = [])
    {
        $query = $this->pdo->prepare($sql);
        if (!empty($values)) {
            $v = 1;
            foreach($values as $value) {
                $query->bindValue($v, $value);
                $v++;
            }
        }
        if ($query->execute()) {
            $results = $query->fetchAll();
            return $results;
        }
        return false;
    }

    public function insert($table, $values)
    {
            $countValues = count($values);
            $valueNumber = 1;
            $sqlColumns = '';
            $sqlValues = '';
            foreach ($values as $column => $value)
            {
                $sqlColumns .= $column;
                $sqlValues .= '?';
                if ($valueNumber<$countValues) {
                    $sqlColumns .= ',';
                    $sqlValues .= ',';
                }
                $valueNumber++;
            }
            $sql = 'INSERT INTO '.$table.' ('.$sqlColumns.') VALUES ('.$sqlValues.')';
            $query = $this->pdo->prepare($sql);
            $valueNumber = 1;
            foreach ($values as $value) {
                $query->bindValue($valueNumber, $value);
                $valueNumber++;
            }
            return $query->execute();
      }

  /*  public function insert($table, $values)
    {
        $sql = 'INSERT INTO '.$table.' SET';
        $countValues = count($values);
        $valueNumber = 1;
        foreach ($values as $column => $value)
        {
            $sql .= ' '.$column.'=?';
            if ($valueNumber<$countValues) {
                $sql .= ',';
            }
            $valueNumber++;
        }
        $query = $this->pdo->prepare($sql);
        $valueNumber = 1;
        foreach ($values as $value) {
            $query->bindValue($valueNumber, $value);
            $valueNumber++;
        }
        return $query->execute();
    } */

    public function deleteOrUpdateWhere($sql, $values = [], $count = false)
    {
        $query = $this->pdo->prepare($sql);
        if (!empty($values)) {
            $v = 1;
            foreach ($values as $value) {
                $query->bindValue($v, $value);
                $v++;
            }
        }
        if ($count == true) {
            $query->execute();
            return $query->rowCount();
        } else {
            return $query->execute();
        }
    }
}
