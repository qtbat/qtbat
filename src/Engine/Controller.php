<?php
namespace Qtbat\Engine;

use Twig_Loader_Filesystem;
use Twig_Environment;

abstract class Controller
{
    private $method;
    private $matches;
    public $twig;
    public $success;
    public $error;

    public function __construct()
    {
        $twigConfig = (empty(TWIG_CACHE)) ? [] : ['cache' => DIR.'cache'];
        $loader = new Twig_Loader_Filesystem(DIR.'templates/');
        $this->twig = new Twig_Environment($loader, $twigConfig);
        $this->twig->addGlobal('path', PATH);
        $this->twig->addGlobal('app_name', APP_NAME);
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function set($method, $matches)
    {
        $this->method = $method;
        $this->matches = $matches;
    }

    public function runMethod()
    {
        $method = $this->method;
        if (!empty($method)) {
            $this->$method($this->matches);
        }
    }

    public function redirect($to)
    {
        header('Location: '.PATH.$to);
        exit();
    }
}
