<?php

namespace Qtbat\Engine;

class UploadFile
{
	private $database;

	public function __construct($database)
	{
		$this->database=$database;
	}

	public function uploadAvatar($loggeduser_Id, $avatar)
	{
		$file_extension = pathinfo($avatar['name'], PATHINFO_EXTENSION);
		$avatar_hash = substr(md5(date('l jS F Y h:i:s A') . $loggeduser_Id),0,15);

		if (!empty($avatar['tmp_name'])) {
			$image_info = getimagesize($avatar['tmp_name']);
			$image_width = $image_info[0];
			$image_height = $image_info[1];
		}

		if (empty($avatar['tmp_name'])) {
			throw new \Exception("Nie wybrałeś pliku!");
		} elseif (getimagesize($avatar['tmp_name'])==false) {
			throw new \Exception("Możesz wybrać tylko zdjęcie!");
		} elseif (($avatar['size'] / 1024) > 100) {
			throw new \Exception("Plik, który próbujesz wysłać jest zbyt duży.");
		} elseif ($image_width > 200 || $image_height > 200) {
			throw new \Exception("Wymiary pliku są zbyt duże!");
		} elseif ($image_width < 100 || $image_height < 100) {
			throw new \Exception("Wymiary pliku są zbyt małe!");
		} elseif (!empty($avatar) && $file_extension != "gif" && $file_extension != "GIF" && $file_extension != "png" && $file_extension != "PNG" && $file_extension != "jpg" && $file_extension != "JPG" && $file_extension != "jpeg" && $file_extension != "JPEG") {
			throw new \Exception("Nieobsługiwany format zdjęcia.");
		} else {
			$current_avatar = $this->database->selectWithWhere("SELECT uf.name, uf.disk, uf.folder
				FROM uploaded_files as uf
				JOIN users_avatars as ua ON ua.id_file=uf.id_file
				JOIN users as u ON u.id_user=ua.id_user
				WHERE u.id_user=?", array($loggeduser_Id));

			$file_management = new \Qtbat\Engine\FileManagement();

			$error = false;

			if (empty($current_avatar)) {
				$directory_info = $file_management->selectDirectory($avatar['size']);
				if ($directory_info['status']=='error') {
					$error = true;
					throw new \Exception('Wystąpił błąd w przesyłaniu twojego pliku na serwer.');
				} else {
					$this->database->insert('uploaded_files', [
						'name' => $avatar_hash . '.jpg',
						'disk' => $directory_info['disk'],
						'folder' => $directory_info['folder']
					]);

					$id_file = $this->database->selectWithWhere('SELECT id_file FROM uploaded_files WHERE name=?', [$avatar_hash . '.jpg']);

					$this->database->insert('users_avatars', [
						'id_user' => $loggeduser_Id,
						'id_file' => $id_file[0]['id_file']
					]);

					$target = $directory_info['path'] . $avatar_hash . '-n.jpg';
					$directory = $directory_info['path'] . $avatar_hash . '.jpg';
				}
			} else {
				$current_avatar_disk = $file_management->getPath($current_avatar[0]['disk']);
				$current_avatar_directory = $current_avatar_disk . '/' .$current_avatar[0]['folder'];
				$current_avatar_name = explode('.', $current_avatar[0]['name']);

				$target = $current_avatar_directory . '/' . $current_avatar_name[0] . '-n.' . $current_avatar_name[1];
				$directory = $current_avatar_directory . '/' . $current_avatar[0]['name'];
			}

			if ($error === false) {
				move_uploaded_file($avatar['tmp_name'], $target);

				if ($file_extension == "gif" || $file_extension == "GIF") {
					$imgcreate = imagecreatefromgif($target);
				} elseif ($file_extension == "png" || $file_extension == "PNG") {
					$imgcreate = imagecreatefrompng($target);
				} elseif ($file_extension == "jpg" || $file_extension == "jpeg" || $file_extension == "JPG" || $file_extension == "jpeg")  {
					$imgcreate = imagecreatefromjpeg($target);
				}
				$ictc = imagecreatetruecolor(150,150);
				imagecopyresampled($ictc, $imgcreate, 0, 0, 0, 0, 150, 150, $image_width, $image_height);
				imagejpeg($ictc, $directory, 100);

				if (file_exists($target)) {
					unlink($target);
				}
			}
		}
	}

	public function validateContestImage($image)
	{
		try {
			$file_extension = pathinfo($image['name'], PATHINFO_EXTENSION);
			$image_info = getimagesize($image['tmp_name']);
			$image_width = $image_info[0];
			$image_height = $image_info[1];

			if (getimagesize($image['tmp_name'])==false) {
				throw new \Exception("Plik, który wybrałeś nie jest zdjęciem.");
			} elseif (($image['size'] / 1024) > 300) {
				throw new \Exception("Plik który próbujesz wysłać jest zbyt duży.");
			} elseif ($image_width > 1524 || $image_height > 1524) {
				throw new \Exception("Wymiary pliku są zbyt duże!");
			} elseif (!empty($image) && $file_extension != "gif" && $file_extension != "GIF" && $file_extension != "png" && $file_extension != "PNG" && $file_extension != "jpg" && $file_extension != "JPG" && $file_extension != "jpeg" && $file_extension != "JPEG") {
				throw new \Exception("Niedozwolony format zdjęcia.");
			}
			$status = 'success';
		} catch (\Exception $e) {
			$status = 'error';
			$message = $e->getMessage();
		}

		if ($status == 'success') {
			return $status;
		} else {
			return $message;
		}
	}

	public function uploadContestImage($loggeduser_Id, $image)
	{
		$file_extension = pathinfo($image['name'], PATHINFO_EXTENSION);
		$img_hash = substr(md5(date('l jS F Y h:i:s A') . $loggeduser_Id . $image['tmp_name']),0,15);

		$file_management = new \Qtbat\Engine\FileManagement();
		$directory_info = $file_management->selectDirectory($image['size']);
		if ($directory_info['status'] == 'error') {
			throw new \Exception('Wystąpił problem z przesłaniem twojego pliku na serwer.');
		} else {
			$target = $directory_info['path'] . $img_hash . '-n.jpg';

			if (!empty($image['tmp_name'])) {
				$image_info = getimagesize($image['tmp_name']);
				$image_width = $image_info[0];
				$image_height = $image_info[1];
			}

			move_uploaded_file($image['tmp_name'], $target);
			if ($file_extension == "gif" || $file_extension == "GIF") {
				$imgcreate = imagecreatefromgif ($target);
			} elseif ($file_extension == "png" || $file_extension == "PNG") {
				$imgcreate = imagecreatefrompng($target);
			} elseif ($file_extension == "jpg" || $file_extension == "jpeg" || $file_extension == "JPG" || $file_extension == "jpeg")  {
				$imgcreate = imagecreatefromjpeg($target);
			}
			$ictc = imagecreatetruecolor($image_width, $image_height);
			imagecopyresampled($ictc, $imgcreate, 0, 0, 0, 0, $image_width, $image_height, $image_width, $image_height);
			imagejpeg($ictc, $directory_info['path'] . $img_hash . '.jpg', 100);

			if (file_exists($target)) {
				unlink($target);
			}

			return ['disk' => $directory_info['disk'], 'folder' => $directory_info['folder'], 'name' => $img_hash . '.jpg'];
		}
	}

	public function changeContestImage($path, $folder, $name, $image)
	{
		if (file_exists($path.$folder.'/'.$name)) {
			unlink($path.$folder.'/'.$name);
		}

		$file_extension = pathinfo($image['name'], PATHINFO_EXTENSION);

		$name_elements = explode('.', $name);

		$target = $path . $folder . '/' . $name_elements[0] . '-n.jpg';

		if (!empty($image['tmp_name'])) {
			$image_info = getimagesize($image['tmp_name']);
			$image_width = $image_info[0];
			$image_height = $image_info[1];
		}

		move_uploaded_file($image['tmp_name'], $target);
		if ($file_extension == "gif" || $file_extension == "GIF") {
			$imgcreate = imagecreatefromgif ($target);
		} elseif ($file_extension == "png" || $file_extension == "PNG") {
			$imgcreate = imagecreatefrompng($target);
		} elseif ($file_extension == "jpg" || $file_extension == "jpeg" || $file_extension == "JPG" || $file_extension == "jpeg")  {
			$imgcreate = imagecreatefromjpeg($target);
		}
		$ictc = imagecreatetruecolor($image_width, $image_height);
		imagecopyresampled($ictc, $imgcreate, 0, 0, 0, 0, $image_width, $image_height, $image_width, $image_height);
		imagejpeg($ictc, $path . $folder . '/' . $name, 100);

		if (file_exists($target)) {
			unlink($target);
		}
	}

	public function unlinkContestImage($path, $folder, $name)
	{
		unlink($path.$folder.'/'.$name);
	}

	public function takePartContestImage($loggeduser_Id, $image, $min_width, $min_height, $max_width, $max_height)
	{
		$file_extension = pathinfo($image['name'], PATHINFO_EXTENSION);
		$image_hash = substr(md5(date('l jS F Y h:i:s A') . $loggeduser_Id),0,15);

		if (!empty($image['tmp_name'])) {
			$image_info = getimagesize($image['tmp_name']);
			$image_width = $image_info[0];
			$image_height = $image_info[1];
		}

		if (empty($image['tmp_name'])) {
			throw new \Exception("Nie wybrałeś pliku!");
		} elseif (getimagesize($image['tmp_name'])==false) {
			throw new \Exception("Możesz wybrać tylko zdjęcie!");
		} elseif (($image['size'] / 1024) > 500) {
			throw new \Exception("Plik, który próbujesz wysłać jest zbyt duży.");
		} elseif ($image_width > $max_width || $image_height > $max_height) {
			throw new \Exception("Wymiary pliku są zbyt duże!");
		} elseif ($image_width < $min_width || $image_height < $min_height) {
			throw new \Exception("Wymiary pliku są zbyt małe!");
		} elseif (!empty($image) && $file_extension != "gif" && $file_extension != "GIF" && $file_extension != "png" && $file_extension != "PNG" && $file_extension != "jpg" && $file_extension != "JPG" && $file_extension != "jpeg" && $file_extension != "JPEG") {
			throw new \Exception("Nieobsługiwany format zdjęcia.");
		} else {

			$file_management = new \Qtbat\Engine\FileManagement();
			$directory_info = $file_management->selectDirectory($image['size']);
			if ($directory_info['status'] == 'error') {
				throw new \Exception('Wystąpił błąd w przesyłaniu twojego pliku na serwer');
			} else {
				move_uploaded_file($image['tmp_name'], $directory_info['path'].$image_hash.".".$file_extension);

				return ['disk' => $directory_info['disk'], 'folder' => $directory_info['folder'], 'name' => $image_hash.".".$file_extension, 'original_name' => $image['name']];
			}
		}
	}

	public function takePartContestFile($loggeduser_Id, $tmp_name, $file_size, $name)
	{
		$file_extension = pathinfo($name, PATHINFO_EXTENSION);
		$file_hash = substr(md5(date('l jS F Y h:i:s A') . $loggeduser_Id . $tmp_name),0,15);

		$file_management = new \Qtbat\Engine\FileManagement();
		$directory_info = $file_management->selectDirectory($file_size);

		move_uploaded_file($tmp_name, $directory_info['path'].$file_hash.".".$file_extension);

		return ['disk' => $directory_info['disk'], 'folder' => $directory_info['folder'], 'name' => $file_hash.".".$file_extension, 'original_name' => $name];
	}

	public function uploadContestBanner($id_contest, $image)
	{
		$file_extension = pathinfo($image['name'], PATHINFO_EXTENSION);
		$image_hash = substr(md5(date('l jS F Y h:i:s A') . $id_contest),0,15);

		if (!empty($image['tmp_name'])) {
			$image_info = getimagesize($image['tmp_name']);
			$image_width = $image_info[0];
			$image_height = $image_info[1];
		}

		if (empty($image['tmp_name'])) {
			throw new \Exception("Nie wybrałeś pliku!");
		} elseif (getimagesize($image['tmp_name'])==false) {
			throw new \Exception("Możesz wybrać tylko zdjęcie!");
		} elseif (($image['size'] / 1024) > 500) {
			throw new \Exception("Plik, który próbujesz wysłać jest zbyt duży.");
		} elseif ($image_width > 2000 || $image_height > 600) {
			throw new \Exception("Wymiary pliku są zbyt duże!");
		} elseif ($image_width < 400 || $image_height < 150) {
			throw new \Exception("Wymiary pliku są zbyt małe!");
		} elseif (!empty($image) && $file_extension != "png" && $file_extension != "PNG" && $file_extension != "jpg" && $file_extension != "JPG" && $file_extension != "jpeg" && $file_extension != "JPEG") {
			throw new \Exception("Nieobsługiwany format zdjęcia.");
		} else {
			$current_banner = $this->database->selectWithWhere('SELECT uf.name, uf.disk, uf.folder
				FROM uploaded_files as uf
				JOIN contests_images as ci ON uf.id_file=ci.id_file
				JOIN contests as c ON c.id_contest=ci.id_contest
				WHERE c.id_contest=? AND ci.type=1', [$id_contest]);

			$file_management = new \Qtbat\Engine\FileManagement();

			$error = false;

			if (empty($current_banner)) {
				$directory_info = $file_management->selectDirectory($image['size']);
				if ($directory_info['status']=='error') {
					$error = true;
					throw new \Exception('Wystąpił błąd w przesyłaniu twojego pliku na serwer.');
				} else {
					$this->database->insert('uploaded_files', [
						'name' => $image_hash . '.' . $file_extension,
						'disk' => $directory_info['disk'],
						'folder' => $directory_info['folder']
					]);

					$id_file = $this->database->selectWithWhere('SELECT id_file FROM uploaded_files WHERE name=?', [$image_hash . '.' . $file_extension]);

					$this->database->insert('contests_images', [
						'id_contest' => $id_contest,
						'id_file' => $id_file[0]['id_file'],
						'type' => 1
					]);

					$directory = $directory_info['path'] . $image_hash . '.' . $file_extension;
				}
			} else {
				$current_banner_disk = $file_management->getPath($current_banner[0]['disk']);
				$current_banner_directory = $current_banner_disk . '/' .$current_banner[0]['folder'];

				$directory = $current_banner_directory . '/' . $current_banner[0]['name'];
			}

			if ($error === false) {
				move_uploaded_file($image['tmp_name'], $directory);
			}
		}
	}
}
