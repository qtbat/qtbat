<?php
namespace Qtbat\Engine;

use Exception;

class FileManagement
{
    private $names;

    public function __construct($file = 'file_management_config.php')
    {
        if (!file_exists(DIR.'config/'.$file)) {
            throw new Exception('Plik config/'.$file.' nie istnieje!');
        }
        $this->names = require DIR.'config/'.$file;
    }

    private function prepare($file_size, $errored_disks) {
        try {
            $disk_error = false;
            $names = $this->names;

            $disk_data_file = file_get_contents(DIR.'disk_data.txt');

            if ($disk_data_file) {

                $disk_data = json_decode($disk_data_file, true);

                $ladder = [];

                foreach ($disk_data as $key => $data) {
                    if (!in_array($key, $errored_disks)) {
                        $ladder[$key] = $data['files']*$data['size'];
                    }
                }

                if (!empty($ladder)) {
                    $array = array_keys($ladder, min($ladder));

                    $disk_name = $array[0];

                    if (array_key_exists($disk_name, $names)) {

                        if (isset($names[$disk_name]) && isset($names[$disk_name]['path']) && isset($names[$disk_name]['files_per_folder'])) {

                            $avaible_folders = [];

                            foreach ($disk_data[$disk_name]['folders'] as $name => $folder) {
                                if ($folder['files']<$names[$disk_name]['files_per_folder']) {
                                    $avaible_folders[] = $name;
                                }
                            }

                            if (empty($avaible_folders)) {
                                $folder = (string)substr(md5(date('l jS F Y h:i:s A')),0,5);
                                while (file_exists($names[$disk_name]['path'].$folder)) {
                                    $folder = substr(md5(date('l jS F Y h:i:s A')),0,5);
                                }
                                $disk_data[$disk_name]['folders'][$folder] = [];
                                $disk_data[$disk_name]['folders'][$folder]['files'] = 1;
                            } else {
                                $folder = (string)$avaible_folders[0];
                                $disk_data[$disk_name]['folders'][$folder]['files'] += 1;
                            }

                            if (!file_exists($names[$disk_name]['path'].$folder)) {
                                mkdir($names[$disk_name]['path'].$folder, 0777, true); //TODO file permissions
                            }

                            $disk_data[$disk_name]['size'] += round($file_size / 1048576, 5);
                            $disk_data[$disk_name]['files'] += 1;

                            $new_disk_data = json_encode($disk_data);

                            file_put_contents(DIR.'disk_data.txt', $new_disk_data);

                            return ['status'=>'success', 'disk'=>$disk_name, 'folder'=>$folder, 'path'=>$names[$disk_name]['path'].$folder.'/'];

                        } else {
                            $disk_error = true;
                        }
                    } else {
                        $disk_error = true;
                    }
                } else {
                    throw new \Exception();
                }
            } else {
                throw new \Exception();
            }

            if ($disk_error) {
                array_push($errored_disks, $disk_name);
                return prepare($file_size, $errored_disks, $names);
            }

        } catch (\Exception $e) {
            return ['status'=>'error'];
        }
    }

    public function selectDirectory($file_size)
    {
        $errored_disks = [];

        return $this->prepare($file_size, $errored_disks);
    }

    public function getPath($disk_name)
    {
        return $this->names[$disk_name]['path'];
    }
}
