<?php
namespace Qtbat\Engine;

use Exception;

class Config
{
    private $config;

    public function __construct($file = 'config.php')
    {
        if (!file_exists(DIR.'config/'.$file)) {
            throw new Exception('Plik config/'.$file.' nie istnieje!');
        }
        $this->config = require DIR.'config/'.$file;
    }

    public function get($table, $var)
    {
        if (isset($this->config[$table])) {
            if (isset($this->config[$table][$var])) {
                return $this->config[$table][$var];
            } else {
                throw new Exception('Błąd konfiguracji! ["'.$table.'"]["'.$var.'"] nie istnieje.');
            }
        } else {
            throw new Exception('Błąd konfiguracji! ["'.$table.'"] nie istnieje.');
        }
    }
}
