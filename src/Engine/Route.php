<?php
namespace Qtbat\Engine;

use Qtbat\Exception\PageError;

class Route
{
    protected $routes = [
        // 'url with regex' => ['object', 'method']
        '/' => ['\\Qtbat\\Controller\\Index', 'index'],
        '/login' => ['\\Qtbat\\Controller\\User', 'login'],
        '/register' => ['\\Qtbat\\Controller\\User', 'register'],
        '/activate/([a-zA-Z0-9]{30})' => ['\\Qtbat\\Controller\\User', 'activate'],
        '/change-email/([a-zA-Z0-9]{30})' => ['\\Qtbat\\Controller\\User', 'changeEmail'],
        '/logout' => ['\\Qtbat\\Controller\\User', 'logout'],
        '/load-notifications' => ['\\Qtbat\\Controller\\User', 'loadNotifications'],
        '/read-notifications' => ['\\Qtbat\\Controller\\User', 'readNotifications'],
        //files
        '/img/([a-zA-Z0-9]{15})\.(jpg|jpeg|png|gif)(\?width=([0-9]+)&height=([0-9]+)|\?width=([0-9]+)|\?height=([0-9]+))?' => ['\\Qtbat\\Controller\\File', 'show'],
        '/download/([a-zA-Z0-9]{15})\.([a-zA-Z0-9]{1,5})' => ['\\Qtbat\\Controller\\File', 'download'],
        //profile
        '/settings' => ['\\Qtbat\\Controller\\Profile', 'settings'],
        '/([a-zA-Z0-9]{3,30})' => ['\\Qtbat\\Controller\\Profile', 'show'],
		'/settings/upload-avatar' => ['\\Qtbat\\Controller\\Profile', 'uploadAvatar'],
		'/settings/edit' => ['\\Qtbat\\Controller\\Profile', 'edit'],
        '/my-contests' => ['\\Qtbat\\Controller\\Profile', 'loggedUserContests'],
        '/load-my-contests' => ['\\Qtbat\\Controller\\Profile', 'loadLoggedUserContests'],
		//groups
        '/groups/create-group' => ['\\Qtbat\\Controller\\Groups', 'createGroup'],
        '/groups/create-group/send' => ['\\Qtbat\\Controller\\Groups', 'createGroupAction'],
		'/groups/([A-Za-z0-9-]{2,30})(\/(finished-contests|ongoing-contests))?' => ['\\Qtbat\\Controller\\Groups', 'contestsList'],
        '/groups/([A-Za-z0-9-]{2,30})/load-contests-list' => ['\\Qtbat\\Controller\\Groups', 'loadContestsList'],
        '/groups/([A-Za-z0-9-]{2,30})/administrator-panel' => ['\\Qtbat\\Controller\\Groups', 'administratorPanel'],
		'/groups/([A-Za-z0-9-]{2,30})/members-list' => ['\\Qtbat\\Controller\\Groups', 'membersList'],
		'/groups/([A-Za-z0-9-]{2,30})/edit' => ['\\Qtbat\\Controller\\Groups', 'groupEdit'],
        '/groups/([A-Za-z0-9-]{2,30})/user-interaction' => ['\\Qtbat\\Controller\\Groups', 'userInteraction'],
        '/groups/([A-Za-z0-9-]{2,30})/load-members' => ['\\Qtbat\\Controller\\Groups', 'loadMembersList'],
        '/invitation-confirm' => ['\\Qtbat\\Controller\\Groups', 'invitationConfirm'],
        // private messages
        '/messages' => ['\\Qtbat\\Controller\\PrivateMessage', 'getList'],
        '/messages/sent' => ['\\Qtbat\\Controller\\PrivateMessage', 'getListSent'],
        '/messages/delete' => ['\\Qtbat\\Controller\\PrivateMessage', 'deleteMessage'],
        '/messages/([a-zA-Z0-9]{3,30})/sent' => ['\\Qtbat\\Controller\\PrivateMessage', 'getListSent'],
        '/messages/([a-zA-Z0-9]{3,30})' => ['\\Qtbat\\Controller\\PrivateMessage', 'getList'],
        '/messages/([a-zA-Z0-9]{3,30})/new' => ['\\Qtbat\\Controller\\PrivateMessage', 'newMessage'],
        '/messages/([0-9]+)' => ['\\Qtbat\\Controller\\PrivateMessage', 'showMessage'],
        //create contest
        '/create-contest' => ['\\Qtbat\\Controller\\CreateContest', 'createContest'],
        '/create-contest/append' => ['\\Qtbat\\Controller\\CreateContest', 'appendElement'],
        '/create-contest/send-form' => ['\\Qtbat\\Controller\\CreateContest', 'sendForm'],
        //contest
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/send-answer' => ['\\Qtbat\\Controller\\ContestFunctionality', 'sendAnswer'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})' => ['\\Qtbat\\Controller\\Contest', 'displayContest'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/settings' => ['\\Qtbat\\Controller\\Contest', 'settings'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/edit-contest' => ['\\Qtbat\\Controller\\Contest', 'editContest'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/edit-contest-images' => ['\\Qtbat\\Controller\\Contest', 'editContestImages'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/edit-contest-banner' => ['\\Qtbat\\Controller\\Contest', 'editContestBanner'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/leave-opinion' => ['\\Qtbat\\Controller\\ContestFunctionality', 'leaveOpinion'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/start-contest' => ['\\Qtbat\\Controller\\Contest', 'startContest'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/delete-contest' => ['\\Qtbat\\Controller\\Contest', 'deleteContest'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/select-winner-take-part' => ['\\Qtbat\\Controller\\ContestFunctionality', 'selectWinnerTakePart'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/application-details' => ['\\Qtbat\\Controller\\Contest', 'selectWinnerApplicationDetails'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/confirm-winners' => ['\\Qtbat\\Controller\\ContestFunctionality', 'selectWinnerConfirmWinners'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/application-highlight' => ['\\Qtbat\\Controller\\ContestFunctionality', 'selectWinnerApplicationHighlight'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/([a-z\-]{15,21})/load-applications' => ['\\Qtbat\\Controller\\Contest', 'selectWinnerLoadApplications'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/([a-z\-]{15,21})' => ['\\Qtbat\\Controller\\Contest', 'selectWinnerApplicationsList'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/draw-take-part' => ['\\Qtbat\\Controller\\ContestFunctionality', 'drawTakePart'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/start-draw' => ['\\Qtbat\\Controller\\ContestFunctionality', 'startDraw'],
        '/([a-zA-Z0-9]{3,30})/([a-zA-Z0-9\-]{2,100})/confirm-draw' => ['\\Qtbat\\Controller\\ContestFunctionality', 'confirmDraw']
    ];

    public function run()
    {
        $url = '/'.trim(filter_input(INPUT_SERVER, 'REQUEST_URI'), '/');
        foreach ($this->routes as $key => $value) {
            if (preg_match('#^'.$key.'$#', $url, $matches)) {
                unset($matches[0]);
                $object = new $value[0]();
                $object->set($value[1], $matches);
                return $object;
            }
        }
        if (!isset($object)) {
            throw new PageError(404);
        }
    }
}
