<?php
namespace Qtbat\Engine;

use Qtbat\Model\User;

class Application
{
    private $config;
    private $database;
    private $user;

    public function __construct($config = null)
    {
        $this->config = $config;
    }

    public function start()
    {
        $this->database = new Database($this->config);
        $this->user = new User($this->database, $this->config);
        $route = new Route();
        $controller = $route->run();
        $controller->config = $this->config;
        $controller->database = $this->database;
        $controller->user = $this->user;
        $controller->runMethod();
    }
}
