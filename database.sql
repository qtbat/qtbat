CREATE TABLE contests (
    id_contest SERIAL NOT NULL,
    creator_id INT NOT NULL,
    contest_type SMALLINT NOT NULL,
    link VARCHAR(100) NOT NULL,
    avaible_for SMALLINT NOT NULL,
    create_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT date_trunc('second', now()),
    start_date TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    end_date TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    title VARCHAR(100) NOT NULL,
    description TEXT,
    id_category INT NOT NULL,
    number_of_winners SMALLINT,
    prize_delivery SMALLINT NOT NULL,
    prize TEXT,
    status SMALLINT DEFAULT 0,
    PRIMARY KEY(id_contest)
);

CREATE TABLE contests_participants (
    id_application SERIAL NOT NULL,
    id_user INT NOT NULL,
    id_contest INT NOT NULL,
    content TEXT DEFAULT NULL,
    position CHAR(1),
    status SMALLINT NOT NULL,
    PRIMARY KEY (id_application)
);

CREATE TABLE first_few (
    id_contest INT NOT NULL,
    question TEXT NOT NULL,
    answer TEXT NOT NULL,
    answer_form TEXT NOT NULL
);

CREATE TABLE draw (
    id_contest INT NOT NULL,
    requirement INT DEFAULT NULL,
    method INT NOT NULL,
    date TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    number_of_applications INT,
    status SMALLINT DEFAULT 0
);

CREATE TABLE select_winner (
    id_contest INT NOT NULL,
    requirements TEXT NOT NULL,
    limit_submission_date SMALLINT NOT NULL DEFAULT 0,
    submission_date TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    show_winners SMALLINT DEFAULT NULL
);

CREATE TABLE select_winner_applications (
    id_application INT NOT NULL,
    highlight CHAR(1) DEFAULT NULL,
    order_number INT NOT NULL
);

CREATE TABLE users (
   id_user SERIAL NOT NULL,
   login VARCHAR(30) NOT NULL UNIQUE,
   email VARCHAR(100) NOT NULL UNIQUE,
   password VARCHAR(72) NOT NULL,
   name VARCHAR(100),
   location VARCHAR(100),
   status SMALLINT NOT NULL DEFAULT 0,
   user_group SMALLINT NOT NULL DEFAULT 1,
   register_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT date_trunc('second', now()),
   register_ip VARCHAR(15) NOT NULL,
   last_date TIMESTAMP,
   last_ip VARCHAR(15),
   activation_hash VARCHAR(30),
   change_email VARCHAR(131),
   remind_password_hash VARCHAR(30),
   contact_email VARCHAR(100),
   sex VARCHAR(1) DEFAULT 0,
   PRIMARY KEY (id_user)
);

CREATE TABLE notifications (
    id_notification SERIAL NOT NULL,
    id_receiver INT NOT NULL,
    type SMALLINT NOT NULL,
    status SMALLINT DEFAULT 1,
    date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT date_trunc('second', now()),
    hash VARCHAR(4),
    PRIMARY KEY (id_notification)
);

CREATE TABLE contests_notifications (
    id_contest INT NOT NULL,
    id_notification INT NOT NULL
);

CREATE TABLE groups_notifications (
    id_group INT NOT NULL,
    id_notification INT NOT NULL
);

CREATE TABLE groups (
   id_group SERIAL NOT NULL,
   name VARCHAR(80) NOT NULL,
   unique_name VARCHAR(30) NOT NULL UNIQUE,
   description TEXT,
   type SMALLINT DEFAULT 0,
   create_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT date_trunc('second', now()),
   PRIMARY KEY (id_group)
);

CREATE TABLE users_groups (
   id_user INT NOT NULL,
   id_group INT NOT NULL,
   rank SMALLINT NOT NULL DEFAULT 0,
   joined_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT date_trunc('second', now()),
   member_status SMALLINT NOT NULL DEFAULT 0
);

CREATE TABLE categories (
   id_category SERIAL NOT NULL,
   name VARCHAR(255) NOT NULL,
   link VARCHAR(70) NOT NULL,
   PRIMARY KEY (id_category)
);

CREATE TABLE uploaded_files (
    id_file SERIAL NOT NULL,
    original_name TEXT DEFAULT NULL,
    name VARCHAR(21) NOT NULL UNIQUE,
    disk VARCHAR(15) NOT NULL,
    folder VARCHAR(5) NOT NULL,
    PRIMARY KEY (id_file)
);

CREATE TABLE files_to_unlink (
    id_file INT NOT NULL,
    unlink BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE contests_images (
    id_contest INT NOT NULL,
    id_file INT NOT NULL,
    type SMALLINT NOT NULL
);

CREATE TABLE contests_groups (
    id_group INT NOT NULL,
    id_contest INT NOT NULL,
    attached BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE users_opinions (
    id_opinion SERIAL NOT NULL,
    from_user INT NOT NULL,
    to_user INT NOT NULL,
    rate SMALLINT NOT NULL,
    comment TEXT,
    id_contest INT NOT NULL,
    date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT date_trunc('second', now()),
    PRIMARY KEY(id_opinion)
);

CREATE TABLE applications_files (
    id_application INT NOT NULL,
    id_file INT NOT NULL
);

CREATE TABLE users_avatars (
    id_user INT NOT NULL,
    id_file INT NOT NULL
);

CREATE TABLE private_messages (
   id_message SERIAL NOT NULL,
   sender_id INT NOT NULL,
   sender_removed SMALLINT NOT NULL DEFAULT 0,
   recipient_id INT NOT NULL,
   recipient_removed SMALLINT NOT NULL DEFAULT 0,
   topic VARCHAR(150) NOT NULL,
   content TEXT,
   send_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT date_trunc('second', now()),
   status SMALLINT NOT NULL DEFAULT 1,
   PRIMARY KEY (id_message)
);

CREATE TABLE deleted_private_messages (
   id_message INT NOT NULL,
   sender_id INT NOT NULL,
   recipient_id INT NOT NULL,
   topic VARCHAR(150) NOT NULL,
   content TEXT,
   delete_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT date_trunc('second', now()),
   PRIMARY KEY (id_message)
);

CREATE FUNCTION pm_archive_procedure() RETURNS trigger AS $$
DECLARE
	BEGIN
        INSERT INTO deleted_private_messages (id_message, sender_id, recipient_id, topic, content) VALUES (OLD.id_message, OLD.sender_id, OLD.recipient_id, OLD.topic, OLD.content);
        RETURN old;
	END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER pm_archive AFTER DELETE ON private_messages
FOR EACH ROW EXECUTE PROCEDURE pm_archive_procedure();

CREATE FUNCTION notifications_procedure() RETURNS trigger AS $$
DECLARE
	BEGIN
        IF TG_TABLE_NAME = 'notifications' THEN
            PERFORM pg_notify('notifications', 'cg:' || NEW.id_receiver || ':' || NEW.id_notification );
        ELSIF TG_TABLE_NAME = 'private_messages' THEN
           PERFORM pg_notify('notifications', 'pm:' || NEW.recipient_id );
        END IF;
        RETURN new;
	END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER pm_notify_trigger AFTER INSERT ON private_messages
FOR EACH ROW EXECUTE PROCEDURE notifications_procedure();

CREATE TRIGGER pg_notify_trigger AFTER INSERT ON notifications
FOR EACH ROW EXECUTE PROCEDURE notifications_procedure();


ALTER TABLE contests
ADD CONSTRAINT fk_contest_links_users
FOREIGN KEY (creator_id)
REFERENCES users(id_user);

ALTER TABLE contests
ADD CONSTRAINT fk_contests_categories
FOREIGN KEY (id_category)
REFERENCES categories(id_category);

ALTER TABLE first_few
ADD CONSTRAINT fk_first_few_contests
FOREIGN KEY (id_contest)
REFERENCES contests(id_contest);

ALTER TABLE draw
ADD CONSTRAINT fk_draw_contests
FOREIGN KEY (id_contest)
REFERENCES contests(id_contest);

ALTER TABLE select_winner
ADD CONSTRAINT fk_select_winner_contests
FOREIGN KEY (id_contest)
REFERENCES contests(id_contest);

ALTER TABLE contests_participants
ADD CONSTRAINT fk_contests_participants_contests
FOREIGN KEY (id_contest)
REFERENCES contests(id_contest);

ALTER TABLE contests_participants
ADD CONSTRAINT fk_contests_participants_users
FOREIGN KEY (id_user)
REFERENCES users(id_user);

ALTER TABLE select_winner_applications
ADD CONSTRAINT fk_select_winner_applications_contests_participants
FOREIGN KEY (id_application)
REFERENCES contests_participants(id_application);

ALTER TABLE notifications
ADD CONSTRAINT fk_notifications_users
FOREIGN KEY (id_receiver)
REFERENCES users(id_user);

ALTER TABLE contests_notifications
ADD CONSTRAINT fk_contest_notifications_contests
FOREIGN KEY (id_contest)
REFERENCES contests(id_contest);

ALTER TABLE contests_notifications
ADD CONSTRAINT fk_contests_notifications_notifications
FOREIGN KEY (id_notification)
REFERENCES notifications(id_notification);

ALTER TABLE contests_groups
ADD CONSTRAINT fk_contests_groups_groups
FOREIGN KEY (id_group)
REFERENCES groups(id_group);

ALTER TABLE contests_groups
ADD CONSTRAINT fk_contests_groups_contests
FOREIGN KEY (id_contest)
REFERENCES contests(id_contest);

ALTER TABLE users_groups
ADD CONSTRAINT fk_users_groups_users
FOREIGN KEY (id_user)
REFERENCES users(id_user);

ALTER TABLE users_groups
ADD CONSTRAINT fk_users_groups_groups
FOREIGN KEY (id_group)
REFERENCES groups(id_group);

ALTER TABLE groups_notifications
ADD CONSTRAINT fk_groups_notifications_groups
FOREIGN KEY (id_group)
REFERENCES groups(id_group);

ALTER TABLE groups_notifications
ADD CONSTRAINT fk_groups_notifications_notifications
FOREIGN KEY (id_notification)
REFERENCES notifications(id_notification);

ALTER TABLE users_opinions
ADD CONSTRAINT fk_users_opinions_from_users
FOREIGN KEY (from_user)
REFERENCES users(id_user);

ALTER TABLE users_opinions
ADD CONSTRAINT fk_users_opinions_to_users
FOREIGN KEY (to_user)
REFERENCES users(id_user);

ALTER TABLE users_opinions
ADD CONSTRAINT fk_users_opinions_contests
FOREIGN KEY (id_contest)
REFERENCES contests(id_contest);

ALTER TABLE private_messages
ADD CONSTRAINT fk_private_messages_sender
FOREIGN KEY (sender_id)
REFERENCES users(id_user);

ALTER TABLE private_messages
ADD CONSTRAINT fk_private_messages_recipient
FOREIGN KEY (recipient_id)
REFERENCES users(id_user);

ALTER TABLE applications_files
ADD CONSTRAINT fk_applications_files_uploaded_files
FOREIGN KEY (id_file)
REFERENCES uploaded_files(id_file);

ALTER TABLE applications_files
ADD CONSTRAINT fk_applications_files_contests_participants
FOREIGN KEY (id_application)
REFERENCES contests_participants(id_application);

ALTER TABLE contests_images
ADD CONSTRAINT fk_contests_images_contests
FOREIGN KEY (id_contest)
REFERENCES contests(id_contest);

ALTER TABLE contests_images
ADD CONSTRAINT fk_contests_images_uploaded_files
FOREIGN KEY (id_file)
REFERENCES uploaded_files(id_file);

ALTER TABLE users_avatars
ADD CONSTRAINT fk_users_avatars_uploaded_files
FOREIGN KEY (id_file)
REFERENCES uploaded_files(id_file);

ALTER TABLE users_avatars
ADD CONSTRAINT fk_users_avatars_users
FOREIGN KEY (id_user)
REFERENCES users(id_user);

ALTER TABLE files_to_unlink
ADD CONSTRAINT fk_files_to_unlink_uploaded_files
FOREIGN KEY (id_file)
REFERENCES uploaded_files(id_file);


INSERT INTO categories(name, link) VALUES ('Muzyka', 'music'), ('Sport', 'sport'), ('Edukacja', 'education'), ('Elektronika', 'electronics');
