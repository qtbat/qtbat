<?php

ini_set('display_errors', '1');
//define('DS', '/'); // directory separator

setlocale(LC_ALL, 'en_US.UTF8');

require_once 'vendor/autoload.php';

require_once 'predis_session.php';

session_start();

define('DIR', __DIR__.'/');
$config = new \Qtbat\Engine\Config();
define('PATH', $config->get('application', 'path'));
define('TWIG_CACHE', $config->get('application', 'twig_cache'));
define('APP_NAME', $config->get('application', 'name'));

try {
    $application = new \Qtbat\Engine\Application($config);
    $application->start();
} catch (\Qtbat\Exception\PageError $e) {
    die($e->getMessage());
} catch (Exception $e) {
    die($e->getMessage());
}
