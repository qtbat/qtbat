# contests

**contest_type** - typ konkursu
+ 1 - first_few
+ 2 - draw
+ 3 - select_winner
+ 4 - tournament

**creator_id** - id użytkownika, który stworzył dany konkurs

**link** - część linku do konkursu; postać linku (qtbat.com/login_autora/link)

**avaible_for** - dla kogo konkurs został udostępniony
+ 1 - dla grup
+ 2 - dla wszystkich

**create_date** - data utworzenia konkursu;

**start_date** - data wystartowania konkursu; data zmiany **status** na 1

**end_date** - data zakończenia konkursu; data zmiany **status** na 2

**title** - tytuł konkursu

**description** - opis konkursu

**id_category** - id kategorii z tabeli **categories**

**number_of_winners** - ilość osób, które mogą wygrać w danym konkursie

**prize_delivery** - w jaki sposób zostanie dostarczona nagroda do zwycięzcy konkursu
+ 1 - nagrodę wysyła autor poprzez kontakt z użytkownikiem
+ 2 - nagroda zostaje wysłana automatycznie; jej wartość pobierana jest z kolumny

**prize** - nagrody zapisane w postaci JSON'a, index tablicy + 1 odpowiada nagrodzie

**status** - etap na jakim znajduje się konkurs
+ 0 - konkurs nie rozpoczęty
+ 1 - konkurs rozpoczęty; trwa
+ 2 - konkurs zakończony

# contests_participants

**id_user** - id użytkownika, który bierze udział w danym konkursie

**id_contest** - id konkursu, w którym dany użytkownik bierze udział

**content** - zawartość zgłoszenia użytkownika np. tekst, informacja o tym, że zamieścił zdjęcie, filmik, pliki w postaci JSON'a

**position** - miejsce jakie użytkownik zajął w konkursie; NULL w przypadku gdy **status** jest 0
+ NULL - nie zajął żadnego miejsca - nie wygrał
+ 1-x - wygrał zajmując x miejsce
    * w przypadku losowania numer ten odpowiada nagrodzie, a nie miejscu w konkursie
+ w - czeka na zatwierdzenie wyników losowania przez autora (losowanie)
+ a - praca wyróżniona (select_winner)

**status** - determinuje to, czy użytkownik wygrał czy nie
+ 0 - nie wygrał - **position** NULL
+ 1 - wygrał, a jego nagrodę/pozycję determinuje kolumna **position**

# first_few

**question** - pytanie konkursowe, na które użytkownik musi poprawnie odpowiedzieć aby wygrać (odpowiedź musi zgadzać się z zawartością kolumny **answer**)

**answer** - odpowiedź na pytanie konkursowe

**answer_form** - forma odpowiedzi, która ma nakierować użytkownika, aby ten mógł poprawnie odpowiedzieć na pytanie (np. "Imię Nazwisko", aby uniknąć odpowiedzi "Nazwisko Imię")

# draw

**requirement** - wymaganie konkursowe w postaci JSON'a; użytkownik musi zamieścic zdjęcie/filmik/tekst/plik aby wziąć udział w konkursie; autor na podstawie zawartości tego będzie mógł odrzucić zgłoszenie i wylosować nowe (zawartość tego co powinno być w **requirement** znajduje się w kolumnie **content** w tabeli **contests_participants**)
+ NULL - brak wymagań; spowoduje to automatyczne zatwierdzenie losowania - brak możliwości wylosowania nowego zwycięzcy
+ 1 - zdjęcie
+ 2 - filmik (youtube)
+ 3 - tekst
+ 4 - plik

**method** - sposób, w jaki zostanie przeprowadzone losowanie
+ 1 - losowanie w określonej dacie
+ 2 - losowanie poprzez naciśnięcie guzika przez autora
+ 3 - losowanie, gdy weźmie udział X osób (liczba ta wtedy jest przedstawiona w kolumnie **number_of_applications**)

**date** - kolumna zawierająca datę, która przedstawia co innego w zależności od kolumny **status** w tabeli **draw**
+ status 0 - data, w której zostanie przeprowadzone losowanie
+ status 1 - data, w której zostaną potwierdzone wyniki losowania; do tego czasu autor ma prawo wylosować nowych zwycięzców

**status** - etap w jakim znajduje się konkurs
+ 0 - ludzie mogą zgłaszać się do losowania
+ 1 - oczekiwanie na zatwierdzenie zwycięzców przez autora
+ 2 - losowanie zostało zakończone

**number_of_applications** - w przypadku gdy **method** = 3 ilość osób która musi zostać osiągnięta, aby zostało przeprowadzone losowanie; w innym przypadku NULL

# select_winner

**requirements** - wymagania konkursowe; jakiej postaci zgłoszenia użytkownik może wysłać; JSON zawierający informacje o rozszerzeniach plików, wymiarach zdjęć itd.

**limit_submission_date** - determinuje to, czy autor podczas startowania konkursu będzie podawał **submission_date**

**submission_date** - data, do której możliwe jest zgłaszanie się do konkursu

**show_winners** - wartość, którą autor podaje podczas zatwierdzania zwycięzców; determinuje to, czy zwycięzcy będą widoczni w liście zwycięzców konkursu
+ 0 - nie będą widoczni
+ 1 - będą widoczni

# select_winner_applications
tabela zawierająca rozszerzenie informacji o zgłoszeniach konkursowych

**id_application** - powiązanie z **contests_participants**

**highlight** - oznaczenie zgłoszenia
+ 1 - pierwsze miejsce
+ 2 - drugie miejsce
+ 3 - trzecie miejsce
+ 4 - czwarte miejsce
+ p - pozytywne
+ n - negative
+ a - wyróżniona praca
+ s - zacznij ładować zgłoszenia od tego miejsca

**order_number** - liczba oznaczająca kolejność, która umożliwia układanie zgłoszeń w odpowiednim porządku

# users

**login** - nazwa użytkownika

**email** - email powiązany z kontem, na który przychodzi np. aktywacja

**password** - hasło do konta

**name** - np. imię nazwisko wyświetlane w danych profilu

**location** - miejsce zamieszkania wyświetlane w danych profilu

**status** - status konta
+ 0 - nieaktywowane konto
+ 1 - aktywowane konto

**user_group** - ranga konta
+ 1 - zwykły użytkownik

**register_date** - data rejestracji

**register_ip** - numer IP, z którego użytkownik się zarejestrował

**last_date** - ostatnia data, w której użytkownik był aktywny

**last_ip** - ostatni numer IP, z którego użytkownik się logował

**activation_hash** - hash do linku, w który użytkownik musi wejść, aby aktywować swoje konto

**change_email** - hash do linku, w który użytkownik musi wejść, aby zmienić swój email

**remind_password_hash** - hash do linku, w który użytkownik musi wejść, aby przypomnieć swoje hasło

**contact_email** - email kontaktowy wyświetlany w profilu

**sex** - płeć, wyświetlana w profilu
+ 0 - nieokreślona
+ m - mężczyzna
+ f - kobieta

# notifications

**id_receiver** - id użytkownika, do którego wysłane zostało dane powiadomienie

**type** - typ powiadomienia
+ 0 - przegrana w konkursie, w którym wzięło się udział
+ 1 - wygrana w konkursie, w którym wzięło się udział
+ 2 - konkurs został zakończony (powiadomienie dla autora konkursu)
+ 3 - losowanie zostało zakończone (powiadomienie dla autora konkursu)
+ 4 - jakiś użytkownik udzielił poprawnej odpowiedzi w konkursie **contest_type 1** (first_few) (powiadomienie dla autora konkursu)
+ 5 - losowanie zostało zatwierdzone
+ 6 - zaproszenie do grupy

**status** - informacja o tym, czy powiadomienie zostało odczytane, czy nie
+ 0 - powiadomienie odczytane
+ 1 - powiadomienie nieodczytane

**date** - data, w której powiadomienie zostało wysłane do użytkownika

**hash** - wartość NULL; po to aby pozyskać **id_notification** z tabeli **notifications** i umieścić je w tabeli pośredniej (**contests_notifications** lub **groups_notifications**)

# groups

**name** - nazwa grupy, którą można modyfikować (tytuł)

**unique_name** - unikalna nazwa grupy wyświetlana w linku (tag)

**description** - opis grupy

**type** - typ grupy
+ 0 - grupa otwarta; każdy może do niej dołączyć i ją oglądać
+ 1 - grupa zamknięta, nie można do niej dołącyzć ani jej oglądać; administrator grupy wciąż może do niej zapraszać

**create_date** - data założenia grupy

# users_groups

**id_group** - id grupy, z którą powiązany jest rekord

**rank** - ranga członka grupy
+ 0 - zwykły członek grupy
+ 1 - administrator grupy (możliwość modyfikacji, usuwania i zapraszania użytkowników, usuwania grupy, przypinania konkursów)

**joined_date** - data, w której użytkownik dołączył do danej grupy

**member_status** - status członka grupy
+ 0 - zaproszony do grupy
+ 1 - pełnoprawny członek grupy

# categories

**name** - nazwa kategorii

**link** - link przekierowywujący do danej kategorii

# uploaded_files

**original_name** - oryginalna nazwa pliku

**name** - hash pliku

**disk** - nazwa dysku, w którym plik się znajduje

**folder** - nazwa folderu, w którym plik się znajduje

# files_to_unlink
informacje o plikach, które są ustawione w kolejce do usuwania Cronem

**unlink** - determinuje, czy plik jest gotowy do usunięcia
+ true - zostanie usunięty przy następnej aktywacji Crona
+ false - nie zostanie usunięty przy następnej aktywacji Crona

# contests_images

**type** - typ zdjęcia
+ 0 - zdjęcie konkursowe
+ 1 - banner

# contests_groups

**attached** - informacja o tym, czy konkurs jest podpięty (widoczny na szczycie listy w grupie)
+ true - jest podpięty
+ false - nie jest podpięty

# users_opinions

**from_user** - użytkownik, który wystawił opinię

**to_user** - użytkownik, którego ta opinia dotyczy

**rate** - ocena
+ 1 - pozytywna
+ 0 - negatywna

**comment** - komentarz do oceny (nie wymagany)

**id_contest** - id konkursu, z jakim związana jest dana opinia

**date** - data wystawienia opinii

# private_messages

**sender_id** - id użytkownika, który wysłał wiadomość

**sender_removed** - determinuje to, czy użytkownik, który wysłał wiadomość usunął ją ze swojej listy

**recipient_id** - id użytkownika, do którego została wysłana wiadomość

**recipient_removed** - determinuje to, czy użytkownik, do którego wiadomość była wysłana została usunięta z jego listy

**topic** - tytuł wiadomości

**content** - treść wiadomości

**send_date** - data wysłania wiadomości

**status** - status wiadomości
+ 0 - przeczytana
+ 1 - nieprzeczytana
