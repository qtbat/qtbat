var express = require('express'),
    app = express(),
    nconf = require('nconf');

nconf.argv().env();
nconf.file({ file: 'config.json' });

app.use(function(req, res){
    res.statusCode = 302;
    res.setHeader('Location', 'http://localhost/404'); //TODO przekierowanie na 404
    res.end();
});

var server = app.listen(nconf.get('port')),
    io = require('socket.io').listen(server),
    pg = require('pg'),
    redis = require('redis');

var client= new redis.createClient(nconf.get('redis:port'), nconf.get('redis:host'));
client.auth(nconf.get('redis:auth'));

var conString = nconf.get('postgresql-conString');
require('events').EventEmitter.prototype._maxListeners = 1000000;
//io.sockets.setMaxListeners(0);

function parse_cookies(_cookies) {
    var cookies = {};

    _cookies && _cookies.split(';').forEach(function(cookie) {
        var parts = cookie.split('=');
        cookies[parts[0].trim()] = (parts[1] || '').trim();
    });

    return cookies;
}

var pg_client = new pg.Client(conString);
pg_client.connect();

pg_client.query("LISTEN notifications");

io.on('connection', function (socket) {
    var cookies = parse_cookies(socket.handshake.headers.cookie);
    var phpsessid = decodeURIComponent(cookies.PHPSESSID);

    if (phpsessid!=null) {
        client.get("sessions/"+phpsessid, function (error, result) {
            if (result) {
                result = JSON.parse(result);
                socket.user_id = result.user_id;

                pg_client.on('notification', function(msg) {
                    var response = msg.payload;
                    var split = response.split(":");

                    if (split[1]==socket.user_id) {
                        if (split[0]=='cg') {
                            pg_client.query('SELECT COUNT(id_notification) as count \
                                FROM notifications \
                                WHERE id_receiver=' + socket.user_id + ' AND status=1', function(query_error, result) {

                                var count = parseInt(result.rows[0].count);
                                io.to(socket.id).emit('cg_notify', count);
                            });
                        } else if (split[0]=='pm') {
                            pg_client.query('SELECT COUNT(id_message) as count \
                                FROM private_messages  \
                                WHERE status=1 AND recipient_id='+socket.user_id, function(query_error, result) {

                                var count = parseInt(result.rows[0].count);
                                io.to(socket.id).emit('pm_notify', count);
                            });
                        }
                    }
                });
            }
        });
    }
});
