var nowShow = 0;

function show_message() {
    var message = $(this);
    if (nowShow != message.data('id')) {
        $.ajax({
            url: '/messages/' + message.data('id'),
            type: 'POST',
            dataType: 'json',
            success: function(result) {
                if (result != '') {
                    nowShow = message.data('id');
                    $('#showSender').html(result['sender']);
                    $('#showDate').html(result['send_date']);
                    $('#showTopic').html(result['topic']);
                    $('#showContent').html(result['content']);
                }
            }
        });
    }
    $('#showMessage').css('display', 'block');
}

function delete_message() {
    var message = $(this);
    $.ajax({
        url: '/messages/delete',
        type: 'POST',
        data: {
            'message': message.data('id')
        },
        dataType: 'json',
        success: function(result) {
            if (result['deleted'] > 0) {
                message.parent().parent().remove();
                if (nowShow == result['deleted']) {
                    $('#showMessage').css('display', 'none');
                }
            }
        }
    });
}

$('.show-message').click(show_message);

$('.delete-message').click(delete_message);

$('#hideMessage').click(function() {
    $('#showMessage').css('display', 'none');
});

$('#replyMessage').click(function() {
    $('#replyTopic').val($('#showTopic').text());
    $('#reply').attr('action', '/messages/'+$('#showSender').text()+'/new');
    $('#reply').submit();
});
