var loadGroupContests = (function(){
    var $list = $('.contests_list');

    var timeout,
        start = 0,
        updateStart = 0,
        stop = false,
        currentPage = 1;

    $(window).scroll(scrollLoad);

    function loadData() {
        $.ajax ({
            type: "POST",
            url: path+"groups/"+group_name+"/load-contests-list",
            dataType: "json",
            data: {
                start: start,
                href: href,
                current_page: currentPage
            },
            beforeSend: function() {
                $list.append('<div class="loading">Loading...</div>');
            },
            success: function(result){
                if (!stop) {
                    updateStart = result['start'];
                    stop = result['stop'];
                    $('.contests_list .loading').remove();
                    $list.append(result['contests_list'] + '<hr>');
                    attachDetach();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    };

    function scrollLoad() {
        clearTimeout(timeout);
        timeout = setTimeout(function(){
            if ($(window).scrollTop()+window.innerHeight == $(document).innerHeight()) {
                if (!stop) {
                    currentPage++;
                    start = updateStart;
                    loadData();
                }
            }
        }, 100);
    }

    loadData();
})();
