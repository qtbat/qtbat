var attachDetach = (function(){
    //cacheDom
    var $attach = $('.attach_contest'),
        $detach = $('.detach_contest');

    //bindEvents
    $attach.on('click', attach);
    $detach.on('click', detach);

    function attach() {
        var id = $(this).parent().data('id');

        $.ajax({
            type: "POST",
            url: path+"groups/"+group_name+"/edit",
            dataType: "json",
            data: {
                attach_contest: id
            },
            success: function(result) {
                if (result['status']=='error') {
                    events.emit("operationError", result['message']);
                } else {
                    location.reload();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
    function detach() {
        var id = $(this).parent().data('id');

        $.ajax({
            type: "POST",
            url: path+"groups/"+group_name+"/edit",
            dataType: "json",
            data: {
                detach_contest: id
            },
            success: function(result) {
                if (result['status']=='error') {
                    events.emit("operationError", result['message']);
                } else {
                    location.reload();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
});
