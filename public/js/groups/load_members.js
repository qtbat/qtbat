var loadMembers = (function(){
    var $membersList = $('.members_list');

    var timeout, start, updateStart

    $(window).scroll(_scrollLoad);

    function loadData(start, searchValue, clear) {
        $.ajax ({
            type: "POST",
            url: path+"groups/"+group_name+"/load-members",
            dataType: "json",
            data: {
                start: start,
                search_value: searchValue
            },
            beforeSend: function() {
                $membersList.append('<div class="loading">Loading...</div>');
            },
            success: function(result){
                updateStart = result['start'];
                stop = result['stop'];
                $('.members_list .loading').remove();
                if (clear) {
                    $membersList.html('');
                    $membersList.append(result['members_list']);
                }
                else {
                    $membersList.append(result['members_list']);
                }
                membersList();
                editMember();
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
    function _scrollLoad() {
        clearTimeout(timeout);
        timeout = setTimeout(function(){
            if ($(window).scrollTop()+window.innerHeight == $(document).innerHeight()) {
                if (!stop) {
                    start = updateStart;
                    loadData(start, searchValue, false);
                }
            }
        }, 100);
    }
    return {
        loadData: loadData
    }
})();

var searchMember = (function(){
    //cacheDom
    var $input = $('.search_member_input');

    var timeout;

    //bindEvents
    $input.on('keyup', execute);

    function execute() {
        clearTimeout(timeout);
        timeout = setTimeout(function(){
            searchValue = $input.val();
            loadMembers.loadData(0, searchValue, true);
        }, 400);
    }
})();
