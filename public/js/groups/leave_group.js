var leaveGroup = (function(){
    //cacheDom
    var $button = $('.leave_group');

    //bindEvents
    $button.click(displayConfirmBox);
    events.on("leaveGroup", accept);

    function displayConfirmBox() {
        var message = 'Czy na pewno chcesz opuścić tę grupę?';
        var clear_buttons = null;

        if (count_group_members == 1) {
            message = 'UWAGA! Jeśli odejdziesz z tej grupy, zostanie ona usunięta! Czy na pewno chcesz to zrobić?';
        }
        events.emit("openConfirmBox", {
            message: message,
            performer: "leaveGroup"});
    }

    function accept() {
        $.ajax ({
            type: "POST",
            url: path+"groups/"+group_name+"/user-interaction",
            data: {
                leave: true
            },
            dataType: "json",
            success: function(result){
                if (result['status']=='error') {
                    events.emit("confirmBoxError", result['message']);
                } else {
                    window.location.replace(path);
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
})();
