var membersList = (function(){
    //cacheDom
    var $switchContentButton = $('.member_info_button');

    //bindEvents
    $switchContentButton.on('click', switchContent);

    function switchContent() {
        var $content=$(this).parent().children('.member_content');
        var $details=$(this).parent().children('.member_details');
        if ($content.css('display')=='block') {
            $content.fadeOut("slow").promise().done(function(){
                $details.fadeIn("slow");
            });
        } else {
            $details.fadeOut("slow").promise().done(function(){
                $content.fadeIn("slow");
            });
        }
    }
});
