var addMember = (function(){
    //cacheDom
    var $form = $('.add_member'),
        $login = $form.find("input[name='login']"),
        $message = $('.add_member_message');

    //bindEvents
    $form.on('submit', execute);

    function execute() {
        $.ajax({
            type: "POST",
            url: path+"groups/"+group_name+"/edit",
            dataType: "json",
            data: {
                new_member_login: $login.val()
            },
            success: function(result) {
                if (result['status']=='error') {
                    $message.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    $message.html(result['message']);
                }
                else {
                    $message.css({
                        'color' : 'green',
                        'font-weight' : 'bold'
                    });
                    $message.html('Zaproszenie zostało wysłane.');
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });

        return false;
    }
})();
