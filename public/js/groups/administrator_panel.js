var administratorPanel = (function(){
    //cacheDom
    var $nameForm = $('.name_form'),
        $nameInput = $nameForm.find('input[name="name"]'),
        $nameError = $('.change_name_error_container'),

        $descriptionForm = $('.description_form'),
        $descriptionContent = $descriptionForm.find('textarea[name="description"]'),

        $typeForm = $('.type_form'),
        $typeError = $('.type_error_container');

    //bindEvents
    $nameForm.on('submit', changeName);
    $descriptionForm.on('submit', changeDescription);
    $typeForm.on('submit', changeType);

    function changeName() {
        $.ajax({
            type: "POST",
            url: path+"groups/"+group_name+"/edit",
            dataType: "json",
            data: {
                group_name: $nameInput.val()
            },
            success: function(result) {
                if (result['status']=='error') {
                    $nameError.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    $nameError.html(result['message']);
                }
                else {
                    location.reload();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
        return false;
    }
    function changeDescription() {
        $.ajax({
            type: "POST",
            url: path+"groups/"+group_name+"/edit",
            data: {
                description: $descriptionContent.val()
            },
            success: function() {
                location.reload();
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
        return false;
    }
    function changeType() {
        var $typeInput = $(this).find("input[name='type']:checked");

        $.ajax({
            type: "POST",
            url: path+"groups/"+group_name+"/edit",
            dataType: 'json',
            data: {
                type: $typeInput.val()
            },
            success: function(result) {
                if (result['status']=='error') {
                    $typeError.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    $typeError.html(result['message']);
                }
                else {
                    location.reload();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
        return false;
    }

})();

var deleteGroup = (function(){
    //cacheDom
    $button = $('.delete_group');

    //bindEvents
    $button.click(displayConfirmBox);
    events.on("deleteGroup", accept);

    function displayConfirmBox() {
        events.emit("openConfirmBox", {
            message: "Czy na pewno chcesz usunąć tą grupę? Będzie to nieodwracalne.",
            performer: "deleteGroup"});
    }
    function accept() {
        $.ajax ({
            type: "POST",
            url: path+"groups/"+group_name+"/edit",
            data: {
                delete_group: true
            },
            dataType: "json",
            success: function(result){
                if(result['status']=='error') {
                    events.emit("confirmBoxError", result['message']);
                } else {
                    window.location.replace(path);
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
})();
