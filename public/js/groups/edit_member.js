var editMember = (function(){
    //cacheDom
    var $deleteMember = $('.delete_member_button'),
        $editRank = $('.edit_member_form');

    var $element = null;

    //bindEvents
    $editRank.on('submit', editRank);
    $deleteMember.on('click', displayConfirmBoxDelete);
    events.on('deleteGroupMember', acceptDeleteMember);

    function editRank() {
        var id = $(this).parent().parent().data('id'),
            value = $(this).find("input[name='rank']:checked").val();

        $.ajax({
            type: "POST",
            url: path+"groups/"+group_name+"/edit",
            data: {
                rank: value,
                id_user: id
            },
            success: function() {
                location.reload();
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
        return false;
    }
    function displayConfirmBoxDelete() {
        $element = $(this);
        events.emit("openConfirmBox", {
            message: "Czy na pewno chcesz usunąć tego użytkownika z grupy?",
            performer: "deleteGroupMember"});
    }
    function acceptDeleteMember() {
        var id = $element.parent().parent().data('id');
        $.ajax ({
            type: "POST",
            url: path+"groups/"+group_name+"/edit",
            data: {
                id_delete: id
            },
            dataType: "json",
            success: function(result){
                if (result['status']=='error') {
                    events.emit("confirmBoxError", result['message']);
                } else {
                    $('.confirm_box').css('display', 'none');
                    $element.parent().parent().fadeOut("slow");
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
});
