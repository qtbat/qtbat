var joinGroup = (function(){
    //cacheDom
    var $button = $('.join_group');

    //bindEvents
    $button.click(displayConfirmBox);
    events.on("joinGroup", accept);

    function displayConfirmBox() {
        events.emit("openConfirmBox", {
            message: "Czy na pewno chcesz dołączyć do tej grupy?",
            performer: "joinGroup"});
    }

    function accept() {
        $.ajax ({
            type: "POST",
            url: path+"groups/"+group_name+"/user-interaction",
            data: {
                join: true
            },
            dataType: "json",
            success: function(result){
                if(result['status']=='error') {
                    events.emit("confirmBoxError", result['message']);
                } else {
                    location.reload();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
})();
