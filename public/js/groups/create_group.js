var createGroup = (function(){
	//cacheDom
	var $form = $('.create_group_form'),
		$uniqueName = $form.find("input[name='unique_name']"),
		$name = $form.find("input[name='name']"),
		$description = $form.find("textarea[name='description']"),
		$errorContainer = $('.error_container');

	//bindEvents
	$form.on('submit', sendForm);

	function sendForm() {
		var $type = $(this).find("input[name='type']:checked");
		$.ajax ({
			type: "POST",
			url: path+"groups/create-group/send",
			dataType: "json",
			data: {
				unique_name: $uniqueName.val(),
				name: $name.val(),
				description: $description.val(),
				type: $type.val()
			},
			success: function(result){
				if (result['status']=='success') {
					location.replace(path+'groups/'+$uniqueName.val());
				} else {
					$errorContainer.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
					$errorContainer.html(result['message']);
				}
			},
			complete: function(xhr, data) {
				if (xhr.status == 404) {
					events.emit("operationError");
				}
			}
		});

		return false;
	}
})();
