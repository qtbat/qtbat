var createContestForm = (function(){
    var $form = $('.contest_form'),
        $formError = $('.contest_form_error');

    $form.on('submit', function(){
        var formData = new FormData($(this)[0]);

        var other_data = $(this).serializeArray();

        $.each(other_data,function(key, input){
            if (input.name=='select_groups') {
                var groups = [];
                $('.groups_selection :selected').each(function(i, selected){
                    groups[i] = $(selected).val();
                });
                formData.append(input.name, JSON.stringify(groups));
            } else if (input.name=='prize[]') {

            } else if (input.name=='requirements[]') {

            } else {
                formData.append(input.name, input.value);
            }
        });

        $.ajax({
            url: path+"create-contest/send-form",
            dataType: "json",
            data: formData,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(result){
                if(result['status']=='success') {
                    location.replace(path+login+'/'+result['link']);
                } else if(result['status']=='error') {
                    $formError.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    $formError.html(result['message']);
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });

        return false;
    });
})();
