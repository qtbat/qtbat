var drawEvents = (function(){
    //cacheDom
    var $drawMethod1 = $('.pick_date'),
        $drawMethod2 = $('.button_click'),
        $drawMethod3 = $('.number_of_applications');

    //bindEvents
    $drawMethod3.click(displayNumberInput);
    $drawMethod1.click(hideNumberInput);
    $drawMethod2.click(hideNumberInput);

    function displayNumberInput() {
        $('.input_number_of_applications').fadeIn("slow");
    }
    function hideNumberInput() {
        $('.input_number_of_applications').hide();
    }
});

var selectWinnerEvents = (function(){
    //cacheDom
    var $pictureRequirement = $('.picture_requirement'),
        $picturesSettings = $('.pictures_settings'),
        $customPictureDimensions = $('.custom_picture_dimensions'),
        $defaultPictureDimensions = $('.default_picture_dimensions'),
        $customPictureDimensionsContainer = $('.custom_picture_dimensions_container'),
        $fileRequirement = $('.file_requirement'),
        $filesSettings = $('.files_settings'),
        $customExtensions = $('.allow_custom_extensions'),
        $customExtensionsContainer = $('.custom_file_extensions'),
        $everyExtension = $('.allow_every_extension');

    var pictureRequirementSet = false,
        fileRequirementSet = false;

    $pictureRequirement.click(pictureRequirementDetails);
    $customPictureDimensions.click(function(){
        $customPictureDimensionsContainer.fadeIn();
    });
    $defaultPictureDimensions.click(function(){
        $customPictureDimensionsContainer.hide();
    });
    $fileRequirement.click(fileRequirementDetails);
    $customExtensions.click(function(){
        $customExtensionsContainer.fadeIn();
    });
    $everyExtension.click(function(){
        $customExtensionsContainer.hide();
    });

    function pictureRequirementDetails() {
        if (pictureRequirementSet === false) {
            pictureRequirementSet = true;
        } else if (pictureRequirementSet === true) {
            pictureRequirementSet = false;
        }
        if (pictureRequirementSet === true) {
            $picturesSettings.fadeIn();
        } else if (pictureRequirementSet === false) {
            $picturesSettings.hide();
        }
    }
    function fileRequirementDetails() {
        if (fileRequirementSet === false) {
            fileRequirementSet = true;
        } else if (fileRequirementSet === true) {
            fileRequirementSet = false;
        }
        if (fileRequirementSet === true) {
            $filesSettings.fadeIn();
        } else if (fileRequirementSet === false) {
            $filesSettings.hide();
        }
    }
});
