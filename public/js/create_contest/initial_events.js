var createContestDisplay = (function(){
    //cacheDom
    var $avaibleForGroups = $('.avaible_for_groups'),
        $avaibleForEveryone = $('.avaible_as_normal'),
        $groupsSelection = $('.select_groups'),
        $groupsSelectionOption = $('.groups_selection option'),
        $groupsList = $('.groups_list'),
        $typeSelection = $('.type_selection');

    //bindEvents
    $avaibleForGroups.click(displayGroupsSelection);
    $avaibleForEveryone.click(avaibleForEveryone);
    $groupsSelectionOption.mousedown(multipleGroupsSelection);
    $groupsList.click(displayTypeSelection);

    function displayGroupsSelection() {
        $groupsSelection.fadeIn("slow");
    }
    function avaibleForEveryone() {
        $groupsSelection.hide();
        displayTypeSelection();
    }
    function displayTypeSelection() {
        $typeSelection.fadeIn("slow");
    }
    function multipleGroupsSelection(e) {
        e.preventDefault();
        $(this).prop('selected', $(this).prop('selected') ? false : true);
        return false;
    }
})();

var everyTypeEvents = (function(){
    //cacheDom
    var $automaticDelivery = $('.automatic_delivery'),
        $deliveryByAuthor = $('.delivery_by_author'),
        $prizesContainer = $('.prize');

    $automaticDelivery.click(function(){
        $prizesContainer.fadeIn("slow");
    });
    $deliveryByAuthor.click(function(){
        $prizesContainer.hide();
    });
});

var createContestPrizes = (function(data){
    //cacheDom
    var $prizesContainer = $('.prizes'),
        $numberOfWinnersInput = $('.number_of_winners'),
        $prizesMessage = $('.prize_statement'),
        $automaticDelivery = $('.automatic_delivery'),
        $refreshPrizes = $('.refresh_prizes');

    if (data.contestType == 'first_few') {
        data.maxNumberOfWinners = 5;
    } else if (data.contestType == 'draw') {
        data.maxNumberOfWinners = 4;
    } else if (data.contestType == 'select_winner') {
        data.maxNumberOfWinners = 4;
    }

    //bindEvents
    $automaticDelivery.click(displayPrizes);
    $refreshPrizes.click(displayPrizes);

    function displayPrizes() {
        $prizesContainer.text('');
        var numberOfWinners = parseInt($numberOfWinnersInput.val());

        if (parseInt(numberOfWinners)) {
            if (numberOfWinners>data.maxNumberOfWinners) {
                $prizesMessage.hide();
                $prizesContainer.css({
                    'color' : 'red'
                });
                $prizesContainer.text('Maksymalna ilość zwycięzców wynosi ' + data.maxNumberOfWinners + '.');
            } else {
                if (data.contestType=='first_few' && numberOfWinners<2) {
                    $prizesContainer.text('Minimalna ilośc zwycięzców wynosi 2. Jeśli chcesz udostępnić nagrodę dla jednego zwycięzcy, skorzystaj z "Kto pierwszy ten lepszy"');
                } else if (numberOfWinners<1) {
                    $prizesMessage.hide();
                    $prizesContainer.css({
            			'color' : 'red'
            		});
                    $prizesContainer.text('Minimalna ilośc zwycięzców wynosi 1.');
                } else {
                    $prizesMessage.show();
                    $prizesContainer.css({
                        'color' : 'black'
                    });

                    if (data.contestType=='draw') {
                        for (var i=1; i<=numberOfWinners; i++) {
                            $prizesContainer.append('<input type="text" name="prize[]" placeholder="Nagroda"/><br />');
                        }
                    } else {
                        for (var i=1; i<=numberOfWinners; i++) {
                            $prizesContainer.append(i+' miejsce <input type="text" name="prize[]" placeholder="Nagroda"/><br />');
                        }
                    }
                }
            }
        } else {
            $prizesMessage.hide();
            $prizesContainer.css({
                'color' : 'red'
            });
            $prizesContainer.text('Ilość zwycięzców musi być przedstawiona jako liczba całkowita.');
        }
    }
});

var createContestDisplayTypes = (function(){
    //cacheDom
    var $formBasicData = $('.contest_form_basic_data'),
        $formData = $('.contest_form_data'),
        $formEndData = $('.contest_form_end_data'),
        $firstComeFirstServed = $('.first_come_first_served'),
        $firstFew = $('.first_few'),
        $draw = $('.draw'),
        $selectWinner = $('.select_winner'),
        $tournament = $('.tournament');

    var currentType;

    //bindEvents
    $firstComeFirstServed.click(loadFirstComeFirstServed);
    $firstFew.click(loadFirstFew);
    $draw.click(loadDraw);
    $selectWinner.click(loadSelectWinner);
    $tournament.click(loadTournament);


    function preLoad(htmlResult) {
        $formBasicData.hide();
        $formEndData.hide();
        $formBasicData.fadeIn("slow");
        $formData.hide().html(htmlResult).fadeIn("slow");
        $formEndData.fadeIn("slow");
    }
    function loadFirstComeFirstServed() {
        if (currentType!="first_come_first_served") {
            $.ajax ({
                type: "POST",
                url: path+"create-contest/append",
                data: {
                    first_come_first_served: true
                },
                success: function(result){
                    preLoad(result);
                    everyTypeEvents();
                    currentType="first_come_first_served";
                }
            });
        }
    }
    function loadFirstFew() {
        if (currentType!="first_few") {
            $.ajax ({
                type: "POST",
                url: path+"create-contest/append",
                data: {
                    first_few: true
                },
                success: function(result){
                    preLoad(result);
                    everyTypeEvents();
                    currentType="first_few";
                    createContestPrizes({
                        contestType: currentType
                    });
                }
            });
        }
    }
    function loadDraw() {
        if (currentType!="draw") {
            $.ajax ({
                type: "POST",
                url: path+"create-contest/append",
                data: {
                    draw: true
                },
                success: function(result){
                    preLoad(result);
                    everyTypeEvents();
                    currentType = "draw";
                    createContestPrizes({
                        contestType: currentType
                    });
                    drawEvents();
                }
            });
        }
    }
    function loadSelectWinner() {
        if (currentType!="select_winner") {
            $.ajax ({
                type: "POST",
                url: path+"create-contest/append",
                data: {
                    select_winner: true
                },
                success: function(result){
                    preLoad(result);
                    everyTypeEvents();
                    currentType="select_winner";
                    createContestPrizes({
                        contestType: currentType
                    });
                    selectWinnerEvents();
                }
            });
        }
    }
    function loadTournament() {
        if( currentType!="tournament") {
            $.ajax ({
                type: "POST",
                url: path+"create-contest/append",
                data: {
                    tournament: true
                },
                success: function(result){
                    preLoad();
                    currentType="tournament";
                }
            });
        }
    }
})();
