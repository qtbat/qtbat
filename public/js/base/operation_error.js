var operationError = (function(){
    //cacheDom
    var $container = $('.operation_error'),
        $close = $('.close_operation_error'),
        $message = $('.operation_error .message');

    //bindEvents
    events.on("operationError", popoutError);
    $close.click(close);

    function close() {
        $container.fadeOut("slow");
    }

    function popoutError(message) {
        $container.hide(); //to make it dissapear while other error is visible
        $message.html(message);
        $container.fadeIn("slow");
        setTimeout(function(){
            $container.fadeOut("slow");
        }, 12000);
    }
})();
