var groupsInvitations = (function(){
    //cacheDom
    var $confirm = $('.account .cg_notifications_content .confirm'),
        $reject = $('.account .cg_notifications_content .reject');

    //bindEvents
    $confirm.click(accept);
    $reject.click(reject);

    function accept() {
        var element = $(this);
        var idAccept = $(this).closest('.notification_wrapper').data('id');

        $.ajax ({
            type: "POST",
            dataType: "json",
            url: path+"invitation-confirm",
            data: {
                id_accept: idAccept
            },
            success: function(result){
                if (result['status']=='error') {
                    element.parent().css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    element.parent().html(result['message']);
                } else {
                    element.closest('.notification_wrapper').fadeOut();
                    element.closest('.notification_wrappers').detach();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
    function reject() {
        var element = $(this);
        var idReject = $(this).closest('.notification_wrapper').data('id');

        $.ajax ({
            type: "POST",
            url: path+"invitation-confirm",
            dataType: "json",
            data: {
                id_reject: idReject
            },
            success: function(){
                if (result['status']=='error') {
                    element.parent().css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    element.parent().html(result['message']);
                } else {
                    element.closest('.notification_wrapper').fadeOut();
                    element.closest('.notification_wrappers').detach();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
});
