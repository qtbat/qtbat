var socket = io.connect('http://localhost:3000');

var notifications = (function(){
    var $notificationsIcon = $('.account .cg_notifications'),
        $container = $('.account .cg_notifications_container'),
        $content = $('.account .cg_notifications_content'),
        $loadButton = $('.account .load_notifications'),
        $notificationsCount = $('.account .cg_notifications'),
        $pmCount = $('.account .pm_notifcations'),
        $newNotificationMessage = $('.account .new_notification_message'),
        $refresh = $('.account .new_notification_message .refresh_notifications');

    var visible = false,
        start = 0,
        page = 1,
        updateStart = 0,
        stop = false;

    socket.on('cg_notify', function(data){
        $notificationsCount.html(data);
        if (visible) {
            $newNotificationMessage.show();
        }
    });
    socket.on('pm_notify', function(data){
        $pmCount.html(data);
    });
    $refresh.click(function(){
        reset();
        loadNotifications();
    });
    $notificationsIcon.click(displayContainer);
    $loadButton.click(loadMore);

    function displayContainer() {
        $.ajax ({
            type: "POST",
            url: path+"read-notifications",
            data: {
                read_notifications: true
            },
            success: function(){
                $notificationsCount.html('0');
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
        if (visible===false) {
            visible = true;
            $container.show();
            loadNotifications();
        } else {
            visible = false;
            reset();
            $container.hide();
        }
    }

    function reset() {
        start = 0;
        page = 1;
        updateStart = 0;
        stop = false;
        $content.html('');
        $newNotificationMessage.hide();
    }
    function loadNotifications() {
        $.ajax ({
            type: "POST",
            url: path+"load-notifications",
            dataType: "json",
            data: {
                start: start,
                page: page
            },
            beforeSend: function() {
                $content.append('<div class="loading">Loading...</div>');
            },
            success: function(result){
                if (!stop) {
                    updateStart = result['start'];
                    stop = result['stop'];
                    if (!stop) {
                        $loadButton.show();
                    }
                    $('.account .cg_notifications_content .loading').remove();
                    $content.append(result['notifications_list']);
                    groupsInvitations();
                    if (stop) {
                        $loadButton.hide();
                    }
                } else {
                    $loadButton.hide();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
    function loadMore() {
        if (!stop) {
            page++;
            start = updateStart;
            loadNotifications();
        }
    }
})();
