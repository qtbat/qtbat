var loginRegisterForms = (function(){
    //cacheDom
    var $login = $('.login'),
        $register = $('.account .register'),
        $container = $('.login_register_container'),
        $loginForm = $('.login_register_container .login'),
        $registerForm = $('.login_register_container .register'),
        $containerBackground = $('.login_register_container .translucent_background'),
        $forms = $('.login_register_container form'),
        $closeWindow = $('.login_register_container .close_window');

    //bindEvents
    $login.click(expandLogin);
    $containerBackground.click(hide)
        .children().click(function(e) {
            e.stopPropagation();
        });
    $closeWindow.click(hide);
    $register.click(expandRegister);

    function expandLogin() {
        $container.show();
        $loginForm.show();
    }
    function expandRegister() {
        $container.show();
        $registerForm.show();
    }
    function hide() {
        $container.hide();
        $loginForm.hide();
        $registerForm.hide();
        $forms[1].reset();
    }

    return {
        expandLogin: expandLogin,
        expandRegister: expandRegister
    }
})();

var loginRegister = (function(){
    //cacheDom
    var $register = $('.register_form'),
        $registerMessage = $('.register_message'),
        $login = $('.login_register_container .login_form'),
        $loginError = $('.login_register_container .login_error');

    //bindEvents
    $register.on('submit', register);
    $login.on('submit', login);

    function register() {
        var formData = new FormData();
        var registerData = $($register[0]).serializeArray();

        $.each(registerData, function(key, input){
            formData.append(input.name, input.value);
        });

        $.ajax({
            url: path+"register",
            type: 'POST',
            dataType: "json",
            data: formData,
            processData: false,
            contentType: false,
            success: function(result) {
                if (result['status']=='error') {
                    $registerMessage.css('color', 'red');
                } else {
                    $registerMessage.css('color', 'green');
                }
                $registerMessage.html(result['message']);
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });

        return false;
    }
    function login() {
        var formData = new FormData();
        var loginData = $($login[0]).serializeArray();

        $.each(loginData, function(key, input){
            formData.append(input.name, input.value);
        });

        $.ajax({
            url: path+"login",
            type: 'POST',
            dataType: "json",
            data: formData,
            processData: false,
            contentType: false,
            success: function(result) {
                if (result['status']=='error') {
                    $loginError.css('color', 'red');
                    $loginError.html(result['message']);
                }
                else {
                    if (typeof(redirect)!='undefined') {
                        if (redirect=='index') {
                            location.replace(path);
                        }
                    } else {
                        location.reload();
                    }
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });

        return false;
    }
})();
