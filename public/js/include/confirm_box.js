var confirmBox = (function(){
	//cacheDom
	var $container = $('.confirm_box'),
		$containerBackground = $('.confirm_box .translucent_background'),
		$errorMessage = $('.confirm_box .error_container'),
		$message = $('.confirm_box .message'),
		$accept = $('.confirm_box #confirm_accept'),
		$reject = $('.confirm_box #confirm_reject'),
		acceptPerformer = null;

	//bindEvents
	$containerBackground.click(close)
		.children().click(function(e) {
			e.stopPropagation();
		});
	$reject.click(close);
	events.on("openConfirmBox", open);
	events.on("confirmBoxError", error);
	$accept.click(execute);

	function close() {
		acceptPerformer = null;
		$container.hide();
		$errorMessage.html('');
		$errorMessage.css(
			'color', 'black'
		);
	}
	function open(data) {
		acceptPerformer = data.performer;
		$container.show();
		if (data.message!=null) {
			$message.html(data.message);
		}
	}
	function error(message) {
		$errorMessage.css({
			'color' : 'red',
			'font-weight' : 'bold'
		});
		$errorMessage.html(message);
	}
	function execute() {
		events.emit(acceptPerformer);
	}
})();
