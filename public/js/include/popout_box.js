var popoutBox = (function(){
    var $container = $('.popout_container'),
        $containerContent = $('.container_content'),
        $closeWindow = $('.close_window'),
        $containerBackground = $('.popout_container .translucent_background'),
        $closeWindow = $('.popout_container .close_window');

    $containerBackground.click(hide)
        .children().click(function(e) {
            e.stopPropagation();
        });
    $closeWindow.click(hide);
    events.on('showPopoutBox', function(){
        $container.show();
    });
    events.on('hidePopoutBox', hide);
    events.on('loadPopoutBoxContent', loadContent);

    function hide() {
        $container.hide();
        $containerContent.html('');
    }
    function loadContent(content) {
        $containerContent.append(content);
    }
})();
