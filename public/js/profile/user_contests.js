var loadUserContests = (function(){
    var $list = $('.contests_list');

    var timeout,
        start = 0,
        page = 1;
        updateStart = 0,
        stop = false;

    $(window).scroll(scrollLoad);

    function loadData() {
        $.ajax ({
            type: "POST",
            url: path+"load-my-contests",
            dataType: "json",
            data: {
                start: start,
                page: page
            },
            beforeSend: function() {
                $list.append('<div class="loading">Loading...</div>');
            },
            success: function(result){
                if (!stop) {
                    updateStart = result['start'];
                    stop = result['stop'];
                    $('.contests_list .loading').remove();
                    $list.append(result['contests_list'] + '<hr>');
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    };

    function scrollLoad() {
        clearTimeout(timeout);
        timeout = setTimeout(function(){
            if ($(window).scrollTop()+window.innerHeight == $(document).innerHeight()) {
                if (!stop) {
                    page++;
                    start = updateStart;
                    loadData();
                }
            }
        }, 100);
    }

    loadData();
})();
