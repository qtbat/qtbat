$(document).on('submit', '.upload-avatar', function(e) {

	var formData = new FormData($(this)[0]);

	$.ajax({
		url: path+"settings/upload-avatar",
		type: 'POST',
		dataType: "json",
		data: formData,
		cache: false,
		contentType: false,
		processData: false,
		success: function(result) {
			if (result['status']=='error') {
				$('.error-container-upload-avatar').addClass('error');
				$('.error-container-upload-avatar').html(result['message']);
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;
});

$('.profile-content > .settings_container > .expand').click(function() {
	$(this).parent().children('.edit_form').toggle();
});

$('.sex').on('submit', function() {

	var sex = $(this).find('input[name="sex"]:checked').val();

	$.ajax({
		url: path+"settings/edit",
		type: 'POST',
		data: {
			sex: sex
		},
		success: function(result) {
			if (result=='error') {
				$('.error-container-sex').addClass('error');
				$('.error-container-sex').html('Wystąpił błąd');
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;
});

$('.email').on('submit', function() {
	var input = $(this).find('input[name="email"]');
	var email = input.val();
	var emailValidate = /^([A-Za-z0-9\-_\.ąćęłńóśźż]+)+@+([A-Za-z0-9\-ąćęłńóśźż]+)+(\.[A-Za-ząćęłńóśźż]+)+$/;
	if (email.match(emailValidate)) {
		$.ajax({
			url: path+"settings/edit",
			type: "POST",
			dataType: "json",
			data: {
				user_email: email
			},
			success: function(result) {
				if(result['status']=='error') {
					$('.error-container-email').addClass('error');
					$('.error-container-email').html(result['message']);
				}
				else {
					$('.success-container-email').addClass('success');
					$('.success-container-email').html(result['message']);
					input.attr('disabled', 'disabled');
				}
			},
			complete: function(xhr, data) {
				if (xhr.status == 404) {
					operation_error();
				}
			}
		});
	} else {
		$('.error-container-email').addClass('error');
		$('.error-container-email').html('Podano błędny adres e-mail!');
	}
	return false;
});

$('.contact_email').on('submit', function() {
	var email = $(this).find('input[name="contact_email"]').val();
	var emailValidate = /^([A-Za-z0-9\-_\.ąćęłńóśźż]+)+@+([A-Za-z0-9\-ąćęłńóśźż]+)+(\.[A-Za-ząćęłńóśźż]+)+$/;
	if (email.match(emailValidate)) {
		$.ajax({
			url: path+"settings/edit",
			type: "POST",
			dataType: "json",
			data: {
				contact_email: email
			},
			success: function(result) {
				if(result['status']=='error') {
					$('.error-container-contact-email').addClass('error');
					$('.error-container-contact-email').html(result['message']);
				}
				else {
					location.reload();
				}
			},
			complete: function(xhr, data) {
				if (xhr.status == 404) {
					operation_error();
				}
			}
		});
	} else {
		$('.error-container-contact-email').addClass('error');
		$('.error-container-contact-email').html('Podano błędny kontaktowy adres e-mail!');
	}
	return false;
});

$('.password').on('submit', function() {

	var old_password = $(this).find('input[name="old_password"]').val();
	var new_password = $(this).find('input[name="new_password"]').val();
	var repeat_new_password = $(this).find('input[name="repeat_new_password"]').val();

	$.ajax({
		url: path+"settings/edit",
		type: "POST",
		dataType: "json",
		data: {
			old_password: old_password,
			new_password: new_password,
			repeat_new_password: repeat_new_password
		},
		success: function(result) {
			if (result['status']=='error') {
				$('.error-container-password').addClass('error');
				$('.error-container-password').html(result['message']);
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;
});

$('.location').on('submit', function() {

	var user_location = $(this).find('input[name="location"]').val();

	$.ajax({
		url: path+"settings/edit",
		type: "POST",
		dataType: "json",
		data: {
			user_location: user_location
		},
		success: function(result) {
			if (result['status']=='error') {
				$('.error-container-location').addClass('error');
				$('.error-container-location').html(result['message']);
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;
});

$('.name').on('submit', function() {

	var name = $(this).find('input[name="name"]').val();

	$.ajax({
		url: path+"settings/edit",
		type: "POST",
		dataType: "json",
		data: {
			name: name
		},
		success: function(result) {
			if (result['status']=='error') {
				$('.error-container-name').addClass('error');
				$('.error-container-name').html(result['message']);
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;
});
