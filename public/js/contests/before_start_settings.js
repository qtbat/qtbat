var changeCategory = (function(){
    var $form = $('.edit_contest_category'),
        $select = $form.find('select[name="select_category"]'),
        $errorContainer = $('.category_error_container');

    $form.on('submit', sendForm);

    function sendForm() {
        $.ajax({
            url: path+contest_creator_login+'/'+contest_link+'/edit-contest',
            type: 'POST',
            dataType: 'json',
            data: {
                select_category: $select.val()
            },
            success: function(result) {
                if (result['status']=='error') {
                    $errorContainer.css({
                        'color' : 'red',
                        'font-weight:' : 'bold'
                    });
                    $errorContainer.html(result['message']);
                }
                else {
                    events.emit('hidePopoutBox');
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });

        return false;
    }
});

var changePrizes = (function(contestType){
    //cacheDom
    var $prizesContainer = $('.prizes'),
        $numberOfWinnersInput = $('.number_of_winners'),
        $prizesMessage = $('.prize_statement'),
        $automaticDelivery = $('.automatic_delivery'),
        $deliveryByAuthor = $('.delivery_by_author'),
        $refreshPrizes = $('.refresh_prizes'),
        $prizesWrapper = $('.prize'),
        $form = $('.change_prizes'),
        $errorContainer = $('.prize_error_container');

    var maxNumberOfWinners;

    if (contestType == 1) {
        maxNumberOfWinners = 5;
    } else if (contestType == 2) {
        maxNumberOfWinners = 4;
    } else if (contestType == 3) {
        maxNumberOfWinners = 4;
    }

    //bindEvents
    $automaticDelivery.click(function(){
        $prizesWrapper.fadeIn("slow");
    });
    $deliveryByAuthor.click(function(){
        $prizesWrapper.hide();
    });
    $automaticDelivery.click(displayPrizes);
    $refreshPrizes.click(displayPrizes);
    $form.on('submit', sendForm);

    function displayPrizes() {
        $prizesContainer.text('');
        var numberOfWinners = parseInt($numberOfWinnersInput.val());

        if (parseInt(numberOfWinners)) {
            if (numberOfWinners>maxNumberOfWinners) {
                $prizesMessage.hide();
                $prizesContainer.css({
                    'color' : 'red',
                    'font-weight' : 'bold'
                });
                $prizesContainer.text('Maksymalna ilość zwycięzców wynosi ' + maxNumberOfWinners + '.');
            } else {
                if (numberOfWinners<1) {
                    $prizesMessage.hide();
                    $prizesContainer.css({
                        'color' : 'red'
                    });
                    $prizesContainer.text('Minimalna ilośc zwycięzców wynosi 1.');
                } else {
                    $prizesMessage.show();
                    $prizesContainer.css({
                        'color' : 'black'
                    });

                    if (contestType==2) {
                        for (var i=1; i<=numberOfWinners; i++) {
                            $prizesContainer.append('<input type="text" name="prize[]" placeholder="Nagroda"/><br />');
                        }
                    } else {
                        for (var i=1; i<=numberOfWinners; i++) {
                            $prizesContainer.append(i+' miejsce <input type="text" name="prize[]" placeholder="Nagroda"/><br />');
                        }
                    }
                }
            }
        } else {
            $prizesMessage.hide();
            $prizesContainer.css({
                'color' : 'red',
                'font-weight' : 'bold'
            });
            $prizesContainer.text('Ilość zwycięzców musi być przedstawiona jako liczba całkowita.');
        }
    }
    function sendForm() {
        var formData = new FormData();

        var other_data = $(this).serializeArray();

        $.each(other_data,function(key, input){
            formData.append(input.name, input.value);
        });

        $.ajax({
            url: path+contest_creator_login+'/'+contest_link+'/edit-contest',
            type: 'POST',
            dataType: 'json',
            data: formData,
            contentType: false,
            processData: false,
            success: function(result) {
                if (result['status']=='error') {
                    $errorContainer.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    $errorContainer.text(result['message']);
                } else {
                    events.emit('hidePopoutBox');
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    $errorContainer.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    $errorContainer.text('Musisz uzupełnić formularz');
                }
            }
        });

        return false;
    }
});
