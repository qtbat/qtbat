$('.take_part_image').on('submit', function() {

	var formData = new FormData($(this)[0]);

	$.ajax({
		url: path+contest_creator_login+'/'+contest_link+'/draw-take-part',
		type: 'POST',
		dataType: "json",
		data: formData,
		cache: false,
		contentType: false,
		processData: false,
		success: function(result) {
			if (result['status']=='error') {
				$('.take_part_error_container').addClass('error');
				$('.take_part_error_container').html(result['message']);
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;
});

$('.take_part_video').on('submit', function(){

	var value = $(this).find('input[name="take_part_video"]').val();

	$.ajax({
		url: path+contest_creator_login+'/'+contest_link+'/draw-take-part',
		type: 'POST',
		data: {
			take_part_video: value
		},
		dataType: "json",
		success: function(result) {
			if (result['status']=='error') {
				$('.take_part_error_container').addClass('error');
				$('.take_part_error_container').html(result['message']);
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;

});

$('.take_part_text').on('submit', function(){

	var value = $(this).find('textarea[name="take_part_text"]').val();

	$.ajax({
		url: path+contest_creator_login+'/'+contest_link+'/draw-take-part',
		type: 'POST',
		data: {
			take_part_text: value
		},
		dataType: "json",
		success: function(result) {
			if (result['status']=='error') {
				operation_error();
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;

});

$('.take_part_file').on('submit', function(){

	var formData = new FormData($(this)[0]);

	$.ajax({
		url: path+contest_creator_login+'/'+contest_link+'/draw-take-part',
		type: 'POST',
		dataType: "json",
		data: formData,
		cache: false,
		contentType: false,
		processData: false,
		success: function(result) {
			if (result['status']=='error') {
				$('.take_part_error_container').addClass('error');
				$('.take_part_error_container').html(result['message']);
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;
});

$('.take_part').on('click', function(){

	$.ajax({
		url: path+contest_creator_login+'/'+contest_link+'/draw-take-part',
		type: 'POST',
		data: {
			take_part: true
		},
		success: function(result) {
			if (result['status']=='error') {
				$('.take_part_error_container').addClass('error');
				$('.take_part_error_container').html(result['message']);
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

});
