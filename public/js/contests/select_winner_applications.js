$('.translucent_background').click(function(){
	$('.application_details').css('display', 'none');
    $('.application_details .content_container').html('');
    $('.application_details .bonus_details').html('');
    $('.application_details .participant_login').html('');
	$('.application_details .application_place')[0].reset();
}).children().click(function(e) {
    e.stopPropagation();
});

var expanded_element = null;

$(document).on('click', '.application', function(){
    var order_number = $(this).data('id');
	var highlight = $(this).data('highlight');
	expanded_element = $(this);
	$('.application_details .details_container').attr('data-id', order_number);
	$('.application_details .details_container').data('id', order_number);

	var status = true;

	if (highlight==null) {
		$('.application_details .details_container').css('border', '2px solid black');
	} else if (highlight=='p') {
		$('.application_details .details_container').css('border', '5px solid green');
	} else if (highlight=='n') {
		$('.application_details .details_container').css('border', '5px solid red');
	} else if (highlight=='s') {
		$('.application_details .details_container').css('border', '5px solid rgb(0, 171, 255)');
	} else if (highlight=='1') {
		$('.application_details .details_container').css('border', '5px solid gold');
	} else if (highlight=='2') {
		$('.application_details .details_container').css('border', '5px solid silver');
	} else if (highlight=='3' || highlight=='4') {
		$('.application_details .details_container').css('border', '5px solid brown');
	} else if (highlight=='a') {
		$('.application_details .details_container').css('border', '5px solid rgb(245, 240, 181)');
	} else {
		status = false;
		operation_error();
	}

	if (status) {
	    $.ajax ({
	        type: "POST",
	        url: path+contest_creator_login+'/'+contest_link+'/application-details',
	        dataType: "json",
	        data: {
	            order_number: order_number
	        },
	        success: function(result){
	            $('.application_details').show();
	            if (result['status']=='success') {
	                if (typeof result['details']['content']['video'] != 'undefined') {
	                    $('.application_details .content_container').html('<iframe width="420" height="315" src="http://www.youtube.com/embed/'+result['details']['content']['video']+'?autoplay=0"></iframe>');

					} else if (typeof result['details']['content']['text'] != 'undefined') {
	                    $('.application_details .content_container').html(result['details']['content']['text']);

					} else if (typeof result['details']['content']['picture'] != 'undefined') {
	                    $('.application_details .content_container').html('<img src="'+path+'img/'+result['details']['content']['picture']+'" style="max-width: 300px; height: auto;"/>');
	                    $('.application_details .bonus_details').html('<a href="'+path+'img/'+result['details']['content']['picture']+'" target="_blank">Zdjęcie w pełnych wymiarach</a>');

					} else if (typeof result['details']['content']['files'] != 'undefined') {
						for (var i=0; i<result['details']['count_files']; i++) {
							$('.application_details .content_container').append('<a href="'+path+'download/'+result['details']['content']['files'][i]['name']+'">'+result['details']['content']['files'][i]['original_name']+'</a><br>');
						}
					}

	                $('.application_details .participant_login').html('<a href="'+path+'profile/'+result['details']['login']+'">'+result['details']['login']+'</a>');
	            } else {
	                operation_error();
	            }
	        },
	        complete: function(xhr, data) {
	            if (xhr.status == 404) {
	                operation_error();
	            }
	        }
	    });
	}
});

var clicks = 0;
var counter = 0;
var locked = false;

function disable_buttons()
{
	setInterval(function(){
		counter++;
		if (counter==10) {
			counter = 0;
		}
		if (counter<=10 && clicks>=10) {
			counter = 0;
			clicks = 0;
			locked = true;
			$('.application_highlights').children().css('background-color', 'grey');
			setTimeout(function(){
				locked = false;
				$('.application_highlights').children().each(function(){
					if ($(this).attr('class')=='positive') {
						$(this).css('background-color', 'green');
					} else if ($(this).attr('class')=='negative') {
						$(this).css('background-color', 'red');
					} else if ($(this).attr('class')=='start_from_here') {
						$(this).css('background-color', 'rgb(0, 171, 255)')
					}
				});
			}, 5000);
		}
	}, 1000);
}

$(document).on('click', '.application_highlights .positive', function(){
	if (!locked) {
		clicks++;
	}

	disable_buttons();

    var order_number = $(this).parent().parent().children('.application').data('id');
    var current_highlight = $(this).parent().parent().children('.application').data('highlight');
	var element = this;
    if (current_highlight != 'p') {
		$(this).parent().parent().children('.application').data('highlight', 'p');
        var highlight = 'p';
    } else {
		$(this).parent().parent().children('.application').data('highlight', 'null');
        var highlight = 'null';
    }

    $.ajax ({
        type: "POST",
        url: path+contest_creator_login+'/'+contest_link+'/application-highlight',
        dataType: "json",
        data: {
            highlight: highlight,
            order_number: order_number
        },
        success: function(result){
            if (result['status']=='success') {
				var count_positive = parseInt($('.count_positive').text());

				if (current_highlight=='n') {
					var count_negative = parseInt($('.count_negative').text());
					$('.count_negative').html(count_negative-1);
				} else if (current_highlight=='1' || current_highlight=='2' || current_highlight=='3' || current_highlight=='4' || current_highlight=='a') {
					var count_awarded = parseInt($('.count_awarded').text());
					$('.count_awarded').html(count_awarded-1);
				}

				if (highlight == 'p') {
					$(element).parent().parent().children('.application').css('border', '5px solid green');
					$('.count_positive').html(count_positive+1);
				} else {
					$(element).parent().parent().children('.application').css('border', '2px solid black');
					$('.count_positive').html(count_positive-1);
				}
            } else {
                operation_error();
            }
        },
        complete: function(xhr, data) {
            if (xhr.status == 404) {
                operation_error();
            }
        }
    });
});

$(document).on('click', '.application_highlights .negative', function(){

	if (!locked) {
		clicks++;
	}

	disable_buttons();

	if (!locked) {
	    var order_number = $(this).parent().parent().children('.application').data('id');
	    var current_highlight = $(this).parent().parent().children('.application').data('highlight');
		var element = this;
	    if (current_highlight != 'n') {
			$(this).parent().parent().children('.application').data('highlight', 'n');
	        var highlight = 'n';
	    } else {
			$(this).parent().parent().children('.application').data('highlight', 'null');
	        var highlight = 'null';
	    }

	    $.ajax ({
	        type: "POST",
	        url: path+contest_creator_login+'/'+contest_link+'/application-highlight',
	        dataType: "json",
	        data: {
	            highlight: highlight,
	            order_number: order_number
	        },
	        success: function(result){
	            if (result['status']=='success') {
					var count_negative = parseInt($('.count_negative').text());

					if (current_highlight=='p') {
						var count_positive = parseInt($('.count_positive').text());
						$('.count_positive').html(count_positive-1);
					} else if (current_highlight=='1' || current_highlight=='2' || current_highlight=='3' || current_highlight=='4' || current_highlight=='a') {
						var count_awarded = parseInt($('.count_awarded').text());
						$('.count_awarded').html(count_awarded-1);
					}

					if (highlight == 'n') {
						$(element).parent().parent().children('.application').css('border', '5px solid red');
						$('.count_negative').html(count_negative+1);
					} else {
						$(element).parent().parent().children('.application').css('border', '2px solid black');
						$('.count_negative').html(count_negative-1);
					}
	            } else {
	                operation_error();
	            }
	        },
	        complete: function(xhr, data) {
	            if (xhr.status == 404) {
	                operation_error();
	            }
	        }
	    });
	}
});

$(document).on('click', '.application_highlights .start_from_here', function(){
	if (!locked) {
		clicks++;
	}

	disable_buttons();

	if (!locked) {
		var order_number = $(this).parent().parent().children('.application').data('id');
		var current_highlight = $(this).parent().parent().children('.application').data('highlight');
		if (current_highlight != 's') {
			var highlight = 's';
		} else {
			var highlight = 'null';
		}

	    $.ajax ({
	        type: "POST",
	        url: path+contest_creator_login+'/'+contest_link+'/application-highlight',
	        dataType: "json",
	        data: {
	            highlight: highlight,
	            order_number: order_number
	        },
	        success: function(result){
	            if (result['status']=='success') {
					location.reload();
	            } else {
	                operation_error();
	            }
	        },
	        complete: function(xhr, data) {
	            if (xhr.status == 404) {
	                operation_error();
	            }
	        }
	    });
	}
});

$(document).on('click', '.application_highlights .remove_highlight', function(){
	if (!locked) {
		clicks++;
	}

	disable_buttons();

	if (!locked) {
		var element = $(this);
		var order_number = $(this).parent().parent().children('.application').data('id');
		var current_highlight = $(this).parent().parent().children('.application').data('highlight');

		$.ajax ({
			type: "POST",
			url: path+contest_creator_login+'/'+contest_link+'/application-highlight',
			dataType: "json",
			data: {
				highlight: 'null',
				order_number: order_number
			},
			success: function(result){
				if (result['status']=='success') {
					$(element).parent().parent().children('.application').data('highlight', 'null');
					$(element).parent().parent().children('.application').css('border', '2px solid black');
					if (current_highlight=='p') {
						var count_positive = parseInt($('.count_positive').text());
						$('.count_positive').html(count_positive-1);
					} else if (current_highlight=='n') {
						var count_negative = parseInt($('.count_negative').text());
						$('.count_negative').html(count_negative-1);
					} else if (current_highlight=='1' || current_highlight=='2' || current_highlight=='3' || current_highlight=='4' || current_highlight=='a') {
						var count_awarded = parseInt($('.count_awarded').text());
						$('.count_awarded').html(count_awarded-1);
					}

				} else {
					operation_error();
				}
			},
			complete: function(xhr, data) {
				if (xhr.status == 404) {
					operation_error();
				}
			}
		});
	}
});

$('.application_place').on('submit', function(){
	var order_number = $('.application_details .details_container').data('id');
	var highlight = $(this).parent().find('input[name="place"]:checked').val();
	var current_highlight = expanded_element.data('highlight');

	var status = true;

	if (highlight!='a') {
		var previous_element = $('.application[data-highlight="'+highlight+'"]');
		$(previous_element).attr('data-highlight', 'null');
		$(previous_element).css('border', '2px solid black');
	}

	expanded_element.attr('data-highlight', highlight);
	expanded_element.data('highlight', highlight);

	if (highlight=='1') {
		$('.application_details .details_container').css('border', '5px solid gold');
		expanded_element.css('border', '5px solid gold');
	} else if (highlight=='2') {
		$('.application_details .details_container').css('border', '5px solid silver');
		expanded_element.css('border', '5px solid silver');
	} else if (highlight=='3' || highlight=='4') {
		$('.application_details .details_container').css('border', '5px solid brown');
		expanded_element.css('border', '5px solid brown');
	} else if (highlight=='a') {
		$('.application_details .details_container').css('border', '5px solid rgb(245, 240, 181)');
		expanded_element.css('border', '5px solid rgb(245, 240, 181)');
	} else {
		status = false;
		operation_error();
	}

	if (status) {
		$.ajax ({
			type: "POST",
			url: path+contest_creator_login+'/'+contest_link+'/application-highlight',
			dataType: "json",
			data: {
				highlight: highlight,
				order_number: order_number
			},
			success: function(result){
				if (result['status']=='success') {
					if (current_highlight=='p') {
						var count_positive = parseInt($('.count_positive').text());
						$('.count_positive').html(count_positive-1);
					} else if (current_highlight=='n') {
						var count_negative = parseInt($('.count_negative').text());
						$('.count_negative').html(count_negative-1);
					}

					if (current_highlight!='1' && current_highlight!='2' && current_highlight!='3' && current_highlight!='4' && current_highlight!='a') {
						var count_awarded = parseInt($('.count_awarded').text());
						$('.count_awarded').html(count_awarded+1);
					}
				} else {
					operation_error();
				}
			},
			complete: function(xhr, data) {
				if (xhr.status == 404) {
					operation_error();
				}
			}
		});
	}

	return false;
});

$('.confirm_winners').click(function(){
	$('.confirm_box').show();

	$('.confirm_box .message').html('Upublicznić zwycięzkie prace? <input type="radio" name="show_winners" value="yes" class="show_winners" /> Tak <input type="radio" name="show_winners" value="no" class="show_winners" /> Nie')

	$('.confirm_box #confirm_accept').click(function(){
		var show_winners = $('.confirm_box .message').find('input[name="show_winners"]:checked').val();

		$.ajax ({
			type: "POST",
			url: path+contest_creator_login+'/'+contest_link+'/confirm-winners',
			dataType: "json",
			data: {
				confirm_winners: true,
				show_winners: show_winners
			},
			success: function(result){
				if (result['status']=='success') {
					location.replace(path+contest_creator_login+'/'+contest_link);
				} else {
					$('.confirm_box .error_container').addClass('error');
					$('.confirm_box .error_container').html(result['message']);
				}
			},
			complete: function(xhr, data) {
				if (xhr.status == 404) {
					operation_error();
				}
			}
		});
	});
});
