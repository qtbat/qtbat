var picturePreview = (function(){
    var $imageInput = $('.upload_image'),
        $clearingInput = $('.clear_input');

    $imageInput.change(imagePreview);
    $clearingInput.click(clearPreview);

    function imagePreview() {
        if (this.files && this.files[0]) {
            var element = $(this);
            var reader = new FileReader();

            reader.onload = function(e) {
                element.parent().css({
                    'background' : 'url('+e.target.result +')',
                    'background-size' : '100% 100%',
                    'background-repeat' : 'no-repeat'
                });
            }
            reader.readAsDataURL(this.files[0]);

            $(this).parent().parent().children('.clear_input').show();
        }
    }
    function clearPreview() {
        $(this).parent().children('.add_contest_image').children('.upload_image').val("");

        $(this).parent().children('.add_contest_image').removeAttr("style");

        $(this).hide();
    }
})();

var editImages = (function(){
    var $form = $('.edit_images'),
        $errorContainer= $('.change_images_error_container');

    $form.on('submit', sendForm);

    function sendForm() {
        var formData = new FormData($(this)[0]);

        var other_data = $(this).serializeArray();

        $.each(other_data,function(key, input){
            formData.append(input.name, input.value);
        });

        $.ajax({
            url: path+contest_creator_login+'/'+contest_link+'/edit-contest-images',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(result) {
                if (result['status']=='error') {
                    $errorContainer.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    $errorContainer.html(result['message']);
                } else {
                    location.reload();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });

        return false;
    }
})();

var deleteContestBanner = (function(){
    var $deleteButton = $('.delete_contest_banner'),
        $errorContainer = $('.banner_error_container');

    $deleteButton.click(commit);

    function commit() {
        $.ajax({
            url: path+contest_creator_login+'/'+contest_link+'/edit-contest-banner',
            type: 'POST',
            dataType: "json",
            data: {
                delete_banner: true
            },
            success: function(result) {
                if (result['status']=='error') {
                    $errorContainer.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    $errorContainer.html(result['message']);
                } else {
                    location.reload();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
})();

var uploadContestBanner = (function(){
    var $form = $('.upload_contest_banner'),
        $errorContainer = $('.banner_error_container');

    $form.on('submit', send);

    function send() {
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: path+contest_creator_login+'/'+contest_link+'/edit-contest-banner',
            type: 'POST',
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(result) {
                if (result['status']=='error') {
                    $errorContainer.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    $errorContainer.html(result['message']);
                } else {
                    location.reload();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });

        return false;
    }
})();
