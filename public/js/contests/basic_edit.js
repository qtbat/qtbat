var editContestTitle = (function(){
    var $switchForm = $('.switch_title_form'),
        $titleContent = $('.title_content'),
        $titleWrapper = $('.title_wrapper'),
        $titleFormContainer = $('.title_form_container'),
        $titleForm = $('.title_form_container .title_form'),
        $titleInput = $titleForm.find('input[name="title"]'),
        $errorContainer = $('.title_error_container');

    $switchForm.click(toggleForm);
    $titleForm.on('submit', changeTitle);

    function toggleForm() {
        $titleWrapper.toggle();
        $titleFormContainer.toggle();
    }
    function changeTitle() {
        $.ajax({
            url: path+contest_creator_login+'/'+contest_link+'/edit-contest',
            type: 'POST',
            dataType: 'json',
            data: {
                contest_title: $titleInput.val()
            },
            success: function(result) {
                if (result['status']=='error') {
                    $errorContainer.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    $errorContainer.html(result['message']);
                }
                else {
                    toggleForm();
                    $titleContent.html($titleInput.val());
                    $titleInput.val($titleInput.val());
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });

        return false;
    }
})();

var editContestDescription = (function(){
    var $switchForm = $('.switch_description_form'),
        $descriptionContent = $('.description_content'),
        $descriptionWrapper = $('.description_wrapper'),
        $descriptionFormContainer = $('.description_form_container'),
        $descriptionForm = $('.description_form_container .description_form'),
        $descriptionTextarea = $descriptionForm.find('textarea[name="description"]'),
        $errorContainer = $('.description_error_container');

    $switchForm.click(toggleForm);
    $descriptionForm.on('submit', changeDescription);

    function toggleForm() {
        $descriptionWrapper.toggle();
        $descriptionFormContainer.toggle();
    }
    function changeDescription() {
        $.ajax({
            url: path+contest_creator_login+'/'+contest_link+'/edit-contest',
            type: 'POST',
            dataType: 'json',
            data: {
                contest_description: $descriptionTextarea.val()
            },
            success: function(result) {
                if (result['status']=='error') {
                    $errorContainer.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    $errorContainer.html(result['message']);
                } else {
                    toggleForm();
                    $descriptionContent.html($descriptionTextarea.val());
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });

        return false;
    }
})();
