$('.take_part_image').on('submit', function() {

	var formData = new FormData($(this)[0]);

	$.ajax({
		url: path+contest_creator_login+'/'+contest_link+'/select-winner-take-part',
		type: 'POST',
		dataType: "json",
		data: formData,
		cache: false,
		contentType: false,
		processData: false,
		success: function(result) {
			if (result['status']=='error') {
				$('.take_part_error_container').addClass('error');
				$('.take_part_error_container').html(result['message']);
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;
});

$('.take_part_video').on('submit', function(){

	var value = $(this).find('input[name="take_part_video"]').val();

	$.ajax({
		url: path+contest_creator_login+'/'+contest_link+'/select-winner-take-part',
		type: 'POST',
		data: {
			take_part_video: value
		},
		dataType: "json",
		success: function(result) {
			if (result['status']=='error') {
				$('.take_part_error_container').addClass('error');
				$('.take_part_error_container').html(result['message']);
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;

});

$('.take_part_text').on('submit', function(){

	var value = $(this).find('textarea[name="take_part_text"]').val();

	$.ajax({
		url: path+contest_creator_login+'/'+contest_link+'/select-winner-take-part',
		type: 'POST',
		data: {
			take_part_text: value
		},
		dataType: "json",
		success: function(result) {
			if (result['status']=='error') {
				$('.take_part_error_container').addClass('error');
				$('.take_part_error_container').html(result['message']);
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;

});

$('.take_part_files').on('submit', function() {

	var formData = new FormData($(this)[0]);
	var data = $(this).serializeArray();

	$.each(data,function(key, input){
        formData.append(input.name, input.value);
    });


	$.ajax({
		url: path+contest_creator_login+'/'+contest_link+'/select-winner-take-part',
		type: 'POST',
		dataType: "json",
		data: formData,
		cache: false,
		contentType: false,
		processData: false,
		success: function(result) {
			if (result['status']=='error') {
				$('.take_part_error_container').addClass('error');
				$('.take_part_error_container').html(result['message']);
			}
			else {
				location.reload();
			}
		},
		complete: function(xhr, data) {
			if (xhr.status == 404) {
				operation_error();
			}
		}
	});

	return false;
});
