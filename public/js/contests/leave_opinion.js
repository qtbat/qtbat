$('.leave_opinion').click(function(){
    $('.leave_opinion_container').show();
});

$('.close_window').click(function(){
    $('.leave_opinion_container').hide();
});

$('.translucent_background').click(function(){
	$('.leave_opinion_container').css('display', 'none');
}).children().click(function(e) {
    e.stopPropagation();
});

$('.rate_author').on('submit', function(){
    var rate = $(this).find('input[name="rate"]:checked').val();
    var comment = $(this).find('textarea[name="comment"]').val();

    $.ajax ({
        type: "POST",
        url: path+contest_creator_login+'/'+contest_link+'/leave-opinion',
        dataType: "json",
        data: {
            rate: rate,
            comment: comment,
        },
        success: function(result){
            if(result['status']=='success') {
                $('.opinion_message').removeClass('error');
                $('.opinion_message').addClass('success');
                $('.opinion_message').text('Pomyślnie wysłano opinię.');
            } else if(result['status']='error') {
                $('.opinion_message').removeClass('success');
                $('.opinion_message').addClass('error');
                $('.opinion_message').text(result['message']);
            }
        },
        complete: function(xhr, data) {
            if (xhr.status == 404) {
                operation_error();
            }
        }
    });

    return false;
});
