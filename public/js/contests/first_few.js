$('.take_part').click(function(){
    $('.take_part_container').show();
});

$('.close_window').click(function(){
    $('.take_part_container').hide();
});

$('.translucent_background').click(function(){
	$('.take_part_container').css('display', 'none');
}).children().click(function(e) {
    e.stopPropagation();
});

$('.answer_form').on('submit', function(){
    var answer = $(this).find('input[name="answer"]').val();
    var prize = $(this).find(':selected').val();

    $.ajax ({
        type: "POST",
        url: path+contest_creator_login+'/'+contest_link+'/send-answer',
        dataType: "json",
        data: {
            answer: answer,
            prize: prize
        },
        success: function(result){
            if(result['status']=='success') {
                location.reload();
            } else if(result['status']='error') {
                $('.answer_error').addClass('error');
                $('.answer_error').text(result['message']);
            }
        }
    });

    return false;
});
