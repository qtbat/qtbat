var startContest = (function(){
    var $button = $('.start_contest');

    //bindEvents
    $button.click(displayConfirmBox);
    events.on("startContest", accept);

    function displayConfirmBox() {
        events.emit("openConfirmBox", {
            message: "Jeśli rozpoczniesz ten konkurs nie będziesz mógł zmodyfikować niektórych elementów jak banner, zdjęcia, nagrody itd. Czy na pewno chcesz rozpocząć ten konkurs?",
            performer: "startContest"});
    }
    function accept() {
        $.ajax({
            url: path+contest_creator_login+'/'+contest_link+'/start-contest',
            type: 'POST',
            dataType: 'json',
            data: {
                start_contest: true
            },
            success: function(result) {
                if (result['status']=='error') {
                    events.emit("confirmBoxError", result['message']);
                } else {
                    location.reload();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
})();
