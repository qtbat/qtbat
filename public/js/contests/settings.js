var changeAvailability = (function(){
    var $avaibleForGroups = $('.avaible_for_groups'),
        $avaibleForEveryone = $('.avaible_as_normal'),
        $groupsSelection = $('.select_groups'),
        $groupsSelectionOption = $('.groups_selection option'),
        $form = $('.contest_availability'),
        $errorContainer = $('.contest_availability_error_container');

    $avaibleForGroups.click(function(){
        $groupsSelection.fadeIn("slow");
    });
    $avaibleForEveryone.click(function(){
        $groupsSelection.hide();
    });
    $groupsSelectionOption.mousedown(multipleGroupsSelection);
    $form.on('submit', sendForm);

    function multipleGroupsSelection(e) {
        e.preventDefault();
        $(this).prop('selected', $(this).prop('selected') ? false : true);
        return false;
    }
    function sendForm() {
        var formData = new FormData();

        var other_data = $(this).serializeArray();

        $.each(other_data,function(key, input){
            if(input.name=='select_groups') {
                var groups = [];
                $('.groups_selection :selected').each(function(i, selected){
                    groups[i] = $(selected).val();
                });
                formData.append(input.name, JSON.stringify(groups));
            }
            else {
                formData.append(input.name, input.value);
            }
        });

        $.ajax({
            url: path+contest_creator_login+'/'+contest_link+'/edit-contest',
            type: 'POST',
            dataType: 'json',
            data: formData,
            contentType: false,
            processData: false,
            success: function(result) {
                if (result['status']=='error') {
                    $errorContainer.css({
                        'color' : 'red',
                        'font-weight:' : 'bold'
                    });
                    $errorContainer.html(result['message']);
                } else {
                    location.reload();
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    $errorContainer.css({
                        'color' : 'red',
                        'font-weight' : 'bold'
                    });
                    $errorContainer.text('Musisz uzupełnić formularz');
                }
            }
        });

        return false;
    }
});

var contestSettings = (function(){
    var $expandButton = $('.settings');

    $expandButton.click(loadContainer);

    function loadContainer() {
        events.emit('showPopoutBox');
        $.ajax({
            url: path+contest_creator_login+'/'+contest_link+'/settings',
            type: 'POST',
            data: {
                load_settings: true
            },
            success: function(result) {
                events.emit('loadPopoutBoxContent', result);
                changePrizes(contestType);
                changeAvailability();
                changeCategory();
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
})();
