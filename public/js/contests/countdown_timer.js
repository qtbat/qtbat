$('#countdown').countdown(finish_date, function(event) {
    $(this).text(
        event.strftime('%D dni %H:%M:%S')
    );
})
    .on('finish.countdown', function(){
        location.reload();
    });
