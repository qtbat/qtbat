var startContestWithDatePicker = (function(){
    var $expandDatePicker = $('.expand_date_picker'),
        $datePickerContainer = $('.date_picker_container'),
        $containerBackground = $('.date_picker_container .translucent_background'),
        $pickDateForm = $('.date_picker_container .pick_date'),
        $date = $pickDateForm.find('input[name="date"]'),
        $hour = $pickDateForm.find('input[name="hour"]'),
        $minute = $pickDateForm.find('.date_picker_minute'),
        $errorContainer = $('.pick_date_error_container');

        $expandDatePicker.click(function(){
            $datePickerContainer.show();
        });
        $containerBackground.click(function(){
            $datePickerContainer.hide();
        }).children().click(function(e) {
            e.stopPropagation();
        });
        $pickDateForm.on('submit', startContest);

        function startContest() {
            $.ajax({
                url: path+contest_creator_login+'/'+contest_link+'/start-contest',
                type: 'POST',
                dataType: 'json',
                data: {
                    start_contest: true,
                    date: $date.val(),
                    hour: $hour.val(),
                    minute: $minute.val()
                },
                success: function(result) {
                    if (result['status']=='error') {
                        $errorContainer.css({
                            'color' : 'red',
                            'font-weight' : 'bold'
                        });
                        $errorContainer.html(result['message']);
                    } else {
                        location.reload();
                    }
                },
                complete: function(xhr, data) {
                    if (xhr.status == 404) {
                        events.emit("operationError");
                    }
                }
            });

            return false;
        }
})();
