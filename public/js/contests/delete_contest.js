var deleteContest = (function(){
    var $button = $('.delete_contest');

    $button.click(displayConfirmBox);
    events.on("deleteContest", accept);

    function displayConfirmBox() {
        events.emit("openConfirmBox", {
            message: "Czy na pewno chcesz usunąć ten konkurs? Wszystkie informacje z nim związane zostaną stracone.",
            performer: "deleteContest"});
    }
    function accept() {
        $.ajax({
            url: path+contest_creator_login+'/'+contest_link+'/delete-contest',
            type: 'POST',
            dataType: 'json',
            data: {
                delete_contest: true
            },
            success: function(result) {
                if (result['status']=='error') {
                    events.emit("confirmBoxError", result['message']);
                } else {
                    location.replace(path);
                }
            },
            complete: function(xhr, data) {
                if (xhr.status == 404) {
                    events.emit("operationError");
                }
            }
        });
    }
})();
